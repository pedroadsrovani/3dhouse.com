<?php 
#lembrar de carregar no autoload.phpa função $autoload['helper'] = array('url', 'funcoes') não utilizar o _helper
defined('BASEPATH') OR exit('No direct script access allowed'); 
if(!function_exists('setURL')){ #checa se a função existe
    function setURL(&$data, $controller){ #parametro $data (variavel de dados) e $controller e o caminho abreviado para utilizar no arquivo de controler onde a função será carregada
        $data['URLLISTAR'] = site_url("painel/{$controller}"); 
       $data['ACAOFORM'] = site_url("painel/{$controller}/salvar");
    }
} 

if(!function_exists('modificaDinheiroBanco')){  
    /*  modifica o valor de moeda para o do banco de dados */
    function modificaDinheiroBanco($valor){ 
        $valor = str_replace(',', null, $valor);  
        $valor = str_replace(',', '.', $valor); 
        return $valor;
    }
} 


if(!function_exists('modificaNumericValor')){  
    /*  modifica o valor de numeric para valor em R$(de ponto para vírgula) */
    function modificaNumericValor($valor){ 
        $valor = number_format($valor, 2, ',', '.');
        
        return $valor;
    }
}