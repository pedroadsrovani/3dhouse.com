<?php 
#lembrar de carregar no autoload.phpa função $autoload['helper'] = array('url', 'funcoes') não utilizar o _helper
defined('BASEPATH') OR exit('No direct script access allowed');  


if(!function_exists('gerar_estrutura_departamento')){ #checa se a função existe
	function gerar_estrutura_departamento(){
   		$CI = get_instance(); 
    	$CI->load->model('Departamento_Model', 'departamentoM'); 
    	$dep_n1 = $CI->departamentoM->get_departamentos_n1();
        $saida="";
        foreach($dep_n1 as $dep){
                
                $dep_filhos = array();
                $dep_filhos = $CI->departamentoM->get_departamentos_filhos($dep->codepartamento);
                
                 $saida.=gerar_estrutura($dep_filhos);
        }

        return $saida;

    }
}

function gerar_estrutura($dados){
    $saida="";
     for($indice=0;$indice < count($dados);$indice++){
           
        
            if (count($dados)==1 or $dados[$indice]->nivel == $dados[$indice+1]->nivel)
                $saida.=sprintf('<li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><strong>%s</strong></a></li>',$dados[$indice]->nome);
            else {
                    if (count($dados)> 1)
                        return $saida.=sprintf('<li class="dropdown-submenu"><a href="#" class="test" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><strong>%s</strong></a><ul class="dropdown-menu">%s</ul></li>'," => ".$dados[$indice]->nome,gerar_estrutura( array_slice($dados,$indice+1)));
                


            }
          

    }  

    return $saida;
} 