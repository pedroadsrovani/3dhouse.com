<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipoAtributo_Model extends CI_Model{  


    #função de paginação do itens da tabela 
public function getTotal($condicao = array()){ 
    $this->db->where($condicao); 
    $this->db->from('tipoatributo'); 
    return $this->db->count_all_results();#contador de todos os itens da pesquisa 

}

#limitação do resultado da querie limita o resultado com o valor da variavel linhas_pesquisa_paineladmin
public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_PAINELADMIN ){ 
#limite equivale a LINHAS_PAINEL_ADMIN
  $this->db->select('codtipoatributo, nometipoatributo'); 
  $this->db->where($condicao); 
  $this->db->from('tipoatributo'); 

  if ($primeiraLinha){ 
    return  $this->db->get()->first_row(); 

  }else{  
      if($limite !== FALSE){ # se o limite for passado como false então não irá limitar
          $this->db->limit($limite, $pagina);#limita o numero de tipos de atributos pela quantidade de itens exibidos na pagina. 

      } 
      
      return $this->db->get()->result(); 
    
    }
}

#metodo post
   public function post($itens){ 
     $res =  $this->db->insert('tipoatributo', $itens); 
        if($res){ 
            return $this->db->insert_id();
        }else{ 

            return FALSE;
        }
   } 


#update atipoatributo 
public function update($itens, $codtipoatributo) {
		$this->db->where('codtipoatributo', $codtipoatributo, FALSE);
		$res = $this->db->update('tipoatributo', $itens);
		if ($res) {
			return $codtipoatributo;
		} else {
			return FALSE;
		}
	}


   #metodo excluir tipoatributo
   public function delete($codtipoatributo){  
       $this->db->where('codtipoatributo', $codtipoatributo, FALSE);
       return $this->db->delete('tipoatributo');

   }
}