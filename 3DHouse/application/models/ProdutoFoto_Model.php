<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoFoto_Model extends CI_Model {
	#função do retaorno da quantidade de dados da tabela atributo 

public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_PAINELADMIN ){ 

  $this->db->select('p.codprodutofoto, p.tipo, p.codproduto, p.fotoprincipal'); 
  $this->db->where($condicao); 
  $this->db->from('produtofoto p'); 

  if ($primeiraLinha){ 
    return  $this->db->get()->first_row(); 

  }else{  
      if($limite !== FALSE){ 
          $this->db->limit($limite, $pagina);#limita o numero de tipos de atributos pela quantidade de itens exibidos na pagina. 

      } 
      
      return $this->db->get()->result(); 
    
    }
} 

	
	/* função  para inserção de itens na tabela atributo */
	public function all() {  

      $result = $this->db->get('produtofoto');
      return $result; 
    }


    public function get_by_product($product_id) {  

      $result = $this->db->from ('produtofoto')->where(array('codproduto'=>$product_id ))->get();
      return $result; 
    }




 public function find($id) { $row = $this->db->where('codprodutofoto',$id)->limit(1)->get('produtofoto');
      return $row;}

 public function create($data) {  
    //$this->db->select('p.id, p.tipo, p.codproduto, p.descricao');
    //$this->db->select('pr.id');
    //$this->db->from('produtofoto p');
    //$this->db->join('produto pr', 'pr.id = p.id', 'INNER');
     try{
         $this->db->insert('produtofoto', $data);
         return true;
      }catch(Exception $e){
         echo $e->getMessage();
      }
 }

 public function update($id, $data) {try{
         $this->db->where('codprodutofoto',$id)->limit(1)->update('produtofoto', $data);
         return true;
      }catch(Exception $e){
         echo $e->getMessage();
      }}

 public function delete($id) { 
     try { 
         
         $this->db->where('codprodutofoto',$id)->delete('produtofoto');
         return true;
      }

      //catch exception
      catch(Exception $e) {
        echo $e->getMessage();
      }
 }
	/* imagens = produtofoto */

  }   
  /* fim do delete */