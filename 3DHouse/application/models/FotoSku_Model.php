<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FotoSku_Model extends CI_Model {
    
    public function post($item) {
        $this->db->insert('produtofotosku', $item);
    }
    
    public function limpaImagens($codprodutofoto) {
        $this->db->where('codprodutofoto', $codprodutofoto, FALSE);
        $this->db->delete('produtofotosku');
    } 


}