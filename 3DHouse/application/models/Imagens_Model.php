<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagens_model extends CI_Model {

 public function all() { $result = $this->db->get('imagens');
      return $result;}

 public function find($id) { $row = $this->db->where('codimagen',$id)->limit(1)->get('imagens');
      return $row;}

 public function create($data) { 
     try{
         $this->db->insert('imagens', $data);
         return true;
      }catch(Exception $e){
         echo $e->getMessage();
      }
 }

 public function update($id, $data) {try{
         $this->db->where('codimagen',$id)->limit(1)->update('imagens', $data);
         return true;
      }catch(Exception $e){
         echo $e->getMessage();
      }}

 public function delete($id) { 
     try {
         $this->db->where('codimagen',$id)->delete('imagens');
         return true;
      }

      //catch exception
      catch(Exception $e) {
        echo $e->getMessage();
      }
 }

}