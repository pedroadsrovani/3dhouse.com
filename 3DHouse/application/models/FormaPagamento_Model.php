<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FormaPagamento_Model extends CI_Model{  


    #função de paginação do itens da tabela 
public function getTotal($condicao = array()){ 
    $this->db->where($condicao); 
    $this->db->from('formapagamento'); 
    return $this->db->count_all_results();#contador de todos os itens da pesquisa 

}

#limitação do resultado da querie limita o resultado com o valor da variavel linhas_pesquisa_paineladmin
public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_PAINELADMIN ){ 
#limite equivale a LINHAS_PAINEL_ADMIN
  $this->db->select('codformapagamento, nomeformapagamento, tipoformapagamento, descontoformapagamento, habilitaformapagamento, maximoparcelaspagamento'); 
  $this->db->where($condicao); 
  $this->db->from('formapagamento'); 

  if ($primeiraLinha){ 
    return  $this->db->get()->first_row(); 

  }else{  
      if($limite !== FALSE){ # se o limite for passado como false então não irá limitar
          $this->db->limit($limite, $pagina);#limita o numero de tipos de atributos pela quantidade de itens exibidos na pagina. 

      } 
      
      return $this->db->get()->result(); 
    
    }
}

#metodo post
   public function post($itens){ 
     $res =  $this->db->insert('formapagamento', $itens); 
        if($res){ 
            return $this->db->insert_id();
        }else{ 

            return FALSE;
        }
   } 


#update atipoatributo 
public function update($itens, $codformapagamento) {
		$this->db->where('codformapagamento', $codformapagamento, FALSE);
		$res = $this->db->update('formapagamento', $itens);
		if ($res) {
			return $codformapagamento;
		} else {
			return FALSE;
		}
	}


   #metodo excluir tipoatributo
   public function delete($codformapagamento){  
       $this->db->where('codformapagamento', $codformapagamento, FALSE);
       return $this->db->delete('formapagamento');

   }
}