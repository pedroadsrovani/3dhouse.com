<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto_Model extends CI_Model{  


    #função de paginação do itens da tabela 
public function getTotal($condicao = array()){ 
    $this->db->where($condicao); 
    $this->db->from('produto'); 
    return $this->db->count_all_results();#contador de todos os itens da pesquisa 

}


public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $usuario_id = null, $limite = LINHAS_PESQUISA_PAINELADMIN , $ordenacao = FALSE, $tipoOrdem = 'ASC'){ 

  $this->db->select('codproduto, nomeproduto, resumoproduto, fichaproduto, valorproduto, valorpromocional, codtipoatributo, urlseo, usuario_id, pesoproduto, comprimentoproduto, alturaproduto, larguraproduto, diametroproduto, formatoproduto, maopropriaproduto');  
  $this->db->where('usuario_id' , $usuario_id);
  $this->db->where($condicao);  
  $this->db->from('produto'); 
  
  

  if ($primeiraLinha){ 
    return  $this->db->get()->first_row(); 

  }else{  
      if($limite !== FALSE){ 
          $this->db->limit($limite, $pagina);#limita o numero de tipos de atributos pela quantidade de itens exibidos na pagina. 

      }  

      if($ordenacao){ 
        $this->db->order_by($ordenacao, $tipoOrdem);
      }
      
      return $this->db->get()->result(); 
    
    }
} 


   public function post($itens){ 
     $res =  $this->db->insert('produto', $itens); 
        if($res){ 
            return $this->db->insert_id();
        }else{ 

            return FALSE;
        }
   } 


#update produto
public function update($itens, $codproduto) {
		$this->db->where('codproduto', $codproduto, FALSE);
		$res = $this->db->update('produto', $itens);
		if ($res) {
			return $codproduto;
		} else {
			return FALSE;
		}
	}


   #metodo excluir admins 
   public function delete($codproduto){  
       $this->db->where('codproduto', $codproduto, FALSE);
       return $this->db->delete('produto');

   } 
   public function getvitrine(){ 
return  $this->db->query("select * from vw_vitrine_ultimos_produtos")->result(); 
 

} 
public function getvitrinepromocional(){ 
return  $this->db->query("select * from vw_vitrine_produtos_promocional")->result(); 
 

} 

public function get_ficha_produto($codproduto){ 
      return  $this->db->query("select * from vw_ficha_tecnica_produto where codproduto = $codproduto")->row_array(); //result retorna mais de um dado
     

      }  

  

  public function get_busca($pesquisa){ 
 
  $this->db->select('p.codproduto, p.nomeproduto, p.resumoproduto, p.fichaproduto, p.valorproduto, p.valorpromocional, p.codtipoatributo, p.urlseo, pf.tipo, pf.fotoprincipal, u.primeironome, u.sobrenome');  

 //$this->db->like('p.nomeproduto', $pesquisa);
  $this->db->from('produto p');  
   $this->db->join('usuarios u', 'p.usuario_id = u.codusuario', 'INNER'); 
  $this->db->join('produtofoto pf', 'p.codproduto = pf.codproduto', 'INNER'); 
  $this->db->where("pf.fotoprincipal = 'S'"); 
  $this->db->where(" similarity(p.nomeproduto, '$pesquisa')> 0.2");
    return $this->db->get()->result();  
      
 
}  

public function get_total_busca($condicao = array()){ 
    $this->db->where($condicao); 
    $this->db->from('produto'); 
    return $this->db->count_all_results();#contador de todos os itens da pesquisa 

}

public function get_produto($codproduto){ 

  $this->db->select('codproduto, nomeproduto, resumoproduto, fichaproduto, valorproduto, valorpromocional, codtipoatributo, urlseo');  
  //$this->db->where('');
    
  $this->db->from('produto'); 
      
  return $this->db->get()->row(); 
    
    } 



    public function get_produto_depto_count($codepartamento){ 

       $this->db->select('count(*) as total'); 
       $this->db->from("usuarios u");
       $this->db->join("lista_produto_departamento($codepartamento) f","f.usuario_id = u.codusuario","INNER");

       return $this->db->get()->row();

    }

    public function get_produto_depto($codepartamento,$limite, $offset){ 

       $this->db->select('u.*, f.*'); 
       $this->db->from("usuarios u");
       $this->db->join("lista_produto_departamento($codepartamento) f","f.usuario_id = u.codusuario","INNER");
       $this->db->limit($limite);
       $this->db->offset($offset);

       return $this->db->get()->result();

    }
} 


 
        

 

