<!-- primeiro passo copiar e colar a estrutura html do paineladmin e lincar os css/bootstrap/e o javascript -->
<!DOCTYPE html>
<html lang="pt-br">
  <head> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">  
 
 <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico');?>" type="image/x-icon" />

  <script src="<?php echo base_url('assets/js/jquery.min.v3.2.1.js')?>"></script> 
  
 
 <script>
            
            $(function(){
                $("#btn_consulta").click(function(){
                    var cep = $('#cep').val();
                    
                    
                    if (cep == '') {
                        alert('Informe o CEP antes de continuar');
                        $('#cep').focus();
                        return false;
                    }
                    
                    
                    $('#btn_consulta').html ('Aguarde...');
                    
                    
                    
                    $.post('<?php echo site_url(array('auth','consulta')); ?>',
                    {
                        cep : cep
                    }, 
                    function(dados){
                       
                        $('#rua').val(dados.logradouro);
                        $('#bairro').val(dados.bairro);
                        $('#cidade').val(dados.localidade);
                        $('#estado').val(dados.uf);
                        $('#btn_consulta').html('Consultar');
                    }, 'json');
                });
            });
            
            
        </script> 


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>3DHouse Painel de Controle</title>

    <!-- Bootstrap core CSS --> 
    
    <!-- Jasny bootstrap -->
   
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet"><!--pega a utl de base do site -->
    <link href="<?php echo base_url('assets/css/paineladmin.css') ?>" rel="stylesheet">   
    <link href="<?php echo base_url('assets/css/offcanvas.css') ?>" rel="stylesheet">  
    

    <!-- Custom styles for this template -->
 <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
        <link rel="stylesheet" href="<?php echo base_url('assets/css/richtext.min.css');?>">
    <!-- javascript de consulta cep-->

    <!-- Custom styles for this template -->


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
   <div id="top-nav" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('principal'); ?>">Painel Administrativo</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="fa fa-user-circle" style="margin-right: 10px;"></i><?php   $query  = $this->ion_auth->user()->row(); echo $query->nomeusuario;  ?> <span class="caret"></span></a>
                    <ul id="g-account-menu" class="dropdown-menu" role="menu"> 
                     <?php   $query  = $this->ion_auth->user()->row();  $query->codusuario; ?>
                       <li><?php echo anchor("auth/edit_user/".$query->codusuario, 'Editar perfil') ;?></li>
                    </ul>
                </li>
              <li><a href="<?php echo site_url('auth/logout') ?>"  <i class="fa fa-sign-out"></i>Sair</a></li>
            </ul>
        </div>
    </div>
    <!-- /container -->
</div>

<!-- /Header -->


<!-- menu navbar -->


    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">
        
           <div class="col-xs-12 col-sm-9">
              <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
              </p>
                   <div class="jumbotron"> 
                     {MENSAGEM_SISTEMA_ERRO} <!-- mensagens do sistema para erro e sucesso  -->
                      {MENSAGEM_SISTEMA_SUCESSO}
                       {CONTEUDO}<!-- Mostra o conteudo do sistema-->
                  </div><!-- div jumbotron --> 

                       
            </div><!--/.col-xs-12.col-sm-9-->


        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
          <div class="list-group"> 
          <?php if ($this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {?>
            <a  class="list-group-item active">Manutenção de Usuários</a> 
            <!--  chamada da pagina por metodo url--> 
            <a href="<?php echo site_url('Auth/index') ?>" class="list-group-item">Usuários</a>   
           <?php } ?>
           
            <a  class="list-group-item active">Manutenção de Produtos</a> 
            <a href="<?php echo site_url('painel/Produto') ?>" class="list-group-item">Produtos</a> 
            
            <a href="<?php echo site_url('painel/Atributo') ?>" class="list-group-item">Atributo</a>
            <a href="<?php echo site_url('painel/Departamento') ?>" class="list-group-item">Departamentos</a> 
            <a href="<?php echo site_url('painel/Tipoatributo') ?>" class="list-group-item">Tipos de Atributos</a>   
            <?php if ($this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {?>
             <a  class="list-group-item active">Mais Opções</a> 
            <a href="<?php echo site_url('Gallery') ?>" class="list-group-item">Banners de Imagem</a> 
             <a href="<?php echo site_url('painel/Formaentrega') ?>" class="list-group-item">Formas de Entrega</a> 
             <a href="<?php echo site_url('painel/Formapagamento') ?>" class="list-group-item">Formas de Pagamento</a>
            <?php } ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

      <hr>

      <footer> 
      
        <p>&copy; 2016 3DHouse, Inc.</p>
      </footer>

    </div><!--/.container-->
<!-- -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Pega a url base do site -->
    
   
    
    
    <script src="<?php echo base_url('assets/js/canvas-to-blob.min.js')?>"></script>  

        <!-- Placed at the end of the document so the pages load faster --> 

    
       
    <script src="<?php echo base_url('assets/js/canvas-to-blob.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.maskMoney.js')?>"></script>
    <script src="<?php echo base_url('assets/js/offcanvas.js')?>"></script> 
   <script src="<?php echo base_url('assets/js/jquery.richtext.js');?>"></script>
   <script src="<?php echo base_url('assets/js/loja.js');?>"></script>
    <!-- jquery do plug in de fotos dos produtos -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    
   
  </body>
</html> 
<!-- passo 2 ir ate config.php habilitar a variavel que sobreescreve as funções do codeigniter
 -->