<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class Frete extends CI_Controller {  
//classe principal
    public function __construct(){ 
// peso em KG. exemplo: 0.5
// comprimento, largura, altura, diametro: em centimetros
        
        
        
       

        parent::__construct();
         $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega  
        $this->load->library(array('ion_auth','form_validation', 'curl'));
    $this->load->helper(array('url','language', 'html')); 
     $this->load->model('Ion_auth_model', 'IonautH');  
     $this->load->model('Imagens_Model'); 
     $this->load->model ('Produto_Model', 'ProdutoM'); 

     $this->load->library('cart');

     $this->load->model('Departamento_Model','DepartamentoM');   
            
        
        
    } 

    public function calculafretecarrinho($cdservico, $cepdestino){ 
        
        $this->layout=null;
        $total = 0.0;

        foreach ($this->cart->contents() as $items){

            $produto = $this->ProdutoM->get_ficha_produto($items['id']); 
            
            $loja = $this->db->limit(1)->get('loja')->row();
            
     
            $resultado = $this->integra_correios($cdservico,$loja->ceploja,$cepdestino,$produto['pesoproduto'],
                $produto['comprimentoproduto'],$produto['alturaproduto'],$produto['larguraproduto'],$produto['diametroproduto'],$produto['formatoproduto'],$produto['maopropriaproduto'],$produto['valorproduto'],'N');

            if($resultado["cep"])
                $total+=($resultado['cep']['valor'] * $items['qty']);

       }

        $this->output->set_content_type('application/json')->set_output( json_encode($total));

    } 

    private function integra_correios( $cdservico, $ceporigem, $cepdestino, $peso, $comprimento, $altura, $largura, $diametro, $formato, $maopropria, $valordeclarado, $avisorecebimento){  
        $retorno = array();

        $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$ceporigem."&sCepDestino=".$cepdestino."&nVlPeso=".$peso."&nCdFormato=".$formato."&nVlComprimento=".$comprimento."&nVlAltura=".$altura."&nVlLargura=".$largura."&sCdMaoPropria=".$maopropria."&nVlValorDeclarado=".$valordeclarado."&sCdAvisoRecebimento=".$avisorecebimento."&nCdServico=".$cdservico."&nVlDiametro=".$diametro."&StrRetorno=xml&nIndicaCalculo=3";
            
            $frete = simplexml_load_string(file_get_contents($url));



        if($frete->cServico->Erro == 0){
            $retorno['cep'] = ['valor' =>  $frete->cServico->Valor,
                         'prazo' => $frete->cServico->PrazoEntrega];
            
            
        } else {
            $retorno['cep']['mensagem_erro'] =$frete->cServico[0]->MsgErro;
            $retorno['cep']['codigo_erro'] = $frete->cServico[0]->Erro;
          } 
 
        return $retorno;
    } 

} 
/*
[id] => 67
            [name] => Transformers
            [price] => 30
            [valorpromocional] => 0.00
            [codsku] => 40
            [tipo] => uploadprodutofoto/710x528_10152373_6868562_1493419846.jpg
            [options] => 
            [qty] => 1
            [rowid] => 735b90b4568125ed6c3f678819b6e058
            [subtotal] => 30


*/