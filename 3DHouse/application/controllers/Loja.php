<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class Loja extends CI_Controller {  
//classe principal
    public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */ 

        parent::__construct(); 
        $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega  
        $this->load->library(array('ion_auth','form_validation', 'curl'));
    $this->load->helper(array('url','language', 'html')); 
     $this->load->model('Ion_auth_model', 'IonautH');  
     $this->load->model('Imagens_Model'); 
     $this->load->model ('Produto_Model', 'ProdutoM'); 

     $this->load->library('cart');

     $this->load->model('Departamento_Model','DepartamentoM');  

                          

        
    }
    public function index(){ //função publica  
    		  //$this->load->view('loja/header');  


      //variavel que armazena as imagens do banner
      $data = [
         'imagens'   => $this->Imagens_Model->all(),
         'departamentoM'=> $this->DepartamentoM,
         
      ]; 

      $this->load->view('loja/slider', $data);
      // fim banner de imagens  


      // listagem de produtos 


            //$this->load->model ( 'Produto_Model', 'ProdutoM' );
            $data = array();
            $produto = $this->ProdutoM->getvitrine(); 

        

        

           foreach($produto as $p){ 
            $data['BLC_LINHA'][] = array( 
              "NOMEPRODUTO" => substr($p->nomeproduto, 0,18), 
              "PRECO" => $p->valorproduto, 
              "DESCRICAO" =>  substr($p->resumoproduto, 0,30),
              "URLFOTO" => $p->tipo, 
              "URLPRODUTO" => site_url('fichatecnicaproduto/show/'.$p->codproduto), 
              "URLPRODUTOC" => site_url('carrinho/adicionar/'.$p->codproduto), 
              "NOME" => $p->primeironome,  
              "SOBRENOME"=> $p->sobrenome, 

                );

              }
 $this->parser->parse('loja/vitrineproduto', $data); 

//print_r ($produto);


//fim produtos      


// produtos promocionais 




            //$this->load->model ( 'Produto_Model', 'ProdutoM' );
             $datapromo = array();
            $produtopro = $this->ProdutoM->getvitrinepromocional(); 

         
//print_r ($produtopro); 
//die()

        

           foreach($produtopro as $pp){ 
            $datapromo['BLC_LINHA'][] = array( 
              "NOMEPRODUTO" => substr($pp->nomeproduto, 0,18), 
              "PRECO" => $pp->valorproduto, 
              "PRECOPROMOCIONAL" => $pp->valorpromocional,  
              "DESCRICAO" => substr($pp->resumoproduto, 0,18), 
              "URLFOTO" => $pp->tipo,
              "URLPRODUTO" => site_url('fichatecnicaproduto/show/'.$pp->codproduto), 
              "NOME" => $pp->primeironome,  
              "SOBRENOME"=> $pp->sobrenome, 
                );

            

          }


              $this->parser->parse('loja/produtospromocional', $datapromo);  
              
              


        
       
      
        #parametro false indica se irá vir uma ou todas as linhas da pesquisa 
        #variável $pagina váriavel utilizada para paginação
        
            
        

    //$this->load->view('painel/principal.php'); 
   
       
 
  }    
  public function logout()
  {
    $this->data['title'] = "Logout";

    // log the user out
    $logout = $this->ion_auth->logout();

    // redirect them to the login page
    $this->session->set_flashdata('message', $this->ion_auth->messages());
    redirect('loja', 'refresh');
  }
}
