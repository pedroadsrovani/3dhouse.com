<?php
/**
 * CodeIgniter Boleto
 *
 * @package    CodeIgniter Boleto
 * @author     Natan Felles
 * @link       https://github.com/natanfelles/codeigniter-boleto
 */
//defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Boleto_controller extends Customer_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('boleto'); 
		
	}


	public function index()
	{
		$dados = array(
			// Informações necessárias para todos os bancos
			'dias_de_prazo_para_pagamento' => 5,
			'taxa_boleto'                  => 1,
			'pedido'                       => array(
				'nome'           => 'Venda de produtos',
				'quantidade'     => $this->input->post('quantidade'),
				'valor_unitario' => $this->input->post('valor'),
				'numero'         => 10000000025,
				'aceite'         => 'N',
				'especie'        => 'R$',
				'especie_doc'    => 'DM',
			),
			'sacado'                       => array(
				'nome'     => $this->input->post('nome'),
				'endereco' => $this->input->post('endereco'),
				'cidade'   => $this->input->post('cidade'),
				'uf'       => $this->input->post('uf'),
				'cep'      => $this->input->post('cep'),
			),
			// Informações necessárias que são específicas do Banco do Brasil
			'variacao_carteira'            => '019',
			'contrato'                     => 999999,
			'convenio'                     => 7777777,
		);

		// Gera o boleto
		$this->boleto->cef($dados);
	}

}
