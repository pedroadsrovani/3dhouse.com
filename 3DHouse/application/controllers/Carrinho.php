<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class Carrinho extends CI_Controller {  
//classe principal
    public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */ 

        parent::__construct(); 
        $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega  
        $this->load->library(array('ion_auth','form_validation', 'curl'));
    $this->load->helper(array('url','language', 'html')); 
     $this->load->model('Ion_auth_model', 'IonautH');  
     $this->load->model('Imagens_Model'); 
     $this->load->model ('Produto_Model', 'ProdutoM'); 
     $this->load->model('Sku_Model', 'SkuM');  
     $this->load->library('cart');

     $this->load->model('Departamento_Model','DepartamentoM');  

                          

        
    } 

    public function index(){ 

/*echo '<pre>';  
print_r($this->cart->contents());
echo '</pre>';
 die(); */    
 

    //print_r($data); die(); 
 if($this->cart->contents()){ 

 $this->load->view('loja/carrinho');
}else{ 
   
   $this->load->view('loja/carrinhovazio');
}
 }
       
    public function adicionar(){ //função publica  

        $dados_sku = explode(":",$this->input->post('sku') );

    $data = array('id' => $this->input->post('codproduto'), 
     'name'=> $this->input->post('nomeproduto'), 
     'price' => $this->input->post('valorproduto'), 
     'valorpromocional' => $this->input->post('valorpromocional'), 
     'codsku' => $dados_sku[0],  
     'tipo' =>$this->input->post('tipo'), 
     'primeironome' => $this->input->post('primeironome'), 
     'sobrenome' => $this->input->post('sobrenome'), 
     'referencia' => $dados_sku[2], 
     'nomeatributo' => $dados_sku[1],
     'options' => null, 
     'qty' => $this->input->post('quantidade')); 
     $this->cart->insert($data); 

    redirect("carrinho"); 



} 


public function finalizar_venda(){

    //inicia transacao
    //db->insert na tabela venda recuperando o id da venda gerada



    foreach($this->cart->contents() as $item ){

            //db->insert into venda_item
    }

    //db->commit();

}



public function delete($rowid){

    $data = array('rowid' => $rowid, 'qty' => 0); 
    $this->cart->update($data); 
    redirect(base_url('carrinho'));
} 

}