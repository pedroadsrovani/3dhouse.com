<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formaentrega extends  Auth_Controller {
   public function __construct(){ 
        parent::__construct(); 
        $this->layout = LAYOUT_PAINELADMIN;
       $this->load->model('FormaEntrega_Model', 'EntregaM'); 
      
   }  

   public function index(){  #exibição de itens de tipo de atributo 
   
if (!$this->ion_auth->logged_in())
    {
      // redirect them to the login page
      redirect('auth/login', 'refresh');
    }
    elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {
      // redirect them to the home page because they must be an administrator to view this
      return $this->load->view('painel/redirecionar');
    }
    else
    {    
       $data = array(); 
       $data['URLADICIONAR'] = site_url('painel/formaentrega/adicionar'); 
       $data['URLLISTAR'] = site_url('painel/formaentrega');   
       $data['BLC_DADOS'] = array();  
       $data['BLC_SEMDADOS'] = array(); 
       $data['BLC_PAGINAS'] = array();
       $pagina = $this->input->get('pagina'); 

       if(!$pagina){ 

         $pagina = 0;
       }else{ 

           $pagina = ($pagina - 1) * LINHAS_PESQUISA_PAINELADMIN;//para exibir os proximos 30 itens
       }

       $res = $this->EntregaM->get(array(), FALSE, $pagina);#array no metodo get para paginação qie retorna o admin

       if($res){ 
           foreach($res as $r){ 
               $data['BLC_DADOS'][] = array( 
                  "NOME" => $r->nomeformaentrega, 
                  "URLEDITAR" => site_url('painel/formaentrega/editar/'.$r->codformaentrega),  
                  "HABILITADO" => $r->habilitaformaentrega,
                  "URLEXCLUIR" => site_url('painel/formaentrega/excluir/'.$r->codformaentrega) 
               );//se vier dados sera adicionado ao array
           }
           
       }else{  
           $data['BLC_SEMDADOS'][] = array();


       }   

       #indice de paginação

       $totalItens = $this->EntregaM->getTotal(); 
       $totalPaginas = ceil($totalItens / LINHAS_PESQUISA_PAINELADMIN); 

       $indicePg = 1;  
       $pagina = ($pagina ==0)?1:$pagina;

           if($totalPaginas > $pagina){ 
               $data['HABPROX'] = null; 
               $data['URLPROXIMO'] = site_url('painel/formaentrega?pagina='.$pagina + 1);
           }else{  
               $data['HABPROX'] = 'disabled';
               $data['URLPROXIMO'] = '#';
           } 

           if($pagina<=1){ 
               $data['HABANTERIOR'] = 'disabled'; 
               $data['URLANTERIOR'] = '#';
           }else{ 
                $data['HABANTERIOR'] = null; 
               $data['URLANTERIOR'] = site_url('painel/formaentrega?pagina='.$pagina - 1); 

           }


         while($indicePg <= $totalPaginas){ 
             $data['BLC_PAGINAS'][] = array( 
                 "LINK" => ($indicePg==$pagina)?'active':null,
                 "INDICE" => $indicePg,  
                 "URLLINK" =>  site_url('painel/formaentrega?pagina='.$indicePg)

             ); 
             $indicePg++;
         }


       $this->parser->parse('painel/formaentrega_listar', $data); 
      }
   }

   public function adicionar(){ 
       $data = array();  
       $data['ACAO'] = 'Novo';
       $data['codformaentrega'] = ''; 
       $data['nomeformaentrega'] = ''; 
       $data['chk_habilitaformaentrega'] = 'null';  
       $data['codigocorreiosformaentrega'] = '';
       
         
       $this->setURL($data); #evita a duplicação desses dados para outros formularios de update, select etc..

       $this->parser->parse('painel/formaentrega_form', $data);


   }  

#função editar 

public function editar($id) {
		$data						= array();
		$data['ACAO']				= 'Edição';
		
		$res	= $this->EntregaM->get(array("codformaentrega" => $id), TRUE);
		
		if ($res) {
			foreach($res as $chave => $valor) {
				$data[$chave] = $valor;
			}
			
      if($res->habilitaformaentrega == 'S'){ 
        $data['chk_habilitaformaentrega'] = 'checked="checked"';  
      }
			
			
		} else {
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		}
		
		$this->setURL($data);
		
		$this->parser->parse('painel/formaentrega_form', $data);
	}

   #função salvar 

   public function salvar(){ 

       $codformaentrega = $this->input->post('codformaentrega'); 
       $nomeformaentrega = $this->input->post('nomeformaentrega');  
       $habilitaformaentrega = $this->input->post('habilitaformaentrega');  
       $codigocorreiosformaentrega = $this->input->post('codigocorreiosformaentrega'); 
       
       if(!$habilitaformaentrega){ 
           
           $habilitaformaentrega = 'N';
       }
       #verifica a validação do tipo atributo
       $erro = FALSE; 
       $mensagem = null;  


           if(!$nomeformaentrega){ 
               $erro  = TRUE; 
               $mensagem .= "Informe o nome da forma de entrega\n"; 
           } 

           

         if(!$erro){  
              $itens = array( 
       
       "nomeformaentrega" => $nomeformaentrega, 
       "codigocorreiosformaentrega" => $codigocorreiosformaentrega, 
         "habilitaformaentrega" => $habilitaformaentrega
       
              ); 


              
               if($codformaentrega){  
                  $codformaentrega =  $this->EntregaM->update($itens, $codformaentrega);

               } else{ 

                 $codformaentrega = $this->EntregaM->post($itens);
               }  


               if($codformaentrega){
                   
                   $this->session->set_flashdata('sucesso', 'Dados inseridos com sucesso!'); 
                   redirect('painel/formaentrega');


               } else{ 

                   $this->session->set_flashdata('erro', 'Ocorreu um erro ao realizar a operação.'); 
                   
                   if($codformaentrega){  
                       redirect('painel/formaentrega/editar/'.$codformaentrega); 

                   } else{ 
                   redirect('painel/formaentrega/adicionar'); 
                   }

               }


                   
               } else{  

                   $this->session->set_flashdata('erro', $mensagem); 
                    if($codformaentrega){  
                       redirect('painel/formaentrega/editar/'.$codformaentrega); 

                   } else{ 
                   redirect('painel/formaentrega/adicionar'); 
                   }


               }

         }
   

   #metodo privado para a a URL 
   private function setURL(&$data){ 
       $data['URLLISTAR'] = site_url('painel/formaentrega'); 
       $data['ACAOFORM'] = site_url('painel/formaentrega/salvar');
   } 

   #excluir administradores 

   public function excluir($id){  
       $res = $this->EntregaM->delete($id); 
       if($res){ 
           $this->session->set_flashdata('sucesso', 'Forma de entrega removido!');
       }else{ 
           $this->session->set_flashdata('erro', 'Forma de entrega não foi removido! Tente novamente'); 
           
       }
        redirect('painel/formaentrega');
   }
}