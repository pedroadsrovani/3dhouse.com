<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends  Vendor_Controller {#classe departamento
	
	public function __construct() {#construtor do departamento
		parent::__construct();
		$this->layout	= LAYOUT_PAINELADMIN;#carregamento do template
		$this->load->model('Departamento_Model', 'DepartamentoM');#carregamento do model de departamento
	}
	/* função index declaração dos blocos de conetudo e paginação   */
	public function index() { 
		#definição dos blocos de conteudo e lincagem 
		
		$data					= array();
		$data['URLADICIONAR']	= site_url('painel/departamento/adicionar');#link para chamada da função adicionar 
		$data['URLLISTAR']		= site_url('painel/departamento');#kink para chamada do função index
		$data['BLC_DADOS']		= array();
		$data['BLC_SEMDADOS']	= array();
		$data['BLC_PAGINAS']	= array();
		
		$pagina			= $this->input->get('pagina');#definição da variavel pagina
		
		if (!$pagina) {#se não estiver setada
			$pagina = 0;
		} else {
			$pagina = ($pagina-1) * LINHAS_PESQUISA_PAINELADMIN;//para exibir os proximos 7 itens da tabela
		}
		
		$res	= $this->DepartamentoM->get(array(), FALSE, $pagina);;#variável que irá receber dados contidos na tabela departamento os dados vindos da tabela são adicionados em um array para realizar a função de paginação
       #parametro false indica se irá vir uma ou todas as linhas da pesquisa 
       #variável $pagina váriavel utilizada para paginação

		if ($res) {#verifica se vier dados
			foreach($res as $r) { /* se os dados vierem o for each irá percorrer cada linha e adicionará aa variaável $data */
				$data['BLC_DADOS'][] = array(
					"NOME"		=> $r->nomedepartamento,#lista os nomes do deprtamento
					"NOMEPAI"	=> (empty($r->nomepai))?'-':$r->nomepai,#lista o nome do depratmento pai 
					#botoes de lincagem de funções editar e excluir
					"URLEDITAR"	=> site_url('painel/departamento/editar/'.$r->codepartamento),
					"URLEXCLUIR"=> site_url('painel/departamento/excluir/'.$r->codepartamento)
				);
			}
		} else {
			$data['BLC_SEMDADOS'][] = array();
		}
	#paginação da pagina de departamento
		$totalItens		= $this->DepartamentoM->getTotal();
		$totalPaginas	= ceil($totalItens/LINHAS_PESQUISA_PAINELADMIN);
		
		$indicePg		= 1;
		$pagina			= ($pagina==0)?1:$pagina;
		
		if ($totalPaginas > $pagina) {
			$data['HABPROX']	= null;
			$data['URLPROXIMO']	= site_url('painel/departamento?pagina='.$pagina+1);
		} else {
			$data['HABPROX']	= 'disabled';
			$data['URLPROXIMO']	= '#';
		}
		
		if ($pagina <= 1) {
			$data['HABANTERIOR']= 'disabled';
			$data['URLANTERIOR']= '#';
		} else {
			$data['HABANTERIOR']= null;
			$data['URLANTERIOR']= site_url('painel/departamento?pagina='.$pagina-1);
		}
		
		
		
		while ($indicePg <= $totalPaginas) {
			$data['BLC_PAGINAS'][] = array(
				"LINK"		=> ($indicePg==$pagina)?'active':null,
				"INDICE"	=> $indicePg,
				"URLLINK"	=> site_url('painel/departamento?pagina='.$indicePg)
			);
			
			$indicePg++;
		}
		
		$this->parser->parse('painel/departamento_listar', $data); 

	}
/* fim da função index */ 
/* função adicionar */	
	public function adicionar() {
	#dados vindos do formulário  e variáveis dos formularios
		$data							= array();
		$data['ACAO']					= 'Novo';
		$data['BLC_DEPARTAMENTOS']		= array();
		$data['hab_coddepartamentopai']	= null;
		$data['codepartamento']		= '';
		$data['nomedepartamento']		= '';
		
		
		$dep	= $this->DepartamentoM->get(array("d.coddepartamentopai IS NULL" => null));# dep recebe e a condição para receber é o coddepartamentopai ser nullo
		
		foreach($dep as $d){ #adicionar os itens
			$data['BLC_DEPARTAMENTOS'][] = array(
				"CODDEPARTAMENTO"		=> $d->codepartamento,
				"NOME"					=> $d->nomedepartamento,
				"sel_coddepartamentopai"=> null #nulo pois o adicionar um item novo pode-se adicionar um novo departamentopai 
			);
		}
		
		$this->setURL($data);
		
		$this->parser->parse('painel/departamento_form', $data);
	}
	/*  função editar   */
	public function editar($id) {
		$data							= array();
		$data['ACAO']					= 'Edição';
		$data['BLC_DEPARTAMENTOS']		= array();
		
		$totalFilho = $this->DepartamentoM->getTotal(array("coddepartamentopai" => $id));
		
		$res	= $this->DepartamentoM->get(array("d.codepartamento" => $id), TRUE);
		
		if ($totalFilho > 0) {/*verifica e desabilita departamentos com pai */
			$data['BLC_DEPARTAMENTOS']		= array();
			$data['hab_coddepartamentopai']	= 'disabled="disabled"';
		} else {# se não tiver filhos 
			$dep	= $this->DepartamentoM->get(array("d.coddepartamentopai IS NULL" => null, "d.codepartamento != " => $id));/* crrega os departamento pais nulos  
			"d.codepartamento != " => $id esse parametro não permitirá que um departamento seja departamentopai dele mesmo
			*/
			
			foreach($dep as $d){
				$data['BLC_DEPARTAMENTOS'][] = array( /* se não tiver departamentos com pai atribui com foreach*/
					"CODDEPARTAMENTO"		=> $d->codepartamento,
					"NOME"					=> $d->nomedepartamento,
					"sel_coddepartamentopai"=> ($res->coddepartamentopai==$d->codepartamento)?'selected="selected"':null# se o departamentopai for igual o codepartaento seleciona o item
				);
			}
		}
		

		if ($res) {
			foreach($res as $chave => $valor) {
				$data[$chave] = $valor;
			}
			
		} else {
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		}
		
		$this->setURL($data);
		
		$this->parser->parse('painel/departamento_form', $data);
	}
	/* fim do editar */ 
	/* metodo salvar */
	public function salvar() {
		
		$codepartamento		= $this->input->post('codepartamento');#receber o codigo do departamento
		$nomedepartamento	= $this->input->post('nomedepartamento');#recebe o nome do departamento
		$coddepartamentopai	= $this->input->post('coddepartamentopai');#recebe o nome do departamento pai
		
		$erros			= FALSE;
		$mensagem		= null;
		
		if (!$nomedepartamento) {
			$erros		= TRUE;
			$mensagem	.= "Informe nome do departamento.\n";
		}
		if (!$erros) {
			$itens	= array(
				"nomedepartamento"	=> $nomedepartamento,
				"coddepartamentopai"=> ($coddepartamentopai)?$coddepartamentopai:null
			);
			
			
			if ($codepartamento) {
				$codepartamento = $this->DepartamentoM->update($itens, $codepartamento);
			} else {
				$codepartamento = $this->DepartamentoM->post($itens);
			}
			
			if ($codepartamento) {
				$this->session->set_flashdata('sucesso', 'Dados inseridos com sucesso.');
				redirect('painel/departamento');
			} else {
				$this->session->set_flashdata('erro', 'Ocorreu um erro ao realizar a operação.');
				
				if ($codepartamento) {
					redirect('painel/departamento/editar/'.$codepartamento);
				} else {
					redirect('painel/departamento/adicionar');
				}
			}
		} else {
			$this->session->set_flashdata('erro', nl2br($mensagem));
			if ($codepartamento) {
				redirect('painel/departamento/editar/'.$codepartamento);
			} else {
				redirect('painel/departamento/adicionar');
			}
		}
		
	}/* fim do metodo salvar */
	
	/* funçã seturl para base url  */
	private function setURL(&$data) {
		$data['URLLISTAR']	= site_url('painel/departamento');
		$data['ACAOFORM']	= site_url('painel/departamento/salvar');
	}
	/* função excluir */
	public function excluir($id) {
		$res = $this->DepartamentoM->delete($id);
		
		if ($res) {
			$this->session->set_flashdata('sucesso', 'Departamento removido com sucesso.');
		} else {
			$this->session->set_flashdata('erro', 'Departamento não pode ser removido.');
		}
		
		redirect('painel/departamento');
	}
	
}