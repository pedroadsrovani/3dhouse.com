<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produto extends  Vendor_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->layout	= LAYOUT_PAINELADMIN; 
		#models carregados 
		//$this->load->database();
		$this->load->model('Produto_Model', 'ProdutoM'); 
		$this->load->model('TipoAtributo_Model', 'TipoAtributoM'); 
        $this->load->model('Departamento_Model', 'DepartamentoM'); 
		$this->load->model('ProdutoDepartamento_Model', 'ProdDepM');   
		$this->load->model('Sku_Model', 'SkuM');  
		$this->load->model('ProdutoFoto_Model', 'PfotoM');  
	    $this->load->model('Ion_auth_model', 'IonautH');  
	    $this->load->library(array('ion_auth')); 
		//library  do jquery file upload   

      $this->load->helper(['url','html','form']);
     
      $this->load->library(['form_validation', 'session']);

		
		
		
		/* ------------------ */
		
	}
	/* função index declaração dos blocos de conetudo e paginação   */
	public function index() { 
		#definição dos blocos de conteudo e links utilizados 
		
		$data					= array();
		$data['URLADICIONAR']	= site_url('painel/produto/adicionar');
		$data['URLLISTAR']		= site_url('painel/produto');
		$data['BLC_DADOS']		= array();
		$data['BLC_SEMDADOS']	= array();
		$data['BLC_PAGINAS']	= array();
	   
		$pagina			= $this->input->get('pagina');
	
		if (!$pagina) {
			$pagina = 0;
		} else {
			$pagina = ($pagina-1) * LINHAS_PESQUISA_PAINELADMIN;
		}
	
		$res	= $this->ProdutoM->get(array(), FALSE, $pagina, $this->ion_auth->user()->row()->codusuario	);#pega os itens de produtos 
	   
		if ($res) {
			foreach($res as $r) {
				$data['BLC_DADOS'][] = array(
						"CODPRODUTO"=> $r->codproduto,
						"NOME"		=> $r->nomeproduto, 
						"PRECO"     => $r->valorproduto,
						"URLEDITAR"	=> site_url('painel/produto/editar/'.$r->codproduto),
						"URLEXCLUIR"=> site_url('painel/produto/excluir/'.$r->codproduto), 
						"URLATRIBUTOS" => site_url('painel/produto/atributos/'.$r->codproduto), // lincagem da função atributo com sku 
						"URLUPLOAD" => site_url('painel/produto/uploadfoto/'.$r->codproduto), 
						"URLVINCULAIMAGEMSKU" => site_url('painel/produto/fotosku/'.$r->codproduto)
						
				);
			}
		} else {
			$data['BLC_SEMDADOS'][] = array();
		}
	
		$totalItens		= $this->ProdutoM->getTotal(array('usuario_id'=> $this->ion_auth->user()->row()->codusuario));
		$totalPaginas	= ceil($totalItens/LINHAS_PESQUISA_PAINELADMIN);
	
		$indicePg		= 1;
		$pagina			= ($pagina==0)?1:$pagina;
	
		if ($totalPaginas > $pagina) {
			$data['HABPROX']	= null;
			$data['URLPROXIMO']	= site_url('painel/produto?pagina='.$pagina+1);
		} else {
			$data['HABPROX']	= 'disabled';
			$data['URLPROXIMO']	= '#';
		}
	
		if ($pagina <= 1) {
			$data['HABANTERIOR']= 'disabled';
			$data['URLANTERIOR']= '#';
		} else {
			$data['HABANTERIOR']= null;
			$data['URLANTERIOR']= site_url('painel/produto?pagina='.$pagina-1);
		}
	
	
	
		while ($indicePg <= $totalPaginas) {
			$data['BLC_PAGINAS'][] = array(
					"LINK"		=> ($indicePg==$pagina)?'active':null,
					"INDICE"	=> $indicePg,
					"URLLINK"	=> site_url('painel/produto?pagina='.$indicePg)
			);
				
			$indicePg++;
		}
	
		$this->parser->parse('painel/produto_listar', $data);
	}/* fim do index*/
    /* função adicionar*/
	public function adicionar() {
	
		$data						= array();
		$data['ACAO']				= 'Novo';
		$data['codproduto']			= null;
		$data['nomeproduto']		= null;
		$data['resumoproduto']		= null;
		$data['fichaproduto']		= null;
		$data['valorproduto']		= '0,00';
		$data['valorpromocional']	= '0,00';
		$data['BLC_TIPOATRIBUTOS']	= array(); 
        $data['BLC_DEPARTAMENTOPAI']	= array();
		$data['des_tipoatributo']	= null; 
		$data['pesoproduto']		= null; 
		$data['comprimentoproduto']		= null; 
		$data['alturaproduto']		= null;
		$data['larguraproduto']		= null; 
		$data['diametroproduto']		= null;
		$tipo	= $this->TipoAtributoM->get(array(), FALSE, 0, FALSE);/* carrega o que tiver no tipoatributo com parametros false para não retornar  */
		
		foreach($tipo as $t){
			        $data['BLC_TIPOATRIBUTOS'][] = array(
					"CODTIPOATRIBUTO"		=> $t->codtipoatributo,
					"NOME"					=> $t->nometipoatributo,
					"sel_codtipoatributo"	=> null
			);
		}
		
		setURL($data, 'produto'); #referencia a função no helper, produto é o controler especificado como parametro

        /*  BUSCAR TODOS OS DEPARTAMENTOS */ 
        /*existe duas buscas uma para departamento pai e outra para filho ou seja os deparatamentos 
          que não são incrementados no departamento pai  existindo dois foreach e dois vetores um pai e outro filho
		*/
        $depPai = $this->DepartamentoM->get(array("d.coddepartamentopai IS NULL" => NULL), FALSE, 0, FALSE); #busca todos os departamentos pai
	   if($depPai){ 
           foreach($depPai as $dp){
			   /*  define os elementos do filho*/ 
			   $aFilhos = array(); 
			       $deFilhos = $this->DepartamentoM->get(array("d.coddepartamentopai" => $dp->codepartamento), FALSE, 0, FALSE); #busca o departamento filho pertencente ao pai
	                    	 
						 if($deFilhos){ 
							 foreach($deFilhos as $df){ // percorre a variavel defilhos que armazena os departamentos filhos 
                                  $aFilhos[] =  array( #foreach armazena os dados no array aFilhos.
                                     "CODDEPARTAMENTOFILHO" => $df->codepartamento, 
                                     "NOMEDEPARTAMENTOFILHO" => $df->nomedepartamento, 
				                     "CODDEPARTAMENTOPAI" =>$df->coddepartamentopai, 
									 "chk_departamentofilho" => null
                                   );

							 }
						 }/* fim */
                        
                                  /*  define os elementos do pai   */
                                 $data['BLC_DEPARTAMENTOPAI'][] =  array( 
                                "CODDEPARTAMENTO" => $dp->codepartamento, 
                                "NOMEDEPARTAMENTO" => $dp->nomedepartamento, 
				                "BLC_DEPARTAMENTOFILHO" => $aFilhos, 
								"chk_departamentopai" =>   null
                                 );
                

           }
       }
		$this->parser->parse('painel/produto_form', $data);
	} 

/* função para listagem dos atributos dos produtos */ 




	/* função salvar */
	public function salvar() {
	
		$codproduto			= $this->input->post('codproduto');
		$nomeproduto		= $this->input->post('nomeproduto');
		$resumoproduto		= $this->input->post('resumoproduto');
		$fichaproduto		= $this->input->post('fichaproduto');
		$valorproduto		= $this->input->post('valorproduto');
		$valorpromocional	= $this->input->post('valorpromocional');
		$codtipoatributo	= $this->input->post('codtipoatributo');  
		$pesoproduto    	= $this->input->post('pesoproduto');  
		$comprimentoproduto	= $this->input->post('comprimentoproduto'); 
		$alturaproduto	    = $this->input->post('alturaproduto'); 
		$larguraproduto	    = $this->input->post('larguraproduto'); 
		$diametroproduto	= $this->input->post('diametroproduto');
		$usuario_id     	= $this->ion_auth->user()->row()->codusuario;
		
		$departamento       = $this->input->post('departamento');
	    
		$erros			= FALSE;
		$mensagem		= null;
	
		if (!$nomeproduto) {
			$erros		= TRUE;
			$mensagem	.= "Informe nome do produto.\n";
		}
		if (!$resumoproduto) {
			$erros		= TRUE;
			$mensagem	.= "Informe o resumo do produto.\n";
		}
		if (!$fichaproduto) {
			$erros		= TRUE;
			$mensagem	.= "Informe a ficha do produto.\n";
		}
		if (!$valorproduto) {
			$erros		= TRUE;
			$mensagem	.= "Informe o valor do produto.\n";
		} else {
			/*if ($valorpromocional) {#teste do valor promocionl maior que o valor do produto no cadastramento do produto
				if ($valorpromocional >= $valorproduto) {
					$erros		= TRUE;
					$mensagem	.= "Valor promocional não pode ser maior que o valor de venda.\n";
				}
			}*/
		}
	
		if (!$erros) {# se não houve erros seta o objeto de array de inserção
			
			$valorproduto 		= modificaDinheiroBanco($valorproduto);
			$valorpromocional 	= modificaDinheiroBanco($valorpromocional);
			
			$itens	= array(# os valores do produto e valores promocionais vem com , então cria-se uma função para formatar os valores no helper em funcoes_helper.php
					"nomeproduto"		=> $nomeproduto,
					"resumoproduto"		=> $resumoproduto,
					"fichaproduto"		=> $fichaproduto,
					"valorproduto"		=> $valorproduto,
					"valorpromocional"	=> $valorpromocional, 
					"usuario_id"        => $usuario_id, 
					"pesoproduto"       => $pesoproduto, 
					"comprimentoproduto" => $comprimentoproduto, 
					"alturaproduto" => $alturaproduto, 
					"larguraproduto" => $larguraproduto, 
					"diametroproduto" => $diametroproduto
				
			);
			
			if (!$codproduto) {#se o codigo do produto não for informado
				$itens["urlseo"] 			= url_title(strtolower($nomeproduto));#converte o nome do produto em uma string
				
				if ($codtipoatributo) {# se vier com codtipoatributo adicionar ao objeto itens o codtipoatributo 
					$itens["codtipoatributo"] 	= $codtipoatributo;
				}
			}
				
			if ($codproduto) {
				$codproduto = $this->ProdutoM->update($itens, $codproduto);
			} else {
				$codproduto = $this->ProdutoM->post($itens); 
                $referencia = "Ref-000".$codproduto; //referencia para produto que não for cadastrado com tipo de atributow

				if(!$codtipoatributo){  
				$sku = array(  // cria sku para produtos que não possui um tipo de atributo
					"referencia" => $referencia, 
					"quantidade" => 0, 
					"codproduto" => $codproduto
				);  
				$this->SkuM->post($sku);
			   }
			}	
			if ($codproduto) {
				$this->session->set_flashdata('sucesso', 'Dados inseridos com sucesso.');  
				  $this->ProdDepM->delete($codproduto);
				  
				  
				  //se o departamento for vinvulado 
				   foreach($departamento as $dep){  
					   $itenDepProd = array();//array que armazena os itens codproduto e codprodutodepartamento 
					   $itenDepProd['codproduto'] = $codproduto; 
					   $itenDepProd['codprodutodepartamento'] = $dep;
                        $this->ProdDepM->post($itenDepProd);//carrega o model de produtodepartamento pelo metodo post do model

				        }
			redirect('painel/produto'); 
                    
				 
			} else { 

				$this->session->set_flashdata('erro', 'Ocorreu um erro ao realizar a operação.');
	
				if ($codproduto) {
					redirect('painel/produto/editar/'.$codproduto);
				} else {
					redirect('painel/produto/adicionar');
				}
			}
		} else {
			$this->session->set_flashdata('erro', nl2br($mensagem));
			if ($codproduto) {
				redirect('painel/produto/editar/'.$codproduto);
			} else {
				redirect('painel/produto/adicionar');
			}
		}
	
	}/*fim do salvar*/
   /*  função editar*/
	public function editar($id) {
		$data						= array();
		$data['ACAO']				= 'Edição';
		$data['des_tipoatributo']	= 'disabled="disabled"';#desabilita o campo tipo atributo na edição
	
		$res	= $this->ProdutoM->get(array("codproduto" => $id), TRUE, 0, $this->ion_auth->user()->row()->codusuario);#parametro true indica que no model o get retornará apenas uma linha 
	
		if ($res) {
			foreach($res as $chave => $valor) {
				$data[$chave] = $valor;
			}
		} else {
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		}
		#chamada da função do helper  para  formatação de valores
		$data['valorproduto'] 		= modificaNumericValor($res->valorproduto);
		$data['valorpromocional']	= modificaNumericValor($res->valorpromocional);
	
		setURL($data, 'produto');
		
		$tipo	= $this->TipoAtributoM->get(array(), FALSE, 0, FALSE); #pega todos os tipos de atributos 
		#itera os tipos de atributos
		foreach($tipo as $t){
			$data['BLC_TIPOATRIBUTOS'][] = array(
					"CODTIPOATRIBUTO"		=> $t->codtipoatributo,
					"NOME"					=> $t->nometipoatributo,
					"sel_codtipoatributo"	=> ($t->codtipoatributo == $res->codtipoatributo)?'selected="selected"':null# automaticamente marca o campo tipo de atributo impossibilitando a troca 
			);
		}  

            // $aDepartamentosVinculados armazena os departamentos vinculados 
            
			$aDepartamentosVinculados = array(); //array de checagem de departamentos
                                                  //quando dp.codproduto equivale a id do produto
			$depVinc = $this->ProdDepM->get(array("dp.codproduto" => $id));  //pegar todos os departamentos do produto que esta sendo retornado pelo metodo editar do model
			   if($depVinc){ //se veio os dados corretamente
				   foreach($depVinc as $depv){ //armazena os deparatmentos vinculados
					 array_push($aDepartamentosVinculados, $depv->codprodutodepartamento); //pega o valor de codprodutodepartamento que está vinculado e insere mo array $aDepartamentosVinculados
				   /* 
                          array_push() trata array como uma pilha, e adiciona as variáveis passadas como argumentos no final de array. 
						   O tamanho do array aumenta de acordo com o número de variáveis adicionadas
				   */
				   
				   }
			   }
		   
        
        /*  BUSCAR TODOS OS DEPARTAMENTOS */ 
        /*existe duas buscas uma para departamento pai e outra para filho ou seja os deparatamentos 
          que não são incrementados no departamento pai  existindo dois foreach e dois vetores um pai e outro filho
		*/
        $depPai = $this->DepartamentoM->get(array("d.coddepartamentopai IS NULL" => NULL), FALSE, 0, FALSE); #busca o departamento pai
	   if($depPai){ 
           foreach($depPai as $dp){
			   /*  define os elementos do filho*/ 
			   $aFilhos = array(); 
			       $deFilhos = $this->DepartamentoM->get(array("d.coddepartamentopai" => $dp->codepartamento), FALSE, 0, FALSE); #busca o departamento filho
	                    	 
						 if($deFilhos){ 
							 foreach($deFilhos as $df){ 
                                  $aFilhos[] =  array( #foreach armazena os dados no array aFilhos.
                                     "CODDEPARTAMENTOFILHO" => $df->codepartamento, 
                                     "NOMEDEPARTAMENTOFILHO" => $df->nomedepartamento, 
				                     "CODDEPARTAMENTOPAI" =>$df->coddepartamentopai, 
									 "chk_departamentofilho" => (in_array($df->codepartamento, $aDepartamentosVinculados))?'checked="checked"':null//se este departamento estiver dentro do array fica marcado senao null
                                                                 //in_array verifica se determinado valor está em um determinado array
								   );                             // o array sempre é o segundo parametro

							 }
						 }/* fim */
                         // mesmo procedimento do departamento filho  para verificação de valores no array que armazena departamentos vinculados
                                  /*  define os elementos do pai   */
                                 $data['BLC_DEPARTAMENTOPAI'][] =  array( 
                                "CODDEPARTAMENTO" => $dp->codepartamento, 
                                "NOMEDEPARTAMENTO" => $dp->nomedepartamento, 
				                "BLC_DEPARTAMENTOFILHO" => $aFilhos, 
								"chk_departamentopai" =>   (in_array($dp->codepartamento, $aDepartamentosVinculados))?'checked="checked"':null//se este departamento estiver dentro do array fica marcado senao null
								 );                          //in_array verifica se determinado valor está em um determinado array 
								                             // o array sempre é o segundo parametro
                

           }
       }    


	
		$this->parser->parse('painel/produto_form', $data);
	}
	/* fim do editar */ 
	/* função excluir */
	public function excluir($id) {
		$res = $this->ProdutoM->delete($id);
	
		if ($res) {
			$this->session->set_flashdata('sucesso', 'Produto removido com sucesso.');
		} else {
			$this->session->set_flashdata('erro', 'Produto não pode ser removido.');
		}
	
		redirect('painel/produto');
	}/* fim do excluir */ 
    
	 /*metodo para vincular atributos nos produtos */
 
public function atributos($id) {
		$data						= array();//aray de dados
		$data['BLC_SEMVINCULADOS']	= array();
		$data['BLC_VINCULADOS']		= array();//mostra os atributos vinculados com produtos
		$data['BLC_SEMDISPONIVEIS']	= array();
		$data['BLC_DISPONIVEIS']	= array();//mostra os atributos disponíveis e vincuilados com skus
		$data['URLSALVAATRIBUTO']	= site_url('painel/produto/salvaatributo');//chama o metodo salvaratributo
		$data['URLLISTAR']			= site_url('painel/produto');
		
		$infoProduto	= $this->ProdutoM->get(array("codproduto" => $id), TRUE, 0, $this->ion_auth->user()->row()->codusuario );//carrega as informações do produto selecionado
		
		if ($infoProduto) {
			$data['NOMEPRODUTO']= $infoProduto->nomeproduto;//armazena o nome do produto
			$data['CODPRODUTO']	= $infoProduto->codproduto;//armazena o codigo do produto
		} else {
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		}
		
		if (empty($infoProduto->codtipoatributo)) { 
			//print_r($infoProduto); die();
			$resExistente 	= $this->SkuM->getPorProdutoSimples($id);//sku para produtos simples ou que não posssui um tipo de atributo
		} else {
			$resExistente 	= $this->SkuM->getPorProdutoAtributo($id);// sku para produtos que possui um tipo de atributo
		}
		//print_r($resExistente); die();
		if ($resExistente) {
			foreach($resExistente as $rEx) {
				$data['BLC_VINCULADOS'][] = array(
						"CODSKU"	 => $rEx->codsku,
						"REFERENCIA" => $rEx->referencia,
						"QUANTIDADE" => $rEx->quantidade,
						"DESCRICAO"	 => $rEx->nomeatributo// descrição será o nome do tipo de atributo
				);
			}
		} else {
			$data['BLC_SEMVINCULADOS'][] = array();
		}
		
		//ATRIBUTOS DISPONÍVEIS

		if (empty($infoProduto->codtipoatributo)) {
			$data['BLC_SEMDISPONIVEIS'][]	= array();
		} else {
			$atribDisponivel = $this->SkuM->getAtributosDisponiveis($id);//lista os  tipos de atributos disponíveis paa vinculação
			
			if ($atribDisponivel) {
				foreach($atribDisponivel as $aD) {
					$data['BLC_DISPONIVEIS'][] 	= array(
							"DESCRICAO" 	=> $aD->nomeatributo,
							"CODATRIBUTO"	=> $aD->codatributo
					);
				}
			} else {
				$data['BLC_SEMDISPONIVEIS'][]	= array();
			}
		}
		
		$this->parser->parse('painel/produtoatributo_listar', $data);
	}
	
	public function salvaatributo() {
		
		$codproduto = $this->input->post('codproduto');//captura o codigo do produto no formulario
		
		//INSERE ATRIBUTOS E TRANSFORMA EM SKU
		$atributo	= $this->input->post('atributo');
		
		foreach($atributo as $codatributo => $valores) {// foreach onde o codigo do atributo é um indice usado para vincular tipos de atributos com seus respectivos skus(referencia, quantidade e codproduto)
			if ((!empty($valores['referencia'])) || (!empty($valores['quantidade']))) {// verifica se os campos referecia e quantidade não estão nulos
				
				$sku = array(//array que armazena dados vindo do formulario produtoatributo
					"referencia" 	=> $valores['referencia'],
					"quantidade"	=> $valores['quantidade'],
					"codproduto"	=> $codproduto
				);
				
				$codsku = $this->SkuM->post($sku);//armazena dados na tabela sku
				
				if ($codsku) {
					$atributoSku = array(
						"codsku" 		=> $codsku,
						"codatributo"	=> $codatributo
					);
					$this->SkuM->postAtributo($atributoSku);//armazena dados na tabela skuatributo
				}
			}
		}
		
		//ATUALIZA SKUS
		$skuExistente	= $this->input->post('sku');//pega todos os campos no formulario com campos cm nome sku
		
		if ($skuExistente) {
			foreach($skuExistente as $codsku => $valores) {//percorre com foreach valores é usado para armazenar o indice utiliza-se ocodigo sku como indice
				if ($valores['remover'] === 'S') {//verifica se o checkbox possui valor s para ser removido
					$this->SkuM->delete($codsku);
				} else {
					
					$skuAtualiza = array(//dados a serem atualizados
							"referencia" => $valores['referencia'],
							"quantidade" => $valores['quantidade']
					);

					$this->SkuM->update($codsku, $skuAtualiza);//atualiza dados na tabela sku
				}
			}
		}
		
		$this->session->set_flashdata('sucesso', 'dados salvos com sucesso.');
		
		redirect('painel/produto/atributos/'.$codproduto);
	} 

//função de salvar fotos no bd 

 public function uploadfoto($id) {  
  $infoProduto	= $this->ProdutoM->get(array("codproduto" => $id), TRUE,0,$this->ion_auth->user()->row()->codusuario);//carrega as informações do produto  
 
     if($infoProduto){  
      $data[ 'produtofoto']   = $this->PfotoM->get_by_product($id); 
      $data['NOMEPRODUTO'] = $infoProduto->nomeproduto; 
      $data['CODPRODUTO']  = $infoProduto->codproduto; 
     } 

    
      
     $this->load->view('painel/produtofoto', $data);
     
  }

   public function add($product_id) {$rules =    [
                    
                    [
                            'field' => 'fotoprincipal',
                            'label' => 'Fotoprincipal',
                            'rules' => 'required'
                    ]
               ];

      $this->form_validation->set_rules($rules);

      if ($this->form_validation->run() == FALSE)
      { 
      	 $data['product_id'] = $product_id;

         $this->load->view('painel/add_foto_produto',$data);
      }
      
      }

      public function save_photo(){ 

       	//$infoProduto	= $this->ProdutoM->get(array("codproduto" => $this->post('codproduto')), TRUE);//carrega as informações do produto  
 
     


      	

         /* Start Uploading File */
         $config =   [
                     'upload_path'   => './uploadprodutofoto/',
                        'allowed_types' => 'gif|jpg|png',
                        'max_size'      => 1000,
                        'max_width'     => 1000,
                        'max_height'    => 700
                     ];

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                   
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('painel/add_foto_produto', $error);
            }
            else
            {
                   
			      	$file = $this->upload->data();
			                    //print_r($file);
			        $data = [
			                 'tipo'          => 'uploadprodutofoto/'. $file['file_name'],
			                 'codproduto' => $this->input->post('codproduto'),
			                 'fotoprincipal'   => $this->input->post('fotoprincipal')
			              ];
			        $this->PfotoM->create($data);
			               $this->session->set_flashdata('message','Sucesso! Imagem adicionada');
			               redirect('painel/produto');
            }
      }


   public function edit($id) {$rules =    [
                   
                    [
                            'field' => 'fotoprincipal',
                            'label' => 'Fotoprincipal',
                            'rules' => 'required'
                    ]
               ];

      $this->form_validation->set_rules($rules);
      $produtofoto = $this->PfotoM->find($id)->row();

      if ($this->form_validation->run() == FALSE)
      {
         $this->load->view('painel/editar_foto_produto',['produtofoto'=>$produtofoto]);
      }
      else
      {
         if(isset($_FILES["userfile"]["name"]))
         {
            /* Start Uploading File */
            $config =   [
                        'upload_path'   => './uploadprodutofoto/',
                           'allowed_types' => 'gif|jpg|png',
                           'max_size'      => 100,
                           'max_width'     => 1024,
                           'max_height'    => 768
                        ];

               $this->load->library('upload', $config);

               if ( ! $this->upload->do_upload())
               {
                       $error = array('error' => $this->upload->display_errors());
                  $this->load->view('painel/editar_foto_produto',['produtofoto'=>$produtofoto,'error'=>$error]);
               }
               else
               {
                       $file = $this->upload->data();
                       $data['file'] = 'uploadprodutofoto/' . $file['file_name'];
                  unlink($produtofoto->file);
               }
         }

         $data['fotoprincipal']   = set_value('fotoprincipal');
         
         $this->PfotoM->update($id,$data);
         $this->session->set_flashdata('message','Sucesso! imagem alterada com sucesso');
         redirect('painel/produto');
      }}

   public function delete($id) {$this->PfotoM->delete($id);
      $this->session->set_flashdata('message','Sucesso! imagem deletada com sucesso');
      redirect('painel/produto');} 


      //exibe pagina de vinculacao de fotos com atributos(sku) 

      public function fotosku($codproduto){   
   
   $res	= $this->ProdutoM->get(array("codproduto" => $codproduto), TRUE, 0, $this->ion_auth->user()->row()->codusuario );#parametro true indica que no model o get retornará apenas uma linha 
	
		if (!$res) {
			
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		} 

		$fotos = $this->PfotoM->get(array("codproduto" => $codproduto));
		
           $data = array();
	    $data['NOMEPRODUTO'] = $res->nomeproduto; 
	    $data['CODPRODUTO'] = $res->codproduto;
	    $data['BLC_FOTOS'] = array();
	    $data['BLC_SEMFOTOS'] = array(); 
	    $data['URLSALVAFOTOATRIBUTO'] = site_url('painel/produto/salvafotosku');
	    
	    $sku = $this->SkuM->getPorProdutoAtributo($codproduto);
	    
	    $aSKU = array();
	    
	    if ($sku) {
	        foreach($sku as $s) {
	            $aSKU[] = array(
	            	"CODSKU"  => $s->codsku,
	                "NOMESKU" => $s->nomeatributo
	            );
	        }
	    }
	    
	    if ($fotos) {
            foreach($fotos as $f) {
    	        $data['BLC_FOTOS'][] = array(
    	        	"URLIMAGEM"        => base_url($f->tipo),
    	            "BLC_SKUSPRODUTO"  => $aSKU,
    	            "CODPRODUTOFOTO"   => $f->codprodutofoto
    	        );
            }
	    } else {
	        $data['BLC_SEMFOTOS'][] = array();
	    }


          $this->parser->parse('painel/produtoatributo_foto_form', $data);
      } 
      
      //vincula as fotos com o sku no banco de dados
      public function salvafotosku(){ 

      		 $skus      = $this->input->post('skus');
	    $codproduto= $this->input->post('codproduto'); 
	    
	    
	    $this->load->model('FotoSku_Model', 'FotoSkuM');

	    foreach($skus as $codprodutofoto => $codigossku) {
	        
	        $this->FotoSkuM->limpaImagens($codprodutofoto);
	        
	        foreach($codigossku as $codsku) {
	           $itemFoto = new stdClass();
	           $itemFoto->codprodutofoto = $codprodutofoto;
	           $itemFoto->codsku = $codsku;
	           
	           $this->FotoSkuM->post($itemFoto);
	        }
	    }
	    $this->session->set_flashdata('sucesso', 'Imagens modificadas com sucesso.');
	    redirect('painel/produto/fotosku/'.$codproduto);
	}



}


   






