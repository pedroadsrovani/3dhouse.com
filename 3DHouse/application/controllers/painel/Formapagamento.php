<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formapagamento extends  Auth_Controller {
   public function __construct(){ 
        parent::__construct(); 
        $this->layout = LAYOUT_PAINELADMIN;
       $this->load->model('FormaPagamento_Model', 'PagaM'); 
      
   }  

   public function index(){  #exibição de itens de tipo de atributo 
    if (!$this->ion_auth->logged_in())
    {
      // redirect them to the login page
      redirect('auth/login', 'refresh');
    }
    elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {
      // redirect them to the home page because they must be an administrator to view this
      return $this->load->view('painel/redirecionar');
    }
    else
    {    

       $data = array(); 
       $data['URLADICIONAR'] = site_url('painel/formapagamento/adicionar'); 
       $data['URLLISTAR'] = site_url('painel/formapagamento');   
       $data['BLC_DADOS'] = array();  
       $data['BLC_SEMDADOS'] = array(); 
       $data['BLC_PAGINAS'] = array();
       $pagina = $this->input->get('pagina'); 

       if(!$pagina){ 

         $pagina = 0;
       }else{ 

           $pagina = ($pagina - 1) * LINHAS_PESQUISA_PAINELADMIN;//para exibir os proximos 30 itens
       }

       $res = $this->PagaM->get(array(), FALSE, $pagina);#array no metodo get para paginação qie retorna o admin

       if($res){ 
           foreach($res as $r){ 
               $data['BLC_DADOS'][] = array( 
                  "NOME" => $r->nomeformapagamento,  
                  "MAXIMOPARCELAS" => $r->maximoparcelaspagamento, 
                  "DESCONTO" => $r->descontoformapagamento,
                  "URLEDITAR" => site_url('painel/formapagamento/editar/'.$r->codformapagamento),  
                  "HABILITADO" => $r->habilitaformapagamento,
                  "URLEXCLUIR" => site_url('painel/formapagamento/excluir/'.$r->codformapagamento) 
               );//se vier dados sera adicionado ao array
           }
           
       }else{  
           $data['BLC_SEMDADOS'][] = array();


       }   

       #indice de paginação

       $totalItens = $this->PagaM->getTotal(); 
       $totalPaginas = ceil($totalItens / LINHAS_PESQUISA_PAINELADMIN); 

       $indicePg = 1;  
       $pagina = ($pagina ==0)?1:$pagina;

           if($totalPaginas > $pagina){ 
               $data['HABPROX'] = null; 
               $data['URLPROXIMO'] = site_url('painel/formapagamento?pagina='.$pagina + 1);
           }else{  
               $data['HABPROX'] = 'disabled';
               $data['URLPROXIMO'] = '#';
           } 

           if($pagina<=1){ 
               $data['HABANTERIOR'] = 'disabled'; 
               $data['URLANTERIOR'] = '#';
           }else{ 
                $data['HABANTERIOR'] = null; 
               $data['URLANTERIOR'] = site_url('painel/formapagamento?pagina='.$pagina - 1); 

           }


         while($indicePg <= $totalPaginas){ 
             $data['BLC_PAGINAS'][] = array( 
                 "LINK" => ($indicePg==$pagina)?'active':null,
                 "INDICE" => $indicePg,  
                 "URLLINK" =>  site_url('painel/formapagamento?pagina='.$indicePg)

             ); 
             $indicePg++;
         }


       $this->parser->parse('painel/formapagamento_listar', $data); 
     }
   }

   public function adicionar(){ 
       $data = array();  
       $data['ACAO'] = 'Novo';
       $data['codformapagamento'] = ''; 
       $data['nomeformapagamento'] = ''; 
       //$data['chk_tipoformapagamento'] = 'null';  
       $data['descontoformapagamento'] = '0'; 
       $data['chk_habilitaformapagamento'] = 'null'; 
       $data['maximoparcelaspagamento'] = '1';  
       $data['sel_tipoformapagamento1'] = null;  
       $data['sel_tipoformapagamento2'] = null; 

       
         
       $this->setURL($data); #evita a duplicação desses dados para outros formularios de update, select etc..

       $this->parser->parse('painel/formapagamento_form', $data);


   }  

#função editar 

public function editar($id) {
		$data						= array();
		$data['ACAO']				= 'Edição'; 
     $data['chk_habilitaformapagamento'] = 'null'; 
    $data['sel_tipoformapagamento1'] = null;  
    $data['sel_tipoformapagamento2'] = null; 
		
		$res	= $this->PagaM->get(array("codformapagamento" => $id), TRUE);
		
		if ($res) {
			foreach($res as $chave => $valor) {
				$data[$chave] = $valor;
			}
			
      if($res->habilitaformapagamento == 'S'){ 
        $data['chk_habilitaformapagamento'] = 'checked="checked"';  
      } 

      $data['sel_tipoformapagamento'.$res->tipoformapagamento] = 'selected="selected"'; 

      $data['descontoformapagamento'] = number_format($res->descontoformapagamento, '2', ',', '.');
			
			
		} else {
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		}
		
		$this->setURL($data);
		
		$this->parser->parse('painel/formapagamento_form', $data);
	}

   #função salvar 

   public function salvar(){ 

       $codformapagamento = $this->input->post('codformapagamento'); 
       $nomeformapagamento = $this->input->post('nomeformapagamento');  
       $tipoformapagamento = $this->input->post('tipoformapagamento');  
       $descontoformapagamento= $this->input->post('descontoformapagamento');  
       $habilitaformapagamento = $this->input->post('habilitaformapagamento');  
       $maximoparcelaspagamento = $this->input->post('maximoparcelaspagamento'); 
       $descontoformapagamento = str_replace(".", null, $descontoformapagamento);  
       $descontoformapagamento = str_replace(",", ".", $descontoformapagamento); 

       
       if(!$habilitaformapagamento){ 
           
           $habilitaformapagamento = 'N';
       }
       #verifica a validação do tipo atributo
       $erro = FALSE; 
       $mensagem = null;  


           if(!$nomeformapagamento){ 
               $erro  = TRUE; 
               $mensagem .= "Informe o nome da forma de pagamento\n"; 
           } 

           

         if(!$erro){  
              $itens = array( 
       
       "nomeformapagamento" => $nomeformapagamento, 
       "tipoformapagamento" => $tipoformapagamento, 
       "descontoformapagamento" => $descontoformapagamento, 
       "habilitaformapagamento" => $habilitaformapagamento, 
       "maximoparcelaspagamento" => $maximoparcelaspagamento  
       
              ); 


              
               if($codformapagamento){  
                  $codformapagamento =  $this->PagaM->update($itens, $codformapagamento);

               } else{ 

                 $codformapagamento = $this->PagaM->post($itens);
               }  


               if($codformapagamento){
                   
                   $this->session->set_flashdata('sucesso', 'Dados inseridos com sucesso!'); 
                   redirect('painel/formapagamento');


               } else{ 

                   $this->session->set_flashdata('erro', 'Ocorreu um erro ao realizar a operação.'); 
                   
                   if($codformapagamento){  
                       redirect('painel/formapagamento/editar/'.$codformapagamento); 

                   } else{ 
                   redirect('painel/formapagamento/adicionar'); 
                   }

               }


                   
               } else{  

                   $this->session->set_flashdata('erro', $mensagem); 
                    if($codformapagamento){  
                       redirect('painel/formapagamento/editar/'.$codformapagamento); 

                   } else{ 
                   redirect('painel/formapagamento/adicionar'); 
                   }


               }

         }
   

   #metodo privado para a a URL 
   private function setURL(&$data){ 
       $data['URLLISTAR'] = site_url('painel/formapagamento'); 
       $data['ACAOFORM'] = site_url('painel/formapagamento/salvar');
   } 

   #excluir administradores 

   public function excluir($id){  
       $res = $this->PagaM->delete($id); 
       if($res){ 
           $this->session->set_flashdata('sucesso', 'Forma de pagamento removido!');
       }else{ 
           $this->session->set_flashdata('erro', 'Forma de pagamento não foi removido! Tente novamente'); 
           
       }
        redirect('painel/formapagamento');
   }
}