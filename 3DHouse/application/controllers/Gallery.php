<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends  Vendor_Controller {
   public function __construct()
   {
      parent::__construct(); 
      $this->layout	= LAYOUT_PAINELADMIN; 
      $this->load->model('Imagens_Model');
      $this->load->helper(['url','html','form']);
      $this->load->database();
      $this->load->library(['form_validation','session']);
   }

   public function index() {  
if (!$this->ion_auth->logged_in())
    {
      // redirect them to the login page
      redirect('auth/login', 'refresh');
    }
    elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {
      // redirect them to the home page because they must be an administrator to view this
      return $this->load->view('painel/redirecionar');
    }
    else
    {    
    $data = [
         'imagens'   => $this->Imagens_Model->all()
      ]; 
      
      $this->load->view('gallery/index', $data);}
  }
   public function add() {$rules =    [
                    [
                            'field' => 'comentario',
                            'label' => 'Comentario',
                            'rules' => 'required'
                    ],
                    [
                            'field' => 'descricao',
                            'label' => 'Descricao',
                            'rules' => 'required'
                    ]
               ];

      $this->form_validation->set_rules($rules);

      if ($this->form_validation->run() == FALSE)
      {
         $this->load->view('gallery/add_image');
      }
      else
      {

         /* Start Uploading File */
         $config =   [
                     'upload_path'   => './uploads/',
                        'allowed_types' => 'gif|jpg|png',
                        'max_size'      => 1000,
                        'max_width'     => 1200,
                        'max_height'    => 400
                     ];

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('gallery/add_image', $error);
            }
            else
            {
                    $file = $this->upload->data();
                    //print_r($file);
                    $data = [
                             'tipo'          => 'uploads/' . $file['file_name'],
                             'comentario'      => set_value('comentario'),
                             'descricao'   => set_value('descricao'), 
                             'link'   => set_value('link')
                          ];
                    $this->Imagens_Model->create($data);
               $this->session->set_flashdata('message','Sucesso! Imagem adicionada');
               redirect('gallery');
            }
      }} 


   public function edit($id) {$rules =    [
                    [
                            'field' => 'comentario',
                            'label' => 'Comentario',
                            'rules' => 'required'
                    ],
                    [
                            'field' => 'descricao',
                            'label' => 'Descricao',
                            'rules' => 'required'
                    ]
               ];

      $this->form_validation->set_rules($rules);
      $imagens = $this->Imagens_Model->find($id)->row();

      if ($this->form_validation->run() == FALSE)
      {
         $this->load->view('gallery/edit_image',['imagens'=>$imagens]);
      }
      else
      {
         if(isset($_FILES["userfile"]["name"]))
         {
            /* Start Uploading File */
            $config =   [
                        'upload_path'   => './uploads/',
                           'allowed_types' => 'gif|jpg|png',
                           'max_size'      => 1000,
                           'max_width'     => 1500,
                           'max_height'    => 800
                        ];

               $this->load->library('upload', $config);

               if ( ! $this->upload->do_upload())
               {
                       $error = array('error' => $this->upload->display_errors());
                  $this->load->view('gallery/edit_image',['imagens'=>$imagens,'error'=>$error]);
               }
               else
               {
                       $file = $this->upload->data();
                       $data['file'] = 'uploads/' . $file['file_name'];
                  unlink($imagens->file);
               }
         }

         $data['comentario']      = set_value('comentario');
         $data['descricao']   = set_value('descricao'); 
         $data['link']      = set_value('link');

         
         $this->Imagens_Model->update($id,$data);
         $this->session->set_flashdata('message','Sucesso! imagem alterada com sucesso');
         redirect('gallery');
      }}

   public function delete($id) {$this->Imagens_Model->delete($id);
      $this->session->set_flashdata('message','Sucesso! imagem deletada com sucesso');
      redirect('gallery');}
}