<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class Cadastrousuario extends Create_Controller { //classe principal 
    public $is_admin;
    public $logged_in_name; 
    public $query;
    public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */ 

        parent::__construct(); 
        $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega 
        $this->load->database();
		$this->load->library(array('ion_auth','form_validation', 'curl'));
		$this->load->helper(array('url','language')); 
		 $this->load->model('Ion_auth_model', 'IonautH'); 
         $this->load->library('cart');

		$this->load->library(array('email'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
        
    }
    public function create_user()
    { 
        $this->title = "Cadastro - Novo";
        $this->data['title'] = $this->lang->line('create_user_heading');

        

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('primeironome', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('sobrenome', $this->lang->line('create_user_validation_lname_label'), 'required'); 

        $this->form_validation->set_rules('cpf', $this->lang->line('create_user_validation_cpf_label'), 'required');

        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('telefone', $this->lang->line('create_user_validation_phone_label'), 'trim'); 

        $this->form_validation->set_rules('sexo', $this->lang->line('create_user_validation_sexo_label'), 'required'); 

        //$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim'); 

        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required'); 

        $this->form_validation->set_rules('cep', $this->lang->line('create_user_validation_cep_label'), 'required');   

        $this->form_validation->set_rules('cidade', $this->lang->line('create_user_validation_cidade_label'), 'required');  

        $this->form_validation->set_rules('estado', $this->lang->line('create_user_validation_estado_label'), 'required');   

        $this->form_validation->set_rules('rua', $this->lang->line('create_user_validation_rua_label'), 'required');  

        $this->form_validation->set_rules('bairro', $this->lang->line('create_user_validation_bairro_label'), 'required'); 

         $this->form_validation->set_rules('complemento');




        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'primeironome' => $this->input->post('primeironome'),
                'sobrenome'  => $this->input->post('sobrenome'),
                'cpf'    => $this->input->post('cpf'),
                'telefone'      => $this->input->post('telefone'), 
                 'sexo'    => $this->input->post('sexo'), 
                  'cep'    => $this->input->post('cep'), 
                   'cidade'    => $this->input->post('cidade'), 
                    'estado'    => $this->input->post('estado'), 
                     'rua'    => $this->input->post('rua'), 
                      'bairro'    => $this->input->post('bairro'), 
                       'complemento'    => $this->input->post('complemento')
            );
        } 

        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("cadastrousuario/login", 'refresh'); 
            print_r($additional_data); die();
        }
        else
        { 
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['primeironome'] = array(
                'name'  => 'primeironome',
                'id'    => 'primeironome',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('primeironome'),
            );
            $this->data['sobrenome'] = array(
                'name'  => 'sobrenome',
                'id'    => 'sobrenome',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('sobrenome'),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['cpf'] = array(
                'name'  => 'cpf',
                'id'    => 'cpf',
                'type'  => 'integer',
                'value' => $this->form_validation->set_value('cpf'),
            );
            $this->data['telefone'] = array(
                'name'  => 'telefone',
                'id'    => 'telefone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('telefone'),
            ); 
            $this->data['sexo'] = array(
                'name'  => 'sexo',
                'id'    => 'sexo',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('sexo'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );  

            $this->data['cep'] = array(
                'name'  => 'cep',
                'id'    => 'cep',
                'type'  => 'integer',
                'value' => $this->form_validation->set_value('cep'),
            ); 

            $this->data['cidade'] = array(
                'name'  => 'cidade',
                'id'    => 'cidade',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('cidade'), 

            ); 
            $this->data['estado'] = array(
                'name'  => 'estado',
                'id'    => 'estado',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('estado'),
            ); 

            $this->data['rua'] = array(
                'name'  => 'rua',
                'id'    => 'rua',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('rua'),
            ); 

            $this->data['bairro'] = array(
                'name'  => 'bairro',
                'id'    => 'bairro',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('bairro'),
            ); 

            $this->data['complemento'] = array(
                'name'  => 'complemento',
                'id'    => 'complemento',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('complemento'),
            );



            $this->_render_page('loja/criar_usuario_form', $this->data);
        }
    }  



        public function edit_user($id)
    {    $this->title = "Editar dados";
        $this->data['title'] = $this->lang->line('edit_user_heading');
        //atenção--------------------------------------------------------------------------------------
        //if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->codusuario == $id))) 
        //----------------------------------------------------------------------------------------------    
        {
            //redirect('loja', 'refresh');
        }

        $user = $this->ion_auth->user($id)->row();
        $groups=$this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();

        // validate form input
        $this->form_validation->set_rules('primeironome');
        $this->form_validation->set_rules('sobrenome'); 
        $this->form_validation->set_rules('cpf');
        $this->form_validation->set_rules('telefone');
        
        $this->form_validation->set_rules('cep'); 
        $this->form_validation->set_rules('cidade'); 

        $this->form_validation->set_rules('estado'); 
        $this->form_validation->set_rules('rua'); 
        $this->form_validation->set_rules('bairro'); 
        $this->form_validation->set_rules('complemento');

        if (isset($_POST) && !empty($_POST))
        {
            // do we have a valid request?
        

            // update the password if it was posted
            if ($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE)
            {
                $data = array(
                    
                    'primeironome' => $this->input->post('primeironome'),
                'sobrenome'  => $this->input->post('sobrenome'),
                'cpf'    => $this->input->post('cpf'),
                'telefone'      => $this->input->post('telefone'), 
                  
                  'cep'    => $this->input->post('cep'), 
                   'cidade'    => $this->input->post('cidade'), 
                    'estado'    => $this->input->post('estado'), 
                     'rua'    => $this->input->post('rua'), 
                      'bairro'    => $this->input->post('bairro'), 
                       'complemento'    => $this->input->post('complemento')
                );

                // update the password if it was posted
                if ($this->input->post('password'))
                {
                    $data['password'] = $this->input->post('password');
                }



                // Only allow updating groups if user is admin
                if ($this->ion_auth->is_admin())
                {
                    //Update the groups user belongs to
                    $groupData = $this->input->post('grupos');

                    if (isset($groupData) && !empty($groupData)) {

                        $this->ion_auth->remove_from_group('', $id);

                        foreach ($groupData as $grp) {
                            $this->ion_auth->add_to_group($grp, $id);
                        }

                    }
                }

            // check to see if we are updating the user
               if($this->ion_auth->update($user->codusuario, $data))
                {
                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('message', $this->ion_auth->messages() ); 

               

                    if ($this->ion_auth->is_admin())
                    {
                        redirect('loja', 'refresh');
                    }
                    else
                    {
                        redirect('/', 'refresh');
                    }

                }
                else
                {
                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('message', $this->ion_auth->errors() );
                    if ($this->ion_auth->is_admin())
                    {
                        redirect('loja', 'refresh');
                    }
                    else
                    {
                        redirect('/', 'refresh');
                    }

                }

            }
        }

        // display the edit user form
        

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['usuario'] = $user;
        $this->data['grupos'] = $groups;
        $this->data['currentGroups'] = $currentGroups;

        $this->data['primeironome'] = array(
            'name'  => 'primeironome',
            'id'    => 'primeironome',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('primeironome', $user->primeironome),
        );
        $this->data['sobrenome'] = array(
            'name'  => 'sobrenome',
            'id'    => 'sobrenome',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('sobrenome', $user->sobrenome),
        );
        $this->data['cpf'] = array(
            'name'  => 'cpf',
            'id'    => 'cpf',
            'type'  => 'integer',
            'value' => $this->form_validation->set_value('cpf', $user->cpf),
        );
        $this->data['telefone'] = array(
            'name'  => 'telefone',
            'id'    => 'telefone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('telefone', $user->telefone),
        );  

        

        //$this->data['chk_sexo'] = 'disabled="disabled"';

        $this->data['password'] = array(
            'name' => 'password',
            'id'   => 'password',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id'   => 'password_confirm',
            'type' => 'password'
        ); 

        $this->data['cep'] = array(
            'name'  => 'cep',
            'id'    => 'cep',
            'type'  => 'integer',
            'value' => $this->form_validation->set_value('cep', $user->cep),
        ); 

        $this->data['cidade'] = array(
            'name'  => 'cidade',
            'id'    => 'cidade',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('cidade', $user->cidade),
        ); 

        $this->data['estado'] = array(
            'name'  => 'estado',
            'id'    => 'estado',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('estado', $user->estado),
        ); 

        $this->data['rua'] = array(
            'name'  => 'rua',
            'id'    => 'rua',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('rua', $user->rua),
        ); 

        $this->data['bairro'] = array(
            'name'  => 'bairro',
            'id'    => 'bairro',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('bairro', $user->bairro),
        ); 

        $this->data['complemento'] = array(
            'name'  => 'complemento',
            'id'    => 'complemento',
            'type'  => 'textr',
            'value' => $this->form_validation->set_value('complemento', $user->complemento),
        );

        $this->_render_page('loja/editar_usuario_form', $this->data);
    }



    

	public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	} 
	public function consulta(){

        $cep = $this->input->post('cep');

        $this->layout=null;
        $this->output->set_content_type('application/json')
        ->set_output( $this->curl->consulta($cep));

    }  

    public function login()
    { 
         
        $this->title = "Login";

        //validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

        if ($this->form_validation->run() == true)
        {
            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
            {
                //if the login is successful
                //redirect them back to the home page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('loja', 'refresh');
            }
            else
            {
                // if the login was un-successful
                // redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('cadastrousuario/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }
        else
        {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id'   => 'password',
                'type' => 'password',
            );

            $this->_render_page('loja/login', $this->data);
        }
    }
}