<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class Fichatecnicaproduto extends CI_Controller {  
//classe principal
    public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */ 

        parent::__construct(); 
        $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega  
        $this->load->library(array('ion_auth','form_validation', 'curl'));
    $this->load->helper(array('url','language', 'html')); 
     $this->load->model('Ion_auth_model', 'IonautH');  
    $this->load->library('cart');
     $this->load->model ('Produto_Model', 'ProdutoM'); 
     $this->load->model('Sku_Model', 'SkuM');  
        $this->load->model('ProdutoFoto_Model', 'PfotoM');

        
    }

    public function show($id,$sku = -1){ //função publica  
        $data = array();  
         //$data = $this->ProdutoM->get_ficha_produto($id);
        
        $data['produto'] =  $this->ProdutoM->get_ficha_produto($id);
        
        //print_r($data['produto']); die();
        if(isset($data['produto'])){
      
                

                if (!empty ($data['produto']['codtipoatributo'])) {
                    $sku = $this->SkuM->getPorProdutoAtributo($id);
                    //print_r($sku); die();

                    foreach ( $sku as $s ) {
                        $data['skus'][] = array(
                                "exibir"=>true,
                                "codigo" => $s->codsku,
                                "nome_atributo" => $s->nomeatributo,
                                "referencia" => $s->referencia 

                                //"codsku" => site_url('checkout/adicionar/'.$s->codsku) 

                        );
                    }
                } else{

                    $sku  = $this->SkuM->getPorProdutoSimples($id);
                    $data['skus'][] = array(
                                "exibir"=>false,
                                "codigo" => $sku->codsku,
                                "nome_atributo" => $sku->nomeatributo,
                                "referencia" => $sku->referencia 
                                //"codsku" => site_url('checkout/adicionar/'.$s->codsku) 
                        );

                } 



            $this->parser->parse('loja/fichatecnicaproduto', $data);

        }
        else { 
            //show_error("produto não encontrado"); 

            $this->load->view('loja/produtoinexistente');
        }
    }

} 
