<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class manualdousuario extends CI_Controller {  
//classe principal
    public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */ 

        parent::__construct(); 
        $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega  
        $this->load->library(array('ion_auth','form_validation', 'curl'));
    $this->load->helper(array('url','language', 'html')); 
     $this->load->model('Ion_auth_model', 'IonautH');  
     $this->load->model('Imagens_Model');
     $this->load->model ('Produto_Model', 'ProdutoM'); 

     $this->load->library('cart');

     $this->load->model('Departamento_Model','DepartamentoM');  

  }                        
public function index(){  

    $this->load->view('loja/manualdousuario');

    } 
}
