<?php
defined('BASEPATH') OR exit('No direct script access allowed');//cabeçalho padrão

class Busca extends CI_Controller {  
//classe principal
    public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */ 

        parent::__construct(); 
        $this->layout = LAYOUT_LOJA;// define o layout do painel admin ou carrega  
        $this->load->library(array('ion_auth','form_validation', 'curl'));
    $this->load->helper(array('url','language', 'html')); 
     $this->load->model('Ion_auth_model', 'IonautH');  
     $this->load->model('Imagens_Model'); 
     $this->load->model ('Produto_Model', 'ProdutoM'); 
     $this->load->library('cart');


     $this->load->model('Departamento_Model','DepartamentoM');  

                          

        
    }
    public function index(){ //função publica   
        
        
       /* $produto = $this->ProdutoM->get_busca();
        print_r($produto); die();

        if (! $produto) {
            show_error ( "Não foram encontrados produtos." );
        } 

        $this->load->view('loja/resultadobusca', $produto);*/ 

        

            //$this->load->model ( 'Produto_Model', 'ProdutoM' );
        
          $pagina = $this->input->get('pagina');
          $pagina     = ($pagina==0)?1:$pagina;
          $totalResultados = 0;


          if ($this->input->get('departamento')){
             $totalResultados = $this->ProdutoM->get_produto_depto_count($this->input->get('departamento'))->total;
              $produto = $this->ProdutoM->get_produto_depto($this->input->get('departamento'),1/*CONSTANTE*/,($pagina-1)*1/*CONSTANTE*/);
              $start_url = "busca?departamento=".$this->input->get('departamento').'&';
          }
          else {            
              $totalResultados=2;
              $produto = $this->ProdutoM->get_busca($this->input->get('pesquisa')); 
              $start_url = "busca?pesquisa=".$this->input->get('pesquisa').'&';
           } 
 
          $data = array();
            

        
          $start_url =str_replace(" ","%20",$start_url);  
        

           foreach($produto as $p){ 
            $data['BLC_LINHA'][] = array( 
              "NOMEPRODUTO" => $p->nomeproduto, 
              "PRECO" => $p->valorproduto, 
              "PROMOCAO" => $p->valorpromocional, 
              "DESCRICAO" =>  substr($p->resumoproduto, 0,10),
              "URLFOTO" => $p->tipo, 
              "URLPRODUTO" => site_url('fichatecnicaproduto/show/'.$p->codproduto), 
              "URLPRODUTOC" => site_url('checkout/adicionar/'.$p->codproduto), 
              "NOME" => $p->primeironome,  
              "SOBRENOME"=> $p->sobrenome, 

                );

          } 
              
 
   

    $totalPaginas = ceil($totalResultados/1/*CONSTANTE*/);
    
    $indicePg   = 1;
    
  
    if ($totalPaginas > $pagina) {
      $data['HABPROX']  = null;
      $data['URLPROXIMO'] = site_url($start_url.'pagina='.($pagina+1));
    } else {
      $data['HABPROX']  = 'disabled';
      $datapro['URLPROXIMO'] = '#';
    }
  
    if ($pagina <= 1) {
      $data['HABANTERIOR']= 'disabled';
      $data['URLANTERIOR']= '#';
    } else {
      $data['HABANTERIOR']= null;
      $data['URLANTERIOR']= site_url($start_url.'pagina='.($pagina-1));
    }
  
  
  
    while ($indicePg <= $totalPaginas) {
      $data['BLC_PAGINAS'][] = array(
          "LINK"    => ($indicePg==$pagina)?'active':null,
          "INDICE"  => $indicePg,
          "URLLINK" => site_url($start_url."pagina=$indicePg")
      );
        
      $indicePg++;
    }

       
 if (! $produto) {
            //$this->load->model ( 'Produto_Model', 'ProdutoM' );
            
            $this->load->view('loja/0resultado'); 

        }   
        else {
          $this->parser->parse('loja/resultadobusca', $data);  
        }



    }   




}