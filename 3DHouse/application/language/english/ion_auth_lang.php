<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation
$lang['account_creation_successful']            = 'Conta criada com sucesso!';
$lang['account_creation_unsuccessful']          = 'Não foi possível criar uma conta!';
$lang['account_creation_duplicate_email']       = 'E-mail já utilizado ou inválido';
$lang['account_creation_duplicate_identity']    = 'E-mail já utilizada ou inválida';
$lang['account_creation_missing_default_group'] = 'O grupo padrão não está configurado';
$lang['account_creation_invalid_default_group'] = 'Grupo padrão inválido';


// Password
$lang['password_change_successful']          = 'Senha mudada com sucesso';
$lang['password_change_unsuccessful']        = 'Não é possível alterar a senha';
$lang['forgot_password_successful']          = 'Senha Redefinir E-mail Enviado';
$lang['forgot_password_unsuccessful']        = 'Não foi possível enviar por e-mail o link Redefinir Senha';

// Activation
$lang['activate_successful']                 = 'Conta ativada';
$lang['activate_unsuccessful']               = 'Não é possível ativar conta';
$lang['deactivate_successful']               = 'Conta desativada';
$lang['deactivate_unsuccessful']             = 'Não é possível desativar conta';
$lang['activation_email_successful']         = 'E-mail de ativação enviada. Verifique sua caixa de entrada ou spam';
$lang['activation_email_unsuccessful']       = 'Não é possível enviar o email de ativação';
$lang['deactivate_current_user_unsuccessful']= 'Você não pode se desativar.';

// Login / Logout
$lang['login_successful']                    = 'Conectado com sucesso';
$lang['login_unsuccessful']                  = 'Login incorreto';
$lang['login_unsuccessful_not_active']       = 'A conta está inativa';
$lang['login_timeout']                       = 'Temporariamente bloqueado. Tente mais tarde.';
$lang['logout_successful']                   = 'Você se deslogou';

// Account Changes
$lang['update_successful']                   = 'Informações da conta atualizadas com sucesso';
$lang['update_unsuccessful']                 = 'Não é possível atualizar informações da conta';
$lang['delete_successful']                   = 'Usuário excluído';
$lang['delete_unsuccessful']                 = 'Não é possível excluir o usuário';

// Groups
$lang['group_creation_successful']           = 'Grupo criado com sucesso';
$lang['group_already_exists']                = 'Nome do grupo já tomado';
$lang['group_update_successful']             = 'Detalhes do grupo atualizados';
$lang['group_delete_successful']             = 'Group deletado';
$lang['group_delete_unsuccessful']           = 'Não foi possível excluir o grupo';
$lang['group_delete_notallowed']             = 'Can\'t delete the administrators\' group';
$lang['group_name_required']                 = 'O nome do grupo é um campo obrigatório';
$lang['group_name_admin_not_alter']          = 'O nome do grupo de administração não pode ser alterado';

// Activation Email
$lang['email_activation_subject']            = 'Conta ativada';
$lang['email_activate_heading']              = 'COnta atiavada para %s';
$lang['email_activate_subheading']           = 'Clique neste link para %s.';
$lang['email_activate_link']                 = 'Ative sua conta';

// Forgot Password Email
$lang['email_forgotten_password_subject']    = 'Verificação de senha esquecida';
$lang['email_forgot_password_heading']       = 'Redefinir senha para %s';
$lang['email_forgot_password_subheading']    = 'Clique neste link para %s.';
$lang['email_forgot_password_link']          = 'Redefina sua senha';

// New Password Email
$lang['email_new_password_subject']          = 'Nova senha';
$lang['email_new_password_heading']          = 'Nova senha para %s';
$lang['email_new_password_subheading']       = 'Sua senha foi redefinida para: %s';
