
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8">
    <title></title> 
    <link href="<?php echo base_url('assets/css/fichatecnicaproduto.css') ?>" rel="stylesheet">   
    <link href="<?php echo base_url('assets/css/smoothproducts.css') ?>" rel="stylesheet">  
    <script src="<?php echo base_url('assets/js/smoothproducts.min.js')?>"></script>  

    <script>   $(document).ready(function(){
            //-- Click on detail
            

            //-- Click on QUANTITY
            $(".btn-minus").on("click",function(){
                var now = $(".section > div > input").val();
                if ($.isNumeric(now)){
                    if (parseInt(now) -1 > 0){ now--;}
                    $(".section > div > input").val(now);
                }else{
                    $(".section > div > input").val("1");
                }
            })            
            $(".btn-plus").on("click",function(){
                var now = $(".section > div > input").val();
                if ($.isNumeric(now)){
                    $(".section > div > input").val(parseInt(now)+1);
                }else{
                    $(".section > div > input").val("1");
                }
            })                        
        }) </script>  
        
    <script type="text/javascript">
    /* wait for images to load */
    $(window).load(function() {
        $('.sp-wrap').smoothproducts();
    });
    </script> 


</head>
    <body>
        <div class="container">  
        <button style="margin-top: 8%;" class="btnprodutos" type="button"><font color="#000"><strong>Ficha técnica do produto: <small id="passwordHelpInline" class="text-muted">
     <?=$produto['nomeproduto'];?>
    </small>   </strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="500" size="100" />  
<div class="row" style="margin-top: 4%;"> 

    <div class="col-xs-4 item-photo" style="margin-top: 2%;">

        
        <div class="sp-loading"><img src="images/sp-loading.gif" alt=""></div>
        <div class="sp-wrap">

            
            <?php 

                $imagens = explode(";",$produto['tipo']);

                foreach($imagens as $img): ?>
            <a href="<?php echo base_url($img); ?>"><img src="<?php echo base_url($img); ?>" alt=""></a>
            <?php endforeach;?>
            
        </div> 


    </div><!-- col-xs-4 item-photo-->  
<form action="<?php echo site_url('carrinho/adicionar'); ?>"  method="post">           
              <input type="hidden" value="<?=$produto['codproduto'];?>" name="codproduto"> 
              <input type="hidden" value="<?=$produto['primeironome'];?>" name="primeironome">  
              <input type="hidden" value="<?=$produto['sobrenome'];?>" name="sobrenome"> 
              <input type="hidden" value="<?=$produto['nomeproduto'];?>" name="nomeproduto">  
              <input type="hidden" value="<?=$produto['valorproduto'];?>" name="valorproduto">
              <input type="hidden" value="<?=$produto['valorpromocional'];?>" name="valorpromocional"> 
              <input type="hidden" value="<?=$produto['foto_principal'];?>" name="tipo">
        <div class="col-xs-5" style="border:0px solid gray">
                    <!-- Datos del vendedor y titulo del producto -->
                    <h3><strong><?=$produto['nomeproduto'];?></strong></h3>  
                    <small  style="font-size: 15px;" id="passwordHelpInline" class="text-muted">
      <strong>Vendido por: </strong> <?=$produto['primeironome'];?> <?=$produto['sobrenome'];  ?>
    </small>   
                    
        
                    <!-- Precios -->
                    <h6 class="title-price"><small><strong>PREÇO</strong></small></h6> 
                    <?php if($produto['valorpromocional'] > 0.00){?>
                    <h3 style="margin-top:0px;"><font color="#006400">R$ <?=$produto['valorpromocional'];  ?></font></h3>
                    <?php }else{  ?>  
                        
                        <h3 style="margin-top:0px;">R$ <?=$produto['valorproduto'];?></h3>
                    <?php } ?> 


                    <!-- Detalles especificos del producto --> 
                    
             

              <?php foreach ($skus as $sku):


                    if($sku["exibir"]):

               ?> 
                  <label class="radio">
                     <input type="radio" value="<?=$sku['codigo'].":".$sku['nome_atributo'].":".$sku['referencia'];?>" name="sku" required><?=$sku['nome_atributo'];?> <small style="font-size:8px;">(<?=$sku['referencia'];?>)</small> 
                  </label>


                <?php
                    else:
                ?> 
              
                         <input type="hidden" value="<?=$sku['codigo'];?>" name="codsku"> 



              

            
                    

                    <?php     endif; 
                    endforeach;?>        


                    <div class="section"style="padding-bottom:20px;">
                        <h5 class="title-attr">Quantidade</h5><br>                    
                        <div>
                            <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
                            <input  value="1" name="quantidade" required/>
                            <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
                        </div>
                    </div>           
                



                    <!-- Botones de compra -->
                    <div class="section" style="padding-bottom:20px;">
                        <button style="background: #006400;" type="submit" class="btn btn-success"><span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Adicionar ao carrinho</button>
                        
                    </div>                                        
                </div>  
             </form>                                
            <!-- detalhes do produto --> 
            <div style="width: 100%;" class="col-xs-9">
            <div class="tabs minimal cross-fade">
            <section>
                <h4>Detalhes do produto</h4>

                <p><?=$produto['resumoproduto']; ?>.</p>
            </section>

            <section>
                <h4>Ficha técnica</h4>

                <p><?=$produto['fichaproduto'];?>.</p>

                
            </section>

            <section>
                <h4>Comentários</h4>

                <div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://http-3dhouse-com.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            
            </section>
        </div>

    </div> 
  </div>  

<script src="<?php echo base_url('assets/js/index.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.ba-resize.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tabs+accordion.js');?>"></script>
<script>
$('.accordion, .tabs')
    .TabsAccordion({
        hashWatch: true,
        pauseMedia: true,
        responsiveSwitch: 'tablist',
        saveState: sessionStorage,
    });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

            <!-- fim do detalhes do produto -->
                


        </div><!-- row -->  
 </div><!-- container -->

    </body>
</html>






