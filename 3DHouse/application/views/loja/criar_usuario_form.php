
                       
                       
                    
<div style="margin-top: 4%; border: 1px solid #ccc; width: 70%;" class="container">
  



<h2>Cadastre-se - <font color="#006400"> Novo</font></h2><!--novo saber que esta adicionando novo admin -->


<hr class="hr">
<?php if ($message): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                             <?php echo $message; ?>
                        </div>
                    <?php endif; ?>


<div style="margin-left: 15%; width: 90%" class="container">
<?php echo form_open("cadastrousuario/create_user");?> 

  

<label>Nome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input style="width: 70%;" type="text" id="primeironome" class="form-control" name="primeironome" placeholder="Digite um nome..." required/>
     <label class="control-label" for="nome"></label>
  </div>

</br> 

<label>Sobrenome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input style="width: 70%;" type="text" id="sobrenome" class="form-control" name="sobrenome" value="" placeholder="Digite um sobrenome..." required/>
     <label class="control-label" for="nome"></label>
  </div>

</br>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

<label>Digite um cpf para o usuário<font color="#FF0000"> *</font></label>
   <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input style="width: 70%;" type="text" id="cpf" class="form-control" name="cpf" placeholder="Ex: 12345678911" required/>
     <label class="control-label" for="cpf"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Com ou sem traços. Apenas 11 dígitos.
    </small>  
  </br> 
</br>

      
<label>Email <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
    <input style="width: 70%;" type="email" id="email" class="form-control" name="email"  placeholder="Ex: example@gmail.com">
     <label class="control-label" for="email"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Obs: digite um e-mail válido e que não esteja já cadastrado na loja.
    </small> 
      <!--<p>
            <?php //echo lang('create_user_email_label', 'email');?> <br />
            <?php //echo form_input($email);?>
      </p>--> 
<br/> 
<br>
      <label>Telefone <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
    <input style="width: 70%;" type="text" id="telefone" class="form-control" name="telefone" value="" placeholder="Ex: 54991557182" required/>
     <label class="control-label" for="email"></label>
  </div>  
  
<br> 
<br>
    <label>Sexo: <font color="#FF0000"> *</font></label> 
</br>
<label class="radio-inline"><input type="radio" name="sexo"  id="sexo" value="masculino" {chk_sexo}>Masculino</label>
<label class="radio-inline"><input type="radio" name="sexo" id="sexo" value="feminino" {chk_sexo}>Feminino</label>
  
    

      <!--<p>
            <?php //echo lang('create_user_phone_label', 'phone');?> <br />
            <?php //echo form_input($phone);?>
      </p>-->
</br>
</br>
<label>Senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input style="width: 70%;" type="password" id="password" class="form-control" name="password">
     <label class="control-label" for="password"></label> 
     
  </div> 
  <small id="passwordHelpInline" class="text-muted">
      A senha deve conter de 8 a 20 caracteres.
    </small>  
    </br>
    </br>
      <!--<p>
            <?php //echo lang('create_user_password_label', 'password');?> <br />
            <?php //echo form_input($password);?>
      </p>-->
<label>Confirme a senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input style="width: 70%;" type="password" id="password_confirm" class="form-control" name="password_confirm">
     <label class="control-label" for="password_confirm"></label> 
     
  </div>   
  <small id="passwordHelpInline" class="text-muted">
      Repita a mesma senha.
    </small>  
  <br/> 
  <br/>
      <!--<p>
            <?php //echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php //echo form_input($password_confirm);?>
      </p>--> 
      <label>Digite um cep e depois clique em Consultar <font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input style="width: 70%;" type="text" id="cep" class="form-control" style="width:50%" name="cep" value="">
     <label class="control-label" for="cep"></label>  <button id="btn_consulta" style="margin-left: 10px;" class="btn btn-primary" type="button" > <em class="glyphicon glyphicon-search"></em> Consultar</button>
  </div>  

</br> 
<label>Cidade<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input style="width: 70%;" type="text" id="cidade" class="form-control" style="width:50%" name="cidade" value="" required/>
     <label class="control-label" for="cidade"></label> 

  </div>   
   
  </br>
<label>Estado<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input style="width: 70%;" type="text" id="estado" class="form-control" style="width:50%" name="estado" value=""  required/>
     <label class="control-label" for="estado"></label>
  </div>  
  <br>
<label>Rua<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input style="width: 70%;" type="text" id="rua" class="form-control" name="rua" value="" required/>
     <label class="control-label" for="rua"></label>
  </div>  
   
  </br>
<label>Bairro<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input style="width: 70%;" type="text" id="bairro" class="form-control" name="bairro" value="" required/>
     <label class="control-label" for="bairro"></label>
  </div>  
 
   
  </br> 

  <div class="form-group">
  <label class="control-label" for="complemento">Complemento (opcional)</label> 
    
       <textarea style="width: 70%;" class="form-control" id="complemento" name="complemento" rows="6" placeholder="Digite aqui..."></textarea>

     <label class="control-label" for="complemento"></label>
  </div>   

<br>   

     
<div style="margin-left: 22%;" ><button type="submit" class="btn btn-success"><em class="glyphicon glyphicon-floppy-disk"></em> Salvar</button>
        <a href="../loja"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a> </div>



<br>
<?php echo form_close();?>  
</div>
</div>
