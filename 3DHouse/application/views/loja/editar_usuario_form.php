<div style="margin-top: 4%; border: 1px solid #ccc; width: 70%;" class="container">


  

<h2>Editar dados  <!--<font color="#006400"> Novo</font>--></h2><!--novo saber que esta adicionando novo admin -->

<div style="margin-left: 15%; width: 90%" class="container">
   

<hr class="hr">
<?php if ($message): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                             <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
                            
                      
<?php echo form_open(uri_string());?>

     <label>Nome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
      <?php echo form_input($primeironome,null, array('class'=>'form-control' , 'style'=>'width: 70%;'));?>
     
  </div>

</br> 

<label>Sobrenome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>

    <?php echo form_input($sobrenome,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
     
  </div>

</br>
      

<label>Digite um cpf para o usuário<font color="#FF0000"> *</font></label>
   <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <?php echo form_input($cpf,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Sem traços. Ex:12345678911
    </small>  
  </br> 
</br>

     
        

      <label>Telefone <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
    <?php echo form_input($telefone,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Exemplo:(DDD) xxxx xx xx
    </small>  

    

      <!--<p>
            <?php //echo lang('create_user_phone_label', 'phone');?> <br />
            <?php //echo form_input($phone);?>
      </p>-->
</br>
</br>
<label>Senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input  type="password" id="password" class="form-control" style="width: 70%;" name="password" >
     <label class="control-label" for="password"></label> 
     
  </div> 
  <small id="passwordHelpInline" class="text-muted">
      A senha deve conter de 8 a 20 caracteres.
    </small>  
    </br>
    </br>
      <!--<p>
            <?php //echo lang('create_user_password_label', 'password');?> <br />
            <?php //echo form_input($password);?>
      </p>-->
<label>Confirme a senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input style="width: 70%"  type="password" id="password_confirm" class="form-control" name="password_confirm">
     <label class="control-label" for="password_confirm"></label> 
     
  </div>  
  <br/> 
  <br/>
      <!--<p>
            <?php //echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php //echo form_input($password_confirm);?>
      </p>--> 
      <label>Digite um cep e depois clique em Consultar <font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <?php echo form_input($cep,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>  <button id="btn_consulta" style="margin-left: 10px;" class="btn btn-primary" type="button" ><em class="glyphicon glyphicon-search"></em> Consultar</button>
  </div>  

</br> 
<label>Cidade<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <?php echo form_input($cidade,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>

  </div>   
   
  </br>
<label>Estado<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <?php echo form_input($estado,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
  </div>  
  <br>
<label>Rua<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <?php echo form_input($rua,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
  </div>  
   
  </br>
<label>Bairro<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <?php echo form_input($bairro,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
  </div>  
 
   
  </br> 

  <div class="form-group">
  <label class="control-label" for="complemento">Complemento (opcional)</label> 
    
      <?php echo form_textarea($complemento,null, array('class'=>'form-control', 'style'=>'width: 70%;'));?>
  </div>   


     

      <?php if ($this->ion_auth->is_admin()): ?>

         <label class="control-label" for="complemento">Grupo de usuário</label> 
          <?php foreach ($grupos as $group):?>
              <label class="checkbox">
              <?php
                  $gID=$group['codgrupo'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->codgrupo) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="radio" name="groups[]" value="<?php echo $group['codgrupo'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['nome'],ENT_QUOTES,'UTF-8');?>
              </label> 

          <?php endforeach?> 
          <small id="passwordHelpInline" class="text-muted">
      Cada grupo de usuário possui permissões e privilégios diferentes.
    </small>  

      <?php endif ?>

      <?php echo form_hidden('codusuario', $usuario->codusuario);?>
 
      <br>
</br>
      <div style="margin-left: 25%;"><button type="submit" class="btn btn-success"><em class="glyphicon glyphicon-floppy-disk"></em> Salvar</button>
        <a href="../../loja"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a> </div>
<br>

<?php echo form_close();?>
</div> 
</div>