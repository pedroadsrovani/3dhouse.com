<a  name="capa"></a> 

<div class="container">

<button style="margin-top: 8%;" class="btnprodutos" type="button"><font color="#000"><strong>Manual do usuário 3DHouse</strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="100%" size="100" />  

<img src="<?php echo base_url('assets/images/capa.png');?>" width="100%" height="120%"/> 
<a name="sumario"></a>
<br>

<button style="margin-top: 2%;" class="btnprodutos" type="button"><font color="#000"><strong>Sumário</strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="100%" size="100" /> 


	<ol>
		<li><a style="color: #000;" href="#capa"><h4>Capa</h4></a></li> 
		<li><a style="color: #000;" href="#sumario"><h4>Sumário</h4></a></li> 
		<li><a style="color: #000;" href="#introducao"><h4>Introdução</h4></a></li> 
		<li><a style="color: #000;" href="#menu"><h4>Menu</h4></a></li><a name="introducao"></a> 
		<li><a style="color: #000;" href="#funcionalidades"><h4>Principais Funcionalidades</h4></a></li> 
		<li><a style="color: #000;" href="#problemas"><h4>Solução de Problemas</h4></a></li>
	</ol> 
	
  

<button style="margin-top: 2%;" class="btnprodutos" type="button"><font color="#000"><strong>Introdução</strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="100%" size="100" />  
<div class="drawer">
<a name="menu"></a>
				<!-- First Item -->
				<div class="drawer-item"><a name="funcionalidades"></a> 
					<div class="drawer-header">
						<h5>Clique aqui para ler a introdução do manual</h5>
						
					</div>
					<div class="drawer-content">
						<h4> 3 - Introdução</h4> 
						<p style="margin-left: 2%; font-size: 15px;"><strong> Especialmente nesta década, a impressão 3D se popularizou no cenário mundial
						tornando-se um nicho de mercado ainda pouco explorado pelo comércio eletrônico. A
						maioria destas máquinas conseguem imprimir qualquer tipo de objeto utilizando a tecnologia de impressão tridimensional. O mercado de impressão 3D está em ascensão em decorrência da popularização desses equipamentos. Entretanto, á prática de impressão de objetos tridimensionais existe há anos. Mas tem sido
						apenas nos últimos cinco a seis anos que a impressão 3D tornou-se um passatempo popular e um tópico privilegiado na mı́dia, principalmente devido a queda de custos e o tamanho dos componentes diminui. Hoje, a impressão em 3D está disponı́vel para qualquer pessoa com um computador doméstico e o interesse em imprimir objetos 3D.</strong></p><br> 

				

						<p style="margin-left: 2%; font-size: 15px;"><strong> Nos dias de hoje, tem sido possı́vel verificar a popularização das impressoras 3D e o avanço para quase todas as categorias de negócios como moda, arte, decoração, medicina, joias, peças industriais, entre outras. Entretanto, há outras formas de explorar esses nichos de mercado não apenas como produtos sob demanda, mas com vendas de objetos através de catálogo online. Por este motivo foi fundada a loja virtual 3DHouse, site de comércio eletrônico, que disponibilizará um catálogo online para compras de produtos feitos por impressoras 3D para arte e decoração de ambientes. Segue listado em forma de itens, os principais objetivos do sistema 3DHouse.</strong></p> <br> 
						<ul>
							<li style="font-size: 15px;">Dar enfoque em produtos feitos de material biodegradável</li> 
							<li style="font-size: 15px;">Fornecer uma interface que facilite a interação com os usuários</li> 
							<li style="font-size: 15px;">Possibilitar aos usuários a obtenção de informações detalhadas dos produtos e de seus respectivos vendedores</li>  
							<li style="font-size: 15px;">Oferecer a possibilidade de várias formas de pagamento</li> 
							<li style="font-size: 15px;">Fornecer conteúdo de forma paginada e com diversidade de produtos</li>

						</ul><br> 
						<h4>3.1 - Grupos de usuários</h4> 
						<p style="margin-left: 2%; font-size: 15px;"><strong> O site de comércio eletrônico 3DHouse possui 3 grupos de usuários (vendedor, cliente e administrador) e cada grupo possui restrições de acesso e funções do sistema. A área de comércio do site 3DHouse é focado no território geográfico do Brasil, abrangendo os 27 estados da federação. Este sistema é focado para um público de interesse em artigos de impressão 3D, sendo assim, de interesse para os atuantes e estudantes das áreas de designers, arquitetos, informática e simpatizantes de movimentos de meio ambiente e conservação ambiental. </strong></p> <br> 

						<p style="margin-left: 2%; font-size: 15px;"><strong> Os usuários do grupo cliente poderão se cadastrar na faixa etária (12 aos 80 anos). Sexos feminino ou masculino, poder aquisitivo alto pertencente as classes média/alta, conhecimentos em tecnologia básico já que uma vez o sistema será bastante intuitivo. O grupo vendedor poderá vender seus produtos no site, ficando a par do administrador do sistema, poderão ser considerados vendedores, usuários entre  12 aos 80 anos, mas dependerão de conta conjunta em uma agencia bancária se forem menores de idade, pertencendo aos sexos masculino e feminino. Os administradores (a principio só possuirá 1)  faixa etária de no mínimo 18 anos, e poder aquisitivo, média/alta. </strong></p><br>


					</div>
				</div> 
			</div> 



<button style="margin-top: 2%;" class="btnprodutos" type="button"><font color="#000"><strong>Menu</strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="100%" size="100" />  

<div class="drawer">

				<!-- First Item -->
				<div class="drawer-item">
					<div class="drawer-header">
						<h5>Clique aqui para ler sobre o menu de itens do site</h5>
						
					</div>
					<div class="drawer-content">
						<h4> 4 - Menu</h4>  
						<h4 style="margin-left: 2%;"> 4.1 - Menu painel do administrador</h4> 
						<p style="margin-left: 2%; font-size: 15px;"><strong> Os menus do painel do administrador são acessíveis apenas pelos níveis de administrador e vendedor, e contém as principais formas de formatação de menus e estilo. Existem três formas de menu. O menu superior mostra apenas conteúdo e funções relevantes para o usuário logado. O segundo menu é um menu vertical, ficando do lado direito da tela de mostragem de dados, com o intuito de facilitar a navegação do usuário para as diferentes funções do sistema. O último menu é a página principal do painel do administrador. Toda vez que um vendedor ou administrador faz login, o mesmo é direcionado para esta página contendo menus com links de redirecionamento para as funções de listagem dos dados do sistema.</strong></p><br> 

						<h4 style="margin-left: 2%;">4.1.1 - Menu superior - painel do administrador</h4> 

						<p style="margin-left: 2%; font-size: 15px;"><strong> Este menu tem como finalidade mostrar dados e funções relevantes de cada usuário que realizar login e estar <font color="#ff0000">cadastrado nos grupos de admin(administrador) e vendedor</font>.</strong></p><br>  

						

						<p style="margin-left: 2%; font-size: 15px;"><strong> O primeiro item do menu é um link chamado paineladministrador, que quando clicado, o usuário será rediecionado para a página principal do painel do adminstrador. O segundo item é mostrado o email do respectivo usuário que está logado. Este item é um neu drop down(suspenso), que quando clicado, ele mostra a opção de alterar dados de cadastro.</strong></p><br>
						<!-- imagem menu superior-->  
						<img style="margin-left: 2%;" src="<?php echo base_url('assets/images/menusuperior.png');?>" width="98%" height="10%"/>  
						
						<br> 
						<br>
						<h4 style="margin-left: 2%;">4.1.2 - Menu vertical - painel do administrador</h4> 
						<p style="margin-left: 2%; font-size: 15px;"><strong> O segundo menu, lista as principais operações do sistema separados por categorias como: manutenção de usuários, manutenção de produtos e mais opções.</strong></p><br> 

						<p style="margin-left: 2%; font-size: 15px;"><strong>A categoria de manutenção de usuários contém um link que redireciona o usuário para a tela de listagem de usuários. A segunda categoria de manutenção de produtos contém os seguintes links: produtos, atributos, departamentos e tipos de atributos. Por fim a categoria de mais opções oferece links para as respectivas funções: Troca de imagens do banner, formas de pagamento e formas de entrega</strong></p><br> 
						<!--  imagem menu -->  
						<img style="margin-left: 40%;" src="<?php echo base_url('assets/images/menuvertical.png');?>" width="20%" height="80%"/>  
						<br> 
						<br>
						<h4 style="margin-left: 2%;">4.1.3 - Menu painel principal - painel do administrador</h4>  
						<p style="margin-left: 2%; font-size: 15px;"><strong>Por fim o menu do painel principal nada mais é do que uma tela principal, onde se encontra links em forma de icones de imagens que quando clicado, redireciona o usuário para a respectiva função. Quando o usuário acessa à area do vendedor, este é a primeira tela apresentada ao usuário, onde o mesmo pode clicar nos ícones e ser redirecionado para a listagem de dados.</strong></p><br>  
						<img style="margin-left: 20%;" src="<?php echo base_url('assets/images/principaladmin.png');?>" width="60%" height="100%"/>  
						<br> 
						<br> 

						<h4 style="margin-left: 2%;">4.2.0 - Menus do grupo vendedor- painel do administrador</h4>  
						<p style="margin-left: 2%; font-size: 15px;"><strong>Para os usuários do grupo vendedor, algumas funções e menus são restritas somente para administradores. O menu superior continua o mesmo, porém o menu do painel principal e o menu do painel vertical mostrarão somente os links referentes aos produtos e a categoria de produtos: produtos, atributos, tipos de atributos e departamentos. </strong></p><br>  
						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/painelvendedor.png');?>" width="100%" height="100%"/>   
						<br> 
						<br>
						<h4 style="margin-left: 2%;">4.3.0 - Menus da página principal</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Na Home(página principal) do site existem 2 tipos de menus. O menu superior, que armazena links e lsitagem de dados como departamentos, logomarca do site, carrinho de compras, dados dos usuários logados, cadastro de novos usuários, alteração de dados, contato da empresa, e a ferramenta de pesquisar. No rodapé do site se encontra liks referentes a empresa como sobre nós, manual do usuário, torne-se um vendedor, home e links referentes as redes sociais.</strong></p><br>  

						<h4 style="margin-left: 2%;">4.3.1 - Menus da página principal- Menu superior</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>O menu superior é um menu horizontal e drop down(suspenso) que acompanha a barra de rolagem do navegador, facilitando ao usuário a troca de páginas do site. No primeiro link encontra-se a logomarca do site que quando clicada, redireciona o usuário para a primeira página Home do site. O segundo link se encontra um menu suspenso dos departamentos. Este menu lista todos os departamentos existentes no sistema. Quando clicado, redireciona o usuário para a listagem de produtos referentes à aqueles respectivos departamentos. O terceiro link é o carrinho de compras que armazenará os produtos escolhidos pelo usuário para a compra.</strong></p><br> 

						<p style="margin-left: 2%; font-size: 15px;"><strong>O menu de usuários vem logo em seguida disponibilizando asopções de login e cadastre-se para os usuários não logados. Para os usuários do grupo cliente, é disponibilizado opções de editar perfil, meus pedidos, e a opção de logout. Para os usuários do grupo vendedor e administrador, outro link alêm dos demais é disponibilizado, o de painel do administrador para o acesso ao painel administrativo. O quinto link é o contato da empresa 3DHouse para os usuários. Ex: número de telefone, endereço, etc. Por fim o link pesquisar faz uma busca pesquisando o testo que o usuário digitar.</strong></p><br> 
						<img style="margin-left: 2%;" src="<?php echo base_url('assets/images/menusuperiorhome.png');?>" width="98%" height="10%"/>   
						<br> 
						<br> 

						<h4 style="margin-left: 2%;">4.3.1 - Menus da página principal- Rodapé</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>O rodapé da 3DHouse contém algumas funcionalidades relevantes. Na aba 3DHouse, contém os links para redirecionar para a Home do site(página principal), e contém outro link que redireciona para o manual do sistema. A segunda aba, lista as redes sociais que a 3DHouse utiliza para promover seus produtos, como facebook twitter e instagran e a terceira aba mostra um plug-in do facebook, mostrando a página da 3DHouse nesta rede social. </strong></p><br>
					</div>
				</div> 
			</div>  


<button style="margin-top: 2%;" class="btnprodutos" type="button"><font color="#000"><strong>Principais Funcionalidades</strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="100%" size="100" />  

<div class="drawer">

				<!-- First Item -->
				<div class="drawer-item">
					<div class="drawer-header">
						<h5>Clique aqui para ler sobre as funcionalidades do sistema</h5> 

						
					</div>
					<div class="drawer-content">
						<h4> 5 - Funcionalidades do site 3DHouse</h4> 
						<h4 style="margin-left: 2%;">5.1 - Funcionalidades do grupo admin</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>O grupo admin do sistema possui sem restrições, a manutenção de 100% do sistema. A administrador tem acesso ao painel administrativo do site, podendo gerenciar e realizar as mais diversas funções e alterações no site. A inserção de novos itens é realizada com o clique no botão (Novo). Em seguida é aberto um formulário para o cadastro deste item. Cada campo deste cadastro que tiver um asterístico (<font color="#ff0000">*</font>) marcado, é um campo de preenchimento obrigatório. Após o cadastro o usuário é redirecionado e é exibido uma mensagem na parte superior da tela, informando que o item foi cadastrado com sucesso ou se algum erro ocorreu. Se o o cadastro ocorreu corretamente, o usuário é redirecionado para a listagem do respectivo item. Tanto paraa manutenção e exclusão do item, basta apenas clicar no botão editar e escolher as opções de excluir ou editar dados. O editar dados abre novamente o formulário de cadastro, recuperando os dados anteriormente cadastrados. Se o usuário não preenche os dados correntamente no cadastro, uma mensagem de erro é mostrado na parte superior da tela. Se o usuário digitou algum valor incorreto, o mesmo é redirecionado novamente para o formulário de cadastro para uma nova tentativa. Este método de cadastro é padrão para todos os itens da loja.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.1.1 - Funcionalidades do grupo admin - Manutenção de usuários</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Somente os usuários que pertencenter ao grupo admin, poderá realizar manutenção de dados de todos os usuários do sistema e de seus respectivos grupos. Além de inserir um novo usuário ou grupo, o admin poderá alterar seus dados cadastrais, desativar seu acesso ao site, impedindo que o mesmo realize login no sistema, e ainda realizar a troca de grupo respectivamente. Ex: o admnistrador poderá trocar o grupo de um usuário de cliente para vendedor. Logo o mesmo usuário poderá ter acesso ao painel administrativo.  </strong></p><br> 

						<h4 style="margin-left: 2%;">5.1.2 - Funcionalidades do grupo admin - Manutenção de banners de imagens</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo admin poderão realizar a troca de banners de imagens para serem exibidos na página principal do site. Estes banners servem como uma amostra de produtos e novidades do site. É possível realizar o upload de várias imagens do tipo: png, gif e jpg, com resolução de 1200x400. 
						Ainda é possível inserir, alterar e excluir a imagem, o comentário e uma descrição da respectiva imagem.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.1.2 - Funcionalidades do grupo admin - Manutenção de Formas de entrega</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo admin poderá cadastrar, alterar e excluir formas de entrega do sistema. O site 3DHouse usará o web service dos correios do Brasil para realização das entregas, no entanto poderá realizar a manutenção dos tipos de serviços de entrega que os correios disponibiliza como PAC, sedex, etc e disponibiliza-las para os usuários do grupo cliente, vendedor ou administrador escolherem a melhor forma que as convém. </strong></p><br>  

						<h4 style="margin-left: 2%;">5.1.2 - Funcionalidades do grupo admin - Manutenção de Formas de pagamento</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo admin poderá cadastrar, alterar e excluir formas de pagamento para serem disponibilizadas na loja. As formas de pagamento são boleto bancário e o pagseguro. Ainda é possível alterar dados destas formas de pagamento como desabilitar, alterar o percentual de desconto e o número máximo de parcelas.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.2.1 - Funcionalidades do grupo admin e vendedor - Manutenção de departamentos</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo vendedor e admin poderão realizar a manutenção dos departamentos da loja. Cada vendedor poderá criar seu próprio departamento e vinvular seus produtos neste departamento. O usuário poderá inserir, alterar e excluir qualquer departamento cadastrado na loja.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.2.2 - Funcionalidades do grupo admin e vendedor - Manutenção de atributos</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo vendedor e admin poderão realizar a manutenção de atributos da loja. Cada vendedor poderá criar seu próprio atributo e vinvular seus atributos aos seus respectivos produtos. Os atributos são características diferenciais de cada produto. O usuário poderá inserir, alterar e excluir qualquer atributo cadastrado na loja. O usuário pode cadastrar um novo atributo e posteriormente poderá vincular seu atributo em um tipo de atributo( ou categoria). Este item é usado para produtos que possuem atributos diferentes. Ex cores diferentes, tipos diferentes de material, etc.</strong></p><br>  

						<h4 style="margin-left: 2%;">5.2.3 - Funcionalidades do grupo admin e vendedor - Manutenção de tipos de atributos</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo vendedor e admin poderão realizar a manutenção de tipos atributos da loja. Cada vendedor poderá criar seu próprio atributo e vinvular seus atributos aos seus respectivos produtos. O usuário poderá inserir, alterar e excluir qualquer atributo cadastrado na loja. O usuário pode cadastrar um novo atributo e posteriormente poderá vincular seu atributo em um tipo de atributo( ou categoria). Este item é usado para produtos que possuem atributos diferentes. Ex cores diferentes, material, etc.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.2.4 - Funcionalidades do grupo admin e vendedor - Manutenção de produtos</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os usuários do grupo vendedor e admin poderão realizar a manutenção de produtos. Os produtos são listados de acordo com o usuário que cadastrou, ou seja, apenas os produtos do respectivo usuário. Na tela de listagem de produtos, existem as opções de Novo, listar(que lista os produtos). No cadastro de produtos nos campos resumo produto e ficha produto, um campo de texto é exibido, podendo o usuário formatar esse testo, inserir imagens, videos, etc. Logo abaixo é mostrado um campo para selecionar se existir um tipo de atributo. Se o produto não tiver um tipo de atributo ele é por padrão um produto simples e atribuido uma referência padrão e quantidade 0. Para editar esses dados, basta acessar a funcionalidade atributos e estoque em editar na view de listagem de produtos. Não será possível alterar este tipo de atributo posteriormente. Os departamentos também estarão listados e poderão ser marcados durante o cadastro do produto. É possível escolher todos os departamentos cadastrados da loja juntamente com seus subníveis. Quando um usuário cadastra um produto, o usuário poderá clicar no botão editar, onde são listado várias funcionalidades além do editar dados do produto, que estarão listadas abaixo. </strong></p><br>   

						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/listagemdeproduto.png');?>" width="100%" height="100%"/> 

						<h4 style="margin-left: 2%;">5.2.4 - Funcionalidades do grupo admin e vendedor - Ficha de cadastro do produto</h4>  

						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/cadastroproduto.png');?>" width="100%" height="100%"/> 

						<h4 style="margin-left: 2%;">5.3 - Funcionalidades Avançadas do grupo admin e vendedor - Manutenção de produtos - Editar atributos  e estoque </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Esta funcionalidade é onde ocorre a Unidade de manutenção de estoque(código de referência e a quantidade de estoque) e a vinculação de um ou mais atributos para o respectivo produto. Essa tela lista os atributos juntamente com as informações de SKU. Para adicionar um sku a um produto primeiramente o produto deve estar vinculado a um tipo de atributo(que ocorre no momento do cadastro do produto). Depois, são listados todos os atributos que são vinculados á aquele tipo e atributo mais as informações de sku. Ex tipo de atributo(cor), atributos(azul, vermelho, verde, etc). Então coda produto pode ter mais de um atributo e quantidades diferentes de cada. O usuário poderá remover esse atributo marcando um campo de chekbox (caixa d marcação) e depois clicar em salvar. Na página de ficha do produto da loja, é listado os atributos do espectivo produto e informações se o produto está disponível.</strong></p><br>  

						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/sku.png');?>" width="100%" height="100%"/>

						<h4 style="margin-left: 2%;">5.3 - Funcionalidades Avançadas do grupo admin e vendedor - Manutenção de produtos - Adicionar fotos </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Esta funcionalidade é onde ocorre a vinculação de fotos dos pordutos. Para o produto ser exibido na página principal, é necessário inserir uma foto. Para adicionar uma foto, basta clicar no botão adicionar fotos e em seguida é carregado um formulário. Neste formulário o usuário faz o upload da foto e escolhe marcar a foto como foto principal. esta foto principal é a exibida na grade de produtos na página principal. As fotos vinculadas poderão ser excluídas e alteradas conforme o necessário. As fotos que forem vinculadas serão apresentadas para o usuário na próxima vez que a função de adicionar fotos for escolhida.</strong></p><br> 

						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/fotoproduto.png');?>" width="100%" height="100%"/>

						<h4 style="margin-left: 2%;">5.4 - Funcionalidades Avançadas - Hierarquia de departamentos </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os departamentos quando inseridos por um administrador ou vendedor poderá ser vinculado um departamento filho ou seja uma hierarquia. Ex (departamento 4 --- departamento 4.1). OS produtos são separados e mostrados de acordo com o departamento pai, ou seja, se o usuário selecionar um departamento filho, os produtos do departamento pai serão listados também. A criação de uma hierarquia de departamentos pode ser feita no cadastro do departamento. É mostrado um campo de seleção, indicando se o usuário deseja vincular seu departamento com outro. Feito isto, o departamento criado se torna filho do departamento que o usuário vinculou. 
						 </strong></p><br>  
						 <img style="margin-left: 0%;" src="<?php echo base_url('assets/images/departamento.png');?>" width="100%" height="100%"/>
						 <h4 style="margin-left: 2%;">5.5 - Funcionalidades da loja - pesquisa de produtos </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>A pesquisa de produtos é feita por meio de uma extensão do banco de dados postgres que de verificação de similaridades. Esta busca se baseia na similaridade da palavra digitada pelo usuário e o nome do produto. É levado em conta uma similaridade de 10% em relação ao texto que o usuário digitar.</strong></p><br> 
						<h4 style="margin-left: 2%;">5.6 - Funcionalidades da loja - carrinho de compras </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>O carrinho de compras armazena os produtos escolhidos pelo usuário. É possível atualizar e remover itens no carrinho de compras. Também é possível verificar os pedidos referentes ao usuário.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.7 - Funcionalidades da loja - Meios de pagamento </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os meios de pagamentos disponibilizados serão o boleto e pagseguro. É usado uma biblioteca de integração com o pagseguro e o boleto. Esta biblioteca é a responsável pela configuração e integração destes serviços já que a própria loja não possui uma implementação de meio de pagamento próprio. Os meios de pagamento são lsitados para a escolha do usuário na página de confirmação de compra e dados cadastrais.</strong></p><br>   
						 <h4 style="margin-left: 2%;">5.8 - Funcionalidades da loja - Web service de busca de endereço </h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Para facilitar o cadastro dos usuários da loja é possível fazer uma busca dos dados de cidade, estado, rua e bairro através do cep. Primeiramente o usuário digita o cep e clicando em buscar, é realizado uma busca em uma base de dados web que auto-preenche estes dados nos campos de cadastro.</strong></p><br>   

						<h4 style="margin-left: 2%;">5.9 - Funcionalidades da loja - Web service dos correios do Brasil</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os produtos serão enviados pelos correios, e possuirão o custo do frete incluso na ficha de pagamento do produto. </strong></p><br>   

						<h4 style="margin-left: 2%;">5.10 - Funcionalidades da loja - Ordenação de produtos</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Os produtos serão armazenados de acordo com a sua inserção no painel administrativo. Na página principal, serão mostrados os 6 últimos produtos cadastrados. Logo abaixo será mostrados os 6 ultimos produtos que contém um preço promocional.</strong></p><br> 

						<h4 style="margin-left: 2%;">5.11 - Funcionalidades da loja - Comentários na ficha do produto</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>O plug-in de comentários disqs é utilizado em todas as fichas dos produtos. Seu uso é para comentários, discuções e suporte. Para realizar um comentário, basta o usuário possuir uma conta em uma rede social como disqs, twitter, facebook e google. </strong></p><br> 
						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/fichaproduto.png');?>" width="100%" height="100%"/>

					</div>
				</div> 
			</div>  

<a name="problemas"></a> 
<button style="margin-top: 2%;" class="btnprodutos" type="button"><font color="#000"><strong>Solução de problemas</strong></font></button>
<hr class="hr" noshade="noshade" align="left" width="100%" size="100" />   

<div class="drawer">

				<!-- First Item -->
				<div class="drawer-item">
					<div class="drawer-header">
						<h5>Clique aqui para ler as soluções de problemas</h5>
						
					</div>
					<div class="drawer-content">
						<h4> 6 - Problemas</h4> 
						<h4 style="margin-left: 2%;">6.1 - Problemas de cadastro de usuário</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Algumas formatações de dados são inválidas e resultarão em erros durante o cadastro. O campo cpf só aceitará números sem traços. A senha do usuário deverá ser de 8 a 20 dígitos. O campo corfirme senha, onde ocorre uma verificação da senha anteriormente digitada. Todos esses campos possuem logo abaixo exemplos de como deve ser preenchidos. Caso o usuário não siga esses exemplos, uma mensagem em ofrma de banner é exibida na parte superior da tela informando o erro. </strong></p><br>   

						<h4 style="margin-left: 2%;">6.1 - Produtos indisponíveis</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Eventualmente alguns produtos podem ficar indisponíveis ou fora de estoque. Para isto uma tela irá informar que o determinado produto está indisponível e o mesmo será automaticamente redirecionado para a página principal da loja e na ficha de produto indicará se o determinado produto está com o estoque esgotado. </strong></p><br>   
						<img style="margin-left: 0%;" src="<?php echo base_url('assets/images/produtoindisponivel.png');?>" width="100%" height="100%"/>

						<h4 style="margin-left: 2%;">6.2 - Comentários na ficha do produto - Suporte</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>Na ficha técnica de cada produto é exibido os comentários de cada produto. Caso ocorra algum erro ou problema, o usuário poderá inserir um comentário que posteriormente será visualizado e o problema corrigido. </strong></p><br>  

						<h4 style="margin-left: 2%;">6.2 - Página do facebook - Suporte</h4>   
						<p style="margin-left: 2%; font-size: 15px;"><strong>O usuário, caso necessário, poderá contactar pela página da empresa no facebook. Esta página fica listada no rodapé do site. </strong></p><br> 
					</div>
				</div> 
			</div> 

<br> 
<br>
</div>