

    <div style="margin-top: 80px;" id="login-overlay" class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
             
              <h4 class="modal-title" id="myModalLabel"><?php echo lang('loginloja');?></h4>
          </div>
          <div class="modal-body"> 
          <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('message') ?>
                        </div>
                    <?php endif; ?>
              <div class="row">
                  <div class="col-xs-6">
                      <div class="well">
                          <?=form_open('cadastrousuario/login')?>
                              <div class="form-group">
                                  <label for="identity" class="control-label">E-mail</label>
                                  <input type="text" class="form-control" id="identity" name="identity" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">Senha</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">erro! e-mail ou senha inválida</div>
                              <p>
    
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> 
  <?php echo lang('login_remember_label', 'remember');?>
  </p>
                              <button style="background: #006400;" type="submit" class="btn btn-success btn-block"><em class="glyphicon glyphicon-log-in"></em> Login</button>
                              <a href="../auth/forgot_password" class="btn btn-default btn-block">Esqueceu sua senha?</a>
                          <?php echo form_close();?> 
                      </div>
                  </div>
                  <div class="col-xs-6">
                      <p class="lead">Crie uma conta <span class="text-success">Grátis</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> Compre produtos</li>
                          <li><span class="fa fa-check text-success"></span> Entrega garantida</li>
                          <li><span class="fa fa-check text-success"></span> Seus dados então seguros</li>
                          <li><span class="fa fa-check text-success"></span> Melhor catálogo de itens de impressão 3D</li>
                          <li><span class="fa fa-check text-success"></span> Impressão sob encomenda <small>(Em Breve!)</small></li>
                         
                      </ul>
                      <p><a style="background: #006400;" href="create_user" class="btn btn-success btn-block">Registre-se</a></p>
                  </div>
              </div>
          </div>
      </div>
  </div>