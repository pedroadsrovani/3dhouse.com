<legend> 
Manutenção de formas de pagamento- {ACAO}

</legend> 

  
<form action="{ACAOFORM}" method="post" class="form-horizontal">  
<!-- formulario nome --> 

      <input type="hidden" name="codformapagamento" id="codformapagamento" value="{codformapagamento}"> 
<label>Nome da forma de pagamento<font color="#FF0000"> *</font></label>
<div class="input-group"> 

    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>  
    <input  type="text" id="nomeformapagamento" class="form-control" name="nomeformapagamento" value="{nomeformapagamento}" placeholder="Digite uma forma de pagamento...">
     <label class="control-label" for="nomeformapagamento"></label>
  </div>   
  <br>  
         
         <div class="input-group"> 

              <label class="control-label" for="tipoformapagamento">Tipos de pagamentos <font color="#FF0000"> *</font></label>
              <div class="controls"> 

                 <select class="form-control" name="tipoformapagamento" id="tipoformapagamento"><!-- {des_tipoatributo} =  desabilita o form tipoatributo no update -->
                    <option value="1" {sel_tipoformapagamento1}>Boleto</option>
                                                  
                    <option value="2" {sel_tipoformapagamento2}>Pagseguro</option>
                                                 
                   </select>
              </div>
          </div> 

  <br>
<label>Digite maximo de desconto em % <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
    <input  type="text" id="descontoformapagamento" class="form-control" name="descontoformapagamento" value="{descontoformapagamento}">
     <label class="control-label" for="descontoformapagamento"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      <font color="#FF0000"> Atenção! </font> Não utilize vírgula(,), ponto(.) e nem o símbolo de desconto(%) neste campo de formulário.
    </small>  

<br> 
<br>

 

  <label>Digite maximo de parcelas de pagamento X <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
    <input  type="text" id="maximoparcelaspagamento" class="form-control" name="maximoparcelaspagamento" value="{maximoparcelaspagamento}">
     <label class="control-label" for="maximoparcelaspagamento"></label>
  </div>   

<br> 

<label>Marque a checkbox se deseja habilitar a forma de pagamento no site<font color="#FF0000"> *</font></label>
  <div class="input-group">
    
    <div class="controls"> 
    <label class="control-label" for="habilitaformapagamento"></label>
    <input  type="checkbox" {chk_habilitaformapagamento} name="habilitaformapagamento" value="S"> Habilitar no site?
     </div>
     
  </div>   

 <br>
  </br> 
  <div class="vell"> 
  <button type="submit" class="btn btn-primary">Salvar</button>  <a href="{URLLISTAR}" title="Listar Usuários" class="btn btn-primary">Voltar</a><!--joga os usuarios listados a direita -->
</div>
</form> 