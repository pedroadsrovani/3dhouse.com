<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <div id="body">
      <?php if(validation_errors() || isset($error)) : ?>
         <div class="alert alert-danger" role="alert" align="center">
            <?=validation_errors()?>
            <?=(isset($error)?$error:'')?>
         </div> 
<?php endif; ?>
   <h3>Adicionar nova imagem</h3>

   
      
      <?=form_open_multipart('painel/produto/save_photo')?> 
     <input type="hidden" name="codproduto" id="codproduto" value="<?= $product_id;?>">

        <div class="form-group"> 
          <input type="file" class="form-control" name="userfile" style="width:50%">
        
        <small id="passwordHelpInline" class="text-muted">
      Clique em escolher arquivo e selecione a imagem
    </small></div> 
       </br>  
    
       
       
        <label>Marque a checkbox se deseja marcar essa foto como principal deste produto<font color="#FF0000"> *</font></label>
  <div class="input-group">
    
    <div class="controls"> 
    <label class="control-label" for="fotoprincipal"></label>
    <input  type="checkbox" {fotoprincipal} name="fotoprincipal" value="S"> Foto principal?
     </div>
     
  </div>   

 <br>
        <button type="submit" class="btn btn-primary">Salvar</button>
        <?=anchor('painel/produto/uploadfoto/'.$product_id,'Cancelar',['class'=>'btn btn-danger'])?>

      </form>
   </div>

   

