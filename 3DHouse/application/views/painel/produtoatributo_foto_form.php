
<legend>
	Vincular Fotos com Atributos do Produto: {NOMEPRODUTO}
	<div class="pull-right">
		
	</div>
</legend>
<form action="{URLSALVAFOTOATRIBUTO}" method="post">
	<input type="hidden" name="codproduto" value="{CODPRODUTO}">

	<table class="table table-bordered table-condensed">
		<tr>
			<th style="width: 200px;">Imagem</th>
			<th> Vincular com atributos</th> 
			
		</tr>
		{BLC_FOTOS}
		<tr>
			<td><img src="{URLIMAGEM}" style="width: 100%;"></td>
			<td><select name="skus[{CODPRODUTOFOTO}][]" multiple="multiple" >
				
			</style>>
					{BLC_SKUSPRODUTO}
					<option value="{CODSKU}">{NOMESKU}</option> {/BLC_SKUSPRODUTO}
			</select> 

		
		</tr>
		{/BLC_FOTOS}
		{BLC_SEMFOTOS}
		<tr>
			<td colspan="2">Não foram encontradas fotos para este produto.</td>
		</tr>
		{/BLC_SEMFOTOS}

	</table>
<br>
<button type="submit" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em> Salvar</button>
        <a href="../"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a>

	
</form>

                                                                             