<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->input->post()){
   
  
   $fotoprincipal    = set_value('chk_fotoprincipal');
} else {
  
   $descricao    = $produtofoto->fotoprincipal;
}
?>



   <h3>Alterar imagem</h3>

   <div id="body">
      <?php if(validation_errors() || isset($error)) : ?>
         <div class="alert alert-danger" role="alert" align="center">
            <?=validation_errors()?>
            <?=(isset($error)?$error:'')?>
         </div>
      <?php endif; ?>
      <?=form_open_multipart('painel/produto/edit/'.$produtofoto->codprodutofoto)?>

        <div class="form-group">
          <label for="userfile"></label> 
          
          <div class="row" style="margin-bottom:5px"><div class="col-xs-12 col-sm-6 col-md-3"><?=img(['src'=>$produtofoto->tipo,'width'=>'100%'])?></div></div>
          
          <input type="file" class="form-control" style="whidth:50%" name="userfile">
        </div>

       <label>Marque a checkbox se deseja marcar essa foto como principal deste produto<font color="#FF0000"> *</font></label>
  <div class="input-group">
    
    <div class="controls"> 
    <label class="control-label" for="fotoprincipal">Foto principal?</label>
    <input  type="checkbox" {fotoprincipal} name="fotoprincipal" value="S"> Sim
    <input  type="checkbox" {fotoprincipal} name="fotoprincipal" value="N"> Não 
     </div>
     
  </div>   

        </br>
        <button type="submit" class="btn btn-primary">Salvar</button>
        <?=anchor('painel/produto','Cancelar',['class'=>'btn btn-danger'])?>

      </form>
   </div>


