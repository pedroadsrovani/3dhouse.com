   
<legend>
Manutenção de Produtos - {ACAO}
	
</legend>
<form action="{ACAOFORM}" method="post" class="form-horizontal"> 
   
	       <input type="hidden" name="codproduto" id="codproduto" value="{codproduto}">
	         <label for="resumoproduto">Nome do produto: <font color="#ff0000">*</font></label>
	          <div class="input-group"> 

                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input type="text" id="nomeproduto" class="form-control" name="nomeproduto" value="{nomeproduto}" required="required" placeholder="Digite um nome de produto">
                <label class="control-label" for="nomeproduto"></label>
           </div>  
           <br> 

              
            
            
           <div class="form-group">
                 <label for="resumoproduto">Resumo do produto: <font color="#ff0000">*</font></label>
                 <textarea class="form-control" id="resumoproduto" name="resumoproduto" rows="6" placeholder="Digite o resumo do produto aqui...">{resumoproduto}</textarea>
            </div>
            <br>
                  <div class="form-group">
                      <label for="fichaproduto">Ficha técnica do produto: <font color="#ff0000">*</font></label>
                      <textarea class="form-control" id="fichaproduto" name="fichaproduto" rows="6" placeholder="Digite informações da ficha técnica do produto aqui...">{fichaproduto}</textarea>
                 </div>
                 <br> 


	                
                 <br> 

                       <label>Digite um valor pro produto: <font color="#ff0000">*</font></label>
                       <div class="input-group"> 
                          <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                          <input type="text" id="valorproduto"  class="form-control set-numeric" name="valorproduto" value="{valorproduto}" required="required" placeholder="">
                          <label class="control-label" for="valorproduto"></label>
                      </div>  
                      <br>  

                           <label>Digite um valor promocional pro produto: (opcional)</label>
                           <div class="input-group"> 
                               <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                              <input type="text" id="valorpromocional" class="form-control set-numeric" name="valorpromocional" value="{valorpromocional}"><!-- classe set-numeric  será utilizado como referencia pra o plugin do jquery maskedinput percorrer e formatar utilizando uma mascara especifica para valor-->
                               <label class="control-label" for="valorpromocional"></label>
                           </div>   

                           <br>
                    
                             <!--form de tipo de atributo-->
	                                   <div class="control-group">
		                                    <label class="control-label" for="codtipoatributo">Tipo de atributo pertencente ao produto: (opcional)</label>
	                                          <div class="controls">
	    	                                          <select class="form-control" name="codtipoatributo" id="codtipoatributo" {des_tipoatributo} class="set-quantidade-sku"><!-- {des_tipoatributo} =  desabilita o form tipoatributo no update -->
	    	                                          <option value="">Não especificado</option>
	    	                                        	{BLC_TIPOATRIBUTOS}
	    		                                       <option value="{CODTIPOATRIBUTO}" {sel_sel_codtipoatributo}>{NOME}</option>
	    		                                        {/BLC_TIPOATRIBUTOS}
	    	                                          </select>
	                                            </div>
	                                    </div> 
                                       
                            
  	                                  
		                                 
	       
             <br>   
             <br> 
             <!-- form de departamento-->
             <div class="row-fluid"> 
                    <div class="span8"> 
                    </div> 
            </div>  
                         <div class="span6">  
                         
                            <h4>Departamentos <font color="#ff0000">*</font></h4>  
                                <ul class="list-unstyled">  
                                {BLC_DEPARTAMENTOPAI}   <!--bloco que busca o cod do departamento pai -->                                                                                        <!--são vários departamentos então tem que vim em um array -->                  <!-- lista o nome do departamento pai--> 
                                     <li><label for="departamento-{CODDEPARTAMENTO}" class="radio-inline checkbox"><input {chk_departamentopai} name="departamento[]" class="set-departamento-pai" type="checkbox" id="departamento-{CODDEPARTAMENTO}" value="{CODDEPARTAMENTO}">{NOMEDEPARTAMENTO}</label></li> 
                                     <li>                                                                                                                             <!--classe de referencia para codigo javascript loja.js -->                         
                                         <ul>
                                       {BLC_DEPARTAMENTOFILHO} <!--bloco que lista  o departamento filho (cod)-->  
                                                                                                                                                      --> 
                                             <li><label for="departamento-{CODDEPARTAMENTOFILHO}" class="radio-inline checkbox"><input {chk_departamentofilho} name="departamento[]" class="set-departamento-filho" data-pai="{CODDEPARTAMENTOPAI}" type="radio" id="departamento-{CODDEPARTAMENTOFILHO}" value="{CODDEPARTAMENTOFILHO}">{NOMEDEPARTAMENTOFILHO}</label></li>
                                                                                                                            pai -->
                                       {/BLC_DEPARTAMENTOFILHO}  
                                        </ul>
                                    </li>
                                       {/BLC_DEPARTAMENTOPAI}
                               </ul>
                          </div> 
                    <br>   
                    <hr>   
                    <legend>
                      Expedição do produto - {ACAO}
  
                    </legend> 
                  <br>
                    <label for="pesoproduto">Peso do produto <font color="#ff0000">*</font></label>
            <div class="input-group"> 

                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input style="width: 50%;" type="text" id="pesoproduto" class="form-control" name="pesoproduto" value="{pesoproduto}" required="required" placeholder="Ex: 0.50">
                <label class="control-label" for="pesoproduto"></label>
           </div>  
           <small id="passwordHelpInline" class="text-muted">
     Peso deve ser informado em kilogramas: 0.50Kg, 0.100kg
    </small>   
           <br>  

           <label for="comprimentoproduto">Comprimento <font color="#ff0000">*</font></label>
            <div class="input-group"> 

                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input style="width: 50%;" type="text" id="comprimentoproduto" class="form-control" name="comprimentoproduto" value="{comprimentoproduto}" required="required" placeholder="Digite o comprimento do produto. Ex 100, 200">
                <label class="control-label" for="nomeproduto"></label>
           </div>  
           <small id="passwordHelpInline" class="text-muted">
     O comprimento deve ser informado em centímetros: 100 cm, 200 cm
    </small> 
           <br>  
           <label for="alturaproduto">Altura <font color="#ff0000">*</font></label>
            <div class="input-group"> 

                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input style="width: 50%;" type="text" id="alturaproduto" class="form-control" name="alturaproduto" value="{alturaproduto}" required="required" placeholder="Digite a altura produto.Ex: 50">
                <label class="control-label" for="alturaproduto"></label>
           </div>  
           <small id="passwordHelpInline" class="text-muted">
     A altura deve ser informado em centímetros: 50 cm, 100 cm
    </small> 
           <br>  

           <label for="larguraproduto">Largura <font color="#ff0000">*</font></label>
            <div class="input-group"> 

                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input style="width: 50%;" type="text" id="larguraproduto" class="form-control" name="larguraproduto" value="{larguraproduto}" required="required" placeholder="Digite a largura produto.Ex: 50">
                <label class="control-label" for="larguraproduto"></label>
           </div>  
           <small id="passwordHelpInline" class="text-muted">
    A largura deve ser informado em centímetros: 50 cm, 100 cm
    </small>  
           <br>  

           <label for="diametroproduto">Diâmetro <font color="#ff0000">*</font></label>
            <div class="input-group"> 

                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input style="width: 50%;" type="text" id="diametroproduto" class="form-control" name="diametroproduto" value="{diametroproduto}" required="required" placeholder="Digite o diametro produto.Ex 50">
                <label class="control-label" for="diametroproduto"></label>
           </div>   
           <small id="passwordHelpInline" class="text-muted">
     O diâmetro deve ser informado em centímetros: 50 cm, 100 cm
    </small> 
           <br>  



                    <br>   
                   <button type="submit" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em>  Salvar</button>
        <a href="{URLLISTAR}"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a>
</form>