<legend>
Atributos<br>
	<div class="pull-right"> 
	    <a href="{URLLISTAR}" title="Listar atributos" class="btn pull-right"><button type="button" class="btn btn-primary"><em class="glyphicon glyphicon-th-list"></em> Listar</button></a><!--joga os usuarios listados a direita --> 
<a href="{URLADICIONAR}" title="Adicionar atributos" class="btn pull-right"><button type="button" class="btn btn-primary">  <em class="glyphicon glyphicon-asterisk"></em> Novo</button></a><!--joga os usuarios listados a direita -->

	</div>
</legend>
<table class="table table-bordered table-condensed">
	<tr>
		<th class="coluna-acao text-center">Manutenção</th>
		<th>Nome</th>
		<th>Tipo</th>
		
	</tr>
	{BLC_DADOS}
	<tr> 
<td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><em class="glyphicon glyphicon-pencil"></em> Editar 
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="{URLEDITAR}">Editar Atributo</a></li>
      <li><a href="{URLEXCLUIR}">Excluir Atributo</a></li>
      
    </ul>
 
</div>
</td>

 		 
 <td>{NOME}</td>
		<td>{NOMETIPO}</td>
	</tr>
	{/BLC_DADOS}
	{BLC_SEMDADOS}
	<tr>
		<td colspan="4" class="alinha-centro">Não há dados</td>
	</tr>
	{/BLC_SEMDADOS}
</table>
<nav aria-label="..." class="text-center">
	<ul class="pagination pagination-sm">
		<li class="{HABANTERIOR}"><a href="{URLANTERIOR}">&laquo;</a>
		{BLC_PAGINAS}
		<li class="{LINK}"><a href="{URLLINK}">{INDICE}</a></li>
		{/BLC_PAGINAS}
		<li class="{HABPROX}"><a href="{URLPROXIMO}">&raquo;</a>
	</ul>
</nav>
