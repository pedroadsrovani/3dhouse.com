<legend> 
Manutenção de formas de entrega - {ACAO}

</legend> 

  
<form action="{ACAOFORM}" method="post" class="form-horizontal">  
<!-- formulario nome --> 

      <input type="hidden" name="codformaentrega" id="codformaentrega" value="{codformaentrega}"> 
<label>Nome da forma de entrega<font color="#FF0000"> *</font></label>
<div class="input-group"> 

    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>  
    <input  type="text" id="nomeformaentrega" class="form-control" name="nomeformaentrega" value="{nomeformaentrega}" placeholder="Digite uma forma de entrega...">
     <label class="control-label" for="nomeformaentrega"></label>
  </div>   

  <br>
<label>Digite o codigo dos correios<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
    <input  type="text" id="codigocorreiosformaentrega" class="form-control" name="codigocorreiosformaentrega" value="{codigocorreiosformaentrega}">
     <label class="control-label" for="codigocorreiosformaentrega"></label>
  </div>   
<br>
<label>Marque a checkbox se deseja habilitar a forma de entrega no site<font color="#FF0000"> *</font></label>
  <div class="input-group">
    
    <div class="controls"> 
    <label class="control-label" for="habilitaformaentrega"></label>
    <input  type="checkbox" {chk_habilitaformaentrega} name="habilitaformaentrega" value="S"> Habilitar no site?
     </div>
     
  </div>



 <br>
  </br> 
  <div class="vell"> 
  <button type="submit" class="btn btn-primary">Salvar</button>  <a href="{URLLISTAR}" title="Listar Usuários" class="btn btn-primary">Voltar</a><!--joga os usuarios listados a direita -->
</div>
</form> 
  