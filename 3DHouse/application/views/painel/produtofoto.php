
<div id="container"> 
    <div id="body">  
    <div class="alert alert-info alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Info!</strong> Resolução máxima: 1000x700 tipos: gif | png | jpg.
</div> 
      <?php if($produtofoto->num_rows() > 0) : ?>
         
         <?php if($this->session->flashdata('message')) : ?>
            <div class="alert alert-success" role="alert" align="center">
               <?=$this->session->flashdata('message')?>
            </div> 
        <?php endif; ?>  
        <br>   
        
   <h3>Galeria de Imagens do produto <?php echo $CODPRODUTO ?>:   <small id="passwordHelpInline"  class="text-muted">
       <?php echo $NOMEPRODUTO; ?> 
      
    </br> 
    <br>

   
          
         
         
         <hr />    
         <div class="row">
            <?php foreach($produtofoto->result() as $img) : ?>
            <div class="col-md-3">
               <div class="thumbnail">
                  <?=img($img->tipo)?>
                  <div class="caption">
                    
                     
                     <p> 
                         
                        <?=anchor('painel/produto/edit/'.$img->codprodutofoto,'Editar',['class'=>'btn btn-primary btn-xs', 'role'=>'button'])?> <?=anchor('painel/produto/delete/'.$img->codprodutofoto,'Excluir',['class'=>'btn btn-danger btn-xs', 'role'=>'button'])?>
                       
                     </p>
                  </div>
               </div>
            </div> 

            <?php endforeach; ?>
         </div>
      <?php else : ?>
        
      <?php endif; ?>
   </div>

</div>
<div class="btn_add" align="center"><?=anchor('painel/produto/add/'.$CODPRODUTO,'Adicionar fotos',['class'=>'btn btn-success'])?><a href="<?php echo base_url('index.php/painel/produto'); ?>" title="Listar Departamentos" style="margin-left: 10px;" class="btn btn-primary">Voltar</a>
  </div> 

