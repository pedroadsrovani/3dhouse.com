<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->input->post()){
   $comentario       = set_value('comentario');
   $descricao    = set_value('descricao'); 
   $link       = set_value('link');
} else {
   $comentario       = $imagens->comentario;
   $descricao    = $imagens->descricao; 
   $link    = $imagens->link;
}
?><!DOCTYPE html>
<html lang="pt-br">
<head>
   <meta charset="utf-8">
   <title>Alterar imagem</title>

   <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
   
   <style type="text/css">

   ::selection { background-color: #E13300; color: white; }
   ::-moz-selection { background-color: #E13300; color: white; }

   

   a {
      color: #003399;
      background-color: transparent;
      font-weight: normal;
   }

  

   code {
      font-family: Consolas, Monaco, Courier New, Courier, monospace;
      font-size: 12px;
      background-color: #f9f9f9;
      border: 1px solid #D0D0D0;
      color: #002166;
      display: block;
      margin: 14px 0 14px 0;
      padding: 12px 10px 12px 10px;
   }

   #body {
      margin: 0 15px 0 15px;
   }

   
   </style>
</head>
<body>


   <h3>Alterar imagem</h3>

   <div id="body">
      <?php if(validation_errors() || isset($error)) : ?>
         <div class="alert alert-danger" role="alert" align="center">
            <?=validation_errors()?>
            <?=(isset($error)?$error:'')?>
         </div>
      <?php endif; ?>
      <?=form_open_multipart('gallery/edit/'.$imagens->codimagen)?>

        <div class="form-group">
          <label for="userfile"></label> 
          
          <div class="row" style="margin-bottom:5px"><div class="col-xs-12 col-sm-6 col-md-3"><?=img(['src'=>$imagens->tipo,'width'=>'100%'])?></div></div>
          
          <input type="file" class="form-control" style="whidth:50%" name="userfile">
        </div>

        <div class="form-group">
          <label for="comentario">Comentário</label>
          <input type="text" class="form-control" name="comentario"  value="<?=$comentario?>">
        </div> 
        <br>
        <div class="form-group">
          <label for="link">Link pro banner</label>
          <input type="text" class="form-control" name="link"  value="<?=$link?>">
        </div>

        <div class="form-group">
          <label for="descricao">Descrição</label>
          <textarea class="form-control" rows="6" name="descricao"><?=$descricao?></textarea>
        </div> 
        
        
        </br>
        <button type="submit" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em> Salvar</button>
        <a href="../index"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a>
      </form>
   </div>



</body>
</html>