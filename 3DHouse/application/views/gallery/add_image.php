<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
<head>
   <meta charset="utf-8">
   <title>Adicionar nova imagem</title>

   <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
  
   <style type="text/css">

   ::selection { background-color: #E13300; color: white; }
   ::-moz-selection { background-color: #E13300; color: white; }

   

   a {
      color: #003399;
      background-color: transparent;
      font-weight: normal;
   }

   

   code {
      font-family: Consolas, Monaco, Courier New, Courier, monospace;
      font-size: 12px;
      background-color: #f9f9f9;
      border: 1px solid #D0D0D0;
      color: #002166;
      display: block;
      margin: 14px 0 14px 0;
      padding: 12px 10px 12px 10px;
   }

   
   </style>
</head>
<body>


  <div id="body">
      <?php if(validation_errors() || isset($error)) : ?>
         <div class="alert alert-danger" role="alert" align="center">
            <?=validation_errors()?>
            <?=(isset($error)?$error:'')?>
         </div> 
<?php endif; ?>
   <h3>Adicionar nova imagem</h3>

   
      
      <?=form_open_multipart('gallery/add')?>

        <div class="form-group"> 
          <input type="file" class="form-control" name="userfile" style="width:50%">
        
        <small id="passwordHelpInline" class="text-muted">
      Clique em escolher arquivo e selecione a imagem
    </small></div> 
       </br>  
    
       
        <div class="form-group"> 
          <label for="comentario">Comentario</label>
          <input type="text" class="form-control" name="comentario" value="" style="width:50%">
        </div>  
        <br>
        <div class="form-group"> 
          <label for="comentario">Link do banner</label>
          <input type="text" class="form-control" name="link" value="" style="width:50%">
        </div>
</br>
        <div class="form-group">
          <label for="descricao">Descrição</label>
          <textarea class="form-control" name="descricao" style="width:100%" rows="6"></textarea>
        </div>
</br> 
        
        <button type="submit" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em> Salvar</button>
        <a href="index"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a>

      </form>
   </div>

   


</body>
</html>