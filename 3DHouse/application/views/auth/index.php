<?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-warning fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('message') ?>
                        </div>
                    <?php endif; ?>
<legend>  

Listagem de Usuários<br> 

<!-- URLLISTAR lista os admins já adicionados --> 
<!-- URLADICIONAR bloco que chama o metodo adicionar admins -->
<a href="<?php echo site_url('auth/index');?>" title="Listar usuarios" class="btn pull-right"><button type="button" class="btn btn-primary"><em class="glyphicon glyphicon-th-list"></em> Listar</button></a>   




<a href="<?php echo site_url('auth/create_group');?>" title="Listar usuarios" class="btn pull-right"><button type="button" class="btn btn-primary"><em class="glyphicon glyphicon-asterisk"></em> Criar grupo</button></a> 

<a href="<?php echo site_url('auth/create_user');?>" title="Adicionar usuarios" class="btn pull-right"><button type="button" class="btn btn-primary">  <em class="glyphicon glyphicon-asterisk"></em> Novo usuário</button></a><!--joga os usuarios listados a direita -->

</legend> 
<table class="table table-bordered table-condensed"> <!-- classe bootastrap para tabelas -->
   <tr> 
        <th class="coluna-acao text-center">Manutenção</th>   
        <th><?php echo lang('index_fname_th');?></th>  
         <th><?php echo lang('index_lname_th');?></th>   
         <th><?php echo lang('index_email_th');?></th> 
        
        <th class="coluna-acao text-center">Grupo</th>   
          <th class="coluna-acao text-center">Status</th> 

   </tr> 

   <!--{BLC_DADOS}--> <?php foreach ($usuarios as $user):?><!-- bloco para listagem de dados --> 
   <tr>  
      

      <td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><em class="glyphicon glyphicon-pencil"></em> Editar 
    <span class="caret"></span></button>
    <ul class="dropdown-menu"> 

      <li><?php echo anchor("auth/edit_user/".$user->codusuario, 'Editar dados') ;?></li> 
      
      <?php foreach ($user->grupos as $group):?>
      <li><?php echo anchor("auth/edit_group/".$group->codgrupo, 'Editar grupo') ;?></li>  
      <?php endforeach?> 

      <li><?php echo ($user->ativo) ? anchor("auth/deactivate/".$user->codusuario, 'Editar status') : anchor("auth/activate/". $user->codusuario, 'Editar status');?></li>
      
    </ul>
 
</div>
</td>
        
        <td><?php echo htmlspecialchars($user->primeironome,ENT_QUOTES,'UTF-8');?></td> <!-- bloco que busca o nome do admin -->
        <td><?php echo htmlspecialchars($user->sobrenome,ENT_QUOTES,'UTF-8');?></td><!-- antigo {ATIVO}--> 
         <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td> 
         <td><?php echo htmlspecialchars($group->nome,ENT_QUOTES,'UTF-8') ;?><br /></td> 
         <td><?php echo ($user->ativo) ? anchor("auth/deactivate/".$user->codusuario, lang('index_active_link')) : anchor("auth/activate/". $user->codusuario, lang('index_inactive_link'));?></td>



   </tr> 
   <!--{/BLC_DADOS}--> <?php endforeach;?>
   <!--{BLC_SEMDADOS}-->  <!-- bloco que mostra quando não há dados para serem mostrados -->
   <tr> 
        
        <!--<td colspan="3" class="text-center"> Não há dados!</td> -->
        
   </tr> 
  <!-- {/BLC_SEMDADOS}-->
</table>   

<nav aria-label="..." class="text-center">
  <ul class="pagination pagination-sm">
    <li class="{HABANTERIOR}"><a href="{URLANTERIOR}">&laquo;</a>
    {BLC_PAGINAS}
    <li class="{LINK}"><a href="{URLLINK}">{INDICE}</a></li>
    {/BLC_PAGINAS}
    <li class="{HABPROX}"><a href="{URLPROXIMO}">&raquo;</a>
  </ul>
</nav>
<!-- html de paginação --> 