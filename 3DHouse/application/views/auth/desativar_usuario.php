<?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('message') ?>
                        </div>
                    <?php endif; ?>
<legend> 
Manutenção de Usuários - Status<!--novo saber que esta adicionando novo admin -->

</legend> 


<p><?php echo sprintf(lang('deactivate_subheading'), $usuario->nomeusuario);?></p>

<?php echo form_open("auth/deactivate/".$usuario->codusuario);?>

  <p>
  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    <input type="radio" name="confirm" value="no" />
  </p>

  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$usuario->codusuario)); ?>

 <button type="submit" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em> Salvar</button>
        <a href="../index"><button class="btn btn-danger" type="button"><em class="glyphicon glyphicon-remove"></em> Cancelar</button></a>


<?php echo form_close();?>