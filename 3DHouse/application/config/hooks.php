<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['display_override'] = array( #sobreescreve a função de display
'class' => 'Template', # classe que sera responsavel por manipular o hook
  'function' => 'init', # função principal
  'filename' => 'Template.php', #nome do arquivo do hook
  'filepath' => 'hooks'#pasta que esta localizado

); 

/* 
/*  

hook sobresqueve a função de display 
passo 4 criar u arquivo template.php dentro da pasta hooks
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/ 

