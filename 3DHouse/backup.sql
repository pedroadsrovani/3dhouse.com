﻿begin;

CREATE TABLE atributo (
  codatributo serial NOT NULL,
  nomeatributo varchar(45) NOT NULL,
  codtipoatributo integer NOT NULL
) ;

--
-- Extraindo dados da tabela atributo
--

INSERT INTO atributo (codatributo, nomeatributo, codtipoatributo) VALUES
(1, 'verde', 1),
(2, 'amarelo', 1),
(3, 'biodegradável', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela carrinho
--

CREATE TABLE carrinho (
  codcarrinho serial NOT NULL,
  datahoracompra timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  valorcompra decimal(10,2) NOT NULL,
  valorfrete decimal(10,2) DEFAULT NULL,
  valorfinalcompra decimal(10,2) NOT NULL,
  codcliente integer NOT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela departamento
--

CREATE TABLE departamento (
  codepartamento serial NOT NULL,
  nomedepartamento varchar(45) NOT NULL,
  coddepartamentopai integer DEFAULT NULL
) ;

--
-- Extraindo dados da tabela departamento
--

INSERT INTO departamento (codepartamento, nomedepartamento, coddepartamentopai) VALUES
(1, 'departamentopai', NULL),
(2, 'departamentofilho', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela groups
--

CREATE TABLE groups (
  id serial NOT NULL,
  name varchar(20) NOT NULL,
  description varchar(100) NOT NULL
) ;

--
-- Extraindo dados da tabela groups
--

INSERT INTO groups (id, name, description) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'cliente', 'grupo de clientes do site');

-- --------------------------------------------------------

--
-- Estrutura da tabela imagens
--

CREATE TABLE imagens (
  id serial NOT NULL,
  tipo text NOT NULL,
  comentario text,
  descricao text
) ;

--
-- Extraindo dados da tabela imagens
--

INSERT INTO imagens (id, tipo, comentario, descricao) VALUES
(3, 'uploads/avatar3.png', 'rtrt', 'rtrt');

-- --------------------------------------------------------

--
-- Estrutura da tabela itemcarrinho
--

CREATE TABLE itemcarrinho (
  coditemcarrinho serial NOT NULL,
  valoritem decimal(10,2) NOT NULL,
  quantidadeitem integer NOT NULL,
  codcarrinho integer NOT NULL,
  codproduto integer NOT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela login_attempts
--

CREATE TABLE login_attempts (
  id serial NOT NULL,
  ip_address varchar(15) NOT NULL,
  login varchar(100) NOT NULL,
  time integer DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela loja
--

CREATE TABLE loja (
  codloja serial NOT NULL,
  nomeloja varchar(100) DEFAULT NULL,
  enderecoloja varchar(100) DEFAULT NULL,
  cnpjloja varchar(14) DEFAULT NULL,
  emailloja varchar(100) DEFAULT NULL,
  cidadeloja varchar(100) DEFAULT NULL,
  ufloja varchar(100) DEFAULT NULL,
  bairroloja varchar(100) DEFAULT NULL,
  complementoloja text,
  ceploja varchar(8) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela produto
--

CREATE TABLE produto (
  codproduto serial NOT NULL,
  nomeproduto varchar(100) NOT NULL,
  resumoproduto text NOT NULL,
  fichaproduto text NOT NULL,
  valorproduto decimal(10,2) NOT NULL,
  valorpromocional decimal(10,2) DEFAULT NULL,
  codtipoatributo integer DEFAULT NULL,
  urlseo varchar(120) NOT NULL
) ;

--
-- Extraindo dados da tabela produto
--

INSERT INTO produto (codproduto, nomeproduto, resumoproduto, fichaproduto, valorproduto, valorpromocional, codtipoatributo, urlseo) VALUES
(4, 'produto de teste', 'efeff', 'fefef', '100.00', '20.00', NULL, 'produto-de-teste'),
(5, 'produto000', 'dwdd', 'wdw', '100.00', '99.00', 1, 'produto000');

-- --------------------------------------------------------

--
-- Estrutura da tabela produtodepartamento
--

CREATE TABLE produtodepartamento (
  codproduto integer NOT NULL,
  codprodutodepartamento integer NOT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela produtofoto
--

CREATE TABLE produtofoto (
  codprodutofoto serial NOT NULL,
  tipo varchar(100) DEFAULT NULL,
  codproduto integer NOT NULL,
  descricao text
) ;

--
-- Extraindo dados da tabela produtofoto
--

INSERT INTO produtofoto (codprodutofoto, tipo, codproduto, descricao) VALUES
(4, 'uploadprodutofoto/avatar0.png', 4, 'dfdf'),
(5, 'uploadprodutofoto/avatar1.png', 5, 'descrica'),
(6, 'uploadprodutofoto/avatar2.png', 5, 'descricao 2');

-- --------------------------------------------------------

--
-- Estrutura da tabela produtofotosku
--

CREATE TABLE produtofotosku (
  codprodutofotosku serial NOT NULL,
  codprodutofoto integer NOT NULL,
  codsku integer NOT NULL
) ;

--
-- Extraindo dados da tabela produtofotosku
--

INSERT INTO produtofotosku (codprodutofotosku, codprodutofoto, codsku) VALUES
(3, 5, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela sku
--

CREATE TABLE sku (
  codsku serial NOT NULL,
  referencia varchar(150) DEFAULT NULL,
  quantidade integer NOT NULL DEFAULT '0',
  codproduto integer NOT NULL
) ;

--
-- Extraindo dados da tabela sku
--

INSERT INTO sku (codsku, referencia, quantidade, codproduto) VALUES
(1, 'Ref-0004', 100, 4),
(2, 'RF-001', 10, 5),
(3, 'RF-002', 25, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela skuatributo
--

CREATE TABLE skuatributo (
  codsku serial NOT NULL,
  codatributo integer NOT NULL
) ;

--
-- Extraindo dados da tabela skuatributo
--

INSERT INTO skuatributo (codsku, codatributo) VALUES
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela tipoatributo
--

CREATE TABLE tipoatributo (
  codtipoatributo serial NOT NULL,
  nometipoatributo varchar(45) NOT NULL
) ;

--
-- Extraindo dados da tabela tipoatributo
--

INSERT INTO tipoatributo (codtipoatributo, nometipoatributo) VALUES
(1, 'cor'),
(2, 'material');

-- --------------------------------------------------------

--
-- Estrutura da tabela users
--

CREATE TABLE users (
  id serial NOT NULL,
  ip_address varchar(45) NOT NULL,
  username varchar(100) DEFAULT NULL,
  password varchar(255) NOT NULL,
  salt varchar(255) DEFAULT NULL,
  email varchar(100) NOT NULL,
  activation_code varchar(40) DEFAULT NULL,
  forgotten_password_code varchar(40) DEFAULT NULL,
  forgotten_password_time integer DEFAULT NULL,
  remember_code varchar(40) DEFAULT NULL,
  created_on integer NOT NULL,
  last_login integer DEFAULT NULL,
  active smallint DEFAULT NULL,
  first_name varchar(50) DEFAULT NULL,
  last_name varchar(50) DEFAULT NULL,
  cpf integer NOT NULL,
  phone varchar(20) DEFAULT NULL,
  sexo text NOT NULL,
  cep integer NOT NULL,
  cidade text NOT NULL,
  estado text NOT NULL,
  rua text NOT NULL,
  bairro text NOT NULL,
  complemento text
) ;

--
-- Extraindo dados da tabela users
--

INSERT INTO users (id, ip_address, username, password, salt, email, activation_code, forgotten_password_code, forgotten_password_time, remember_code, created_on, last_login, active, first_name, last_name, cpf, phone, sexo, cep, cidade, estado, rua, bairro, complemento) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$N.QdF4QrGdm2V17A902xK.zaEnsoXiozr4mQQtasYQ0Y9wHc3TYju', '', 'admin@admin.com', '', 'uTXm7pNUVy9irk9eL0qGBu7d2344909a76b6dcac', 1505918365, NULL, 1268889823, 1506462094, 1, 'pedro henrique', 'rovani', 214000, '555', 'masculino', 99955000, 'vila lanagaro', 'rs', '22 de outubro', ' centro', 'nao sei'),
(7, '::1', 'cliente@gmail.com', '$2y$08$lNe3tpPhc66rFd9V6Yi2I.UoFNIjxLY/zpyBbWcrdS5jT7.Pr3g4C', NULL, 'cliente@gmail.com', NULL, 'M-kNCNhVibolh2dmKfsS9Oe458fd03d20dca515c', 1506191301, NULL, 1505938871, 1506013684, 1, 'cliente', 'cliente 2', 456, '147159', 'masculino', 77480000, 'Alvorada', 'TO', 'dwd', 'wdw', 'wdwd'),
(8, '::1', 'teste@gmail.com', '$2y$08$NXsSD5YFJyknAIqn.SD3lOaBbsCo0W.ntrT.7Pbe35Q6xt3YlXKPG', NULL, 'teste@gmail.com', NULL, NULL, NULL, NULL, 1506188854, NULL, 1, 'ef', 'efeii', 47, '47', 'feminino', 99740000, 'Barão de Cotegipe', 'RS', 'rru', 'rur', 'fdf');

-- --------------------------------------------------------

--
-- Estrutura da tabela users_groups
--

CREATE TABLE users_groups (
  id serial NOT NULL,
  user_id integer NOT NULL,
  group_id integer NOT NULL
) ;

--
-- Extraindo dados da tabela users_groups
--

INSERT INTO users_groups (id, user_id, group_id) VALUES
(3, 1, 1),
(9, 7, 3),
(12, 8, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela usuario
--

CREATE TABLE usuario (
  codusuario serial NOT NULL,
  nome varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  senha varchar(300) NOT NULL,
  cpf integer NOT NULL,
  cep varchar(9) NOT NULL,
  rua varchar(100) NOT NULL,
  bairro varchar(100) NOT NULL,
  complemento text,
  cidade varchar(100) NOT NULL,
  estado varchar(100) NOT NULL,
  telefone integer NOT NULL,
  sexo char(9) NOT NULL,
  perfil varchar(45) NOT NULL
) ;

--
-- Extraindo dados da tabela usuario
--

INSERT INTO usuario (codusuario, nome, email, senha, cpf, cep, rua, bairro, complemento, cidade, estado, telefone, sexo, perfil) VALUES
(1, 'Pedro Henrique Rovani', 'pedroadsrovani@gmail.com', 'Ckvv4dZfYZ7Eg9bCKM21VLAf1LE0ZB4CZdn/U2BiXhmB5F41LAUeO1xrnRxtclx+yBBnyk3qYKMx07BUhPMOUg==', 789, '99955000', '22 de Outubro', 'Centro', 'frfrfrggrgrfrfegeageragrgrg54', 'Vila Lângaro', 'RS', 99155785, '', 'administrador'),
(3, 'Teste 2', 'teste@gmail.com', 'WiN8Q8sVuzitulldaf1SU2aMgHMhKjTth4kksrcg72LBG5eVX0FPK2b9u0SaTXGEVBaBgUWr4JrN6nPbHwaY/g==', 22, '99955000', 'Rya', 'centro', 'efef', 'Vila Lângaro', 'RS', 54, 'masculino', 'administrador');

CREATE TABLE public.formapagamento
(
  codformapagamento integer NOT NULL DEFAULT nextval('formapagamento_codformapagamento_seq'::regclass),
  nomeformapagamento character varying(100),
  tipoformapagamento character(1),
  descontoformapagamento numeric(10,2),
  habilitaformapagamento character(1) DEFAULT 'S'::bpchar,
  maximoparcelaspagamento integer DEFAULT 1,
  CONSTRAINT formapagamento_pkey PRIMARY KEY (codformapagamento)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.formapagamento
  OWNER TO site; 

CREATE TABLE public.formaentrega
(
  codformaentrega integer NOT NULL DEFAULT nextval('formaentrega_codformaentrega_seq'::regclass),
  nomeformaentrega character varying(100),
  habilitaformaentrega character(1) DEFAULT 'S'::bpchar,
  codigocorreiosformaentrega character varying(100),
  CONSTRAINT formaentrega_pkey PRIMARY KEY (codformaentrega)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.formaentrega
  OWNER TO site;


--
-- Indexes for dumped tables
--

--
-- Indexes for table atributo
--
ALTER TABLE atributo
  ADD PRIMARY KEY (codatributo);
  
create index fk_atributo_tipoatributo1_idx on  atributo (codtipoatributo);

--
-- Indexes for table carrinho
--
ALTER TABLE carrinho
  ADD PRIMARY KEY (codcarrinho);
 
create index fk_carrinho_comprador1_idx on carrinho (codcliente);

--
-- Indexes for table departamento
--
ALTER TABLE departamento
  ADD PRIMARY KEY (codepartamento);
  
create index fk_departamento_departamento1_idx on departamento (coddepartamentopai);

--
-- Indexes for table groups
--
ALTER TABLE groups
  ADD PRIMARY KEY (id);

--
-- Indexes for table imagens
--
ALTER TABLE imagens
  ADD PRIMARY KEY (id);

--
-- Indexes for table itemcarrinho
--
ALTER TABLE itemcarrinho
  ADD PRIMARY KEY (coditemcarrinho);

create index  fk_itemcarrinho_carrinho1_idx on itemcarrinho (codcarrinho);
create index fk_itemcarrinho_produto1_idx1 on itemcarrinho (codproduto);

--
-- Indexes for table login_attempts
--
ALTER TABLE login_attempts
  ADD PRIMARY KEY (id);

--
-- Indexes for table loja
--
ALTER TABLE loja
  ADD PRIMARY KEY (codloja);

--
-- Indexes for table produto
--
ALTER TABLE produto
  ADD PRIMARY KEY (codproduto);
create index fk_produto_tipoatributo1_idx on produto (codtipoatributo);

--
-- Indexes for table produtodepartamento
--
ALTER TABLE produtodepartamento
  ADD PRIMARY KEY (codproduto,codprodutodepartamento);
  create index fk_produto_has_departamento_departamento1_idx on produtodepartamento (codprodutodepartamento);
 create index fk_produto_has_departamento_produto1_idx on produtodepartamento (codproduto);

--
-- Indexes for table produtofoto
--
ALTER TABLE produtofoto
  ADD PRIMARY KEY (codprodutofoto),
  ADD constraint  codprodutofoto_UNIQUE UNIQUE(codprodutofoto);
create index fk_produtofoto_produto1_idx on produtofoto (codproduto);

--
-- Indexes for table produtofotosku
--
ALTER TABLE produtofotosku
  ADD PRIMARY KEY (codprodutofotosku);
 create index fk_produtofotosku_produtofoto1_idx on produtofotosku (codprodutofoto);
 create index fk_produtofotosku_sku1_idx on produtofotosku (codsku);

--
-- Indexes for table sku
--
ALTER TABLE sku
  ADD PRIMARY KEY (codsku);
create index fk_sku_produto1_idx1 on sku (codproduto);

--
-- Indexes for table skuatributo
--
ALTER TABLE skuatributo
  ADD PRIMARY KEY (codsku,codatributo);
  
  create index fk_sku_has_atributo_atributo1_idx on skuatributo (codatributo);
  create index fk_sku_has_atributo_sku1_idx on skuatributo (codsku);

--
-- Indexes for table tipoatributo
--
ALTER TABLE tipoatributo
  ADD PRIMARY KEY (codtipoatributo);

--
-- Indexes for table users
--
ALTER TABLE users
  ADD PRIMARY KEY (id);

--
-- Indexes for table users_groups
--
ALTER TABLE users_groups
  ADD PRIMARY KEY (id),
  ADD constraint uc_users_groups unique(user_id,group_id);
 
 Create index fk_users_groups_users1_idx on users_groups (user_id);
 create index fk_users_groups_groups1_idx on users_groups (group_id);

--
-- Indexes for table usuario
--
ALTER TABLE usuario
  ADD PRIMARY KEY (codusuario);


-- Constraints for dumped tables
--

--
-- Limitadores para a tabela atributo
--
ALTER TABLE atributo
  ADD CONSTRAINT fk_atributo_tipoatributo1 FOREIGN KEY (codtipoatributo) REFERENCES tipoatributo (codtipoatributo) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela departamento
--
ALTER TABLE departamento
  ADD CONSTRAINT fk_departamento_departamento1 FOREIGN KEY (coddepartamentopai) REFERENCES departamento (codepartamento) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela itemcarrinho
--
ALTER TABLE itemcarrinho
  ADD CONSTRAINT fk_itemcarrinho_carrinho1 FOREIGN KEY (codcarrinho) REFERENCES carrinho (codcarrinho) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_itemcarrinho_produto1 FOREIGN KEY (codproduto) REFERENCES produto (codproduto) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela produto
--
ALTER TABLE produto
  ADD CONSTRAINT fk_produto_tipoatributo1 FOREIGN KEY (codtipoatributo) REFERENCES tipoatributo (codtipoatributo) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela produtodepartamento
--
ALTER TABLE produtodepartamento
  ADD CONSTRAINT fk_produto_has_departamento_departamento1 FOREIGN KEY (codprodutodepartamento) REFERENCES departamento (codepartamento) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_produto_has_departamento_produto1 FOREIGN KEY (codproduto) REFERENCES produto (codproduto) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela produtofoto
--
ALTER TABLE produtofoto
  ADD CONSTRAINT fk_produtofoto_produto1 FOREIGN KEY (codproduto) REFERENCES produto (codproduto) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela produtofotosku
--
ALTER TABLE produtofotosku
  ADD CONSTRAINT fk_produtofotosku_produtofoto1 FOREIGN KEY (codprodutofoto) REFERENCES produtofoto (codprodutofoto) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_produtofotosku_sku1 FOREIGN KEY (codsku) REFERENCES sku (codsku) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela sku
--
ALTER TABLE sku
  ADD CONSTRAINT fk_sku_produto1 FOREIGN KEY (codproduto) REFERENCES produto (codproduto) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela skuatributo
--
ALTER TABLE skuatributo
  ADD CONSTRAINT fk_sku_has_atributo_atributo1 FOREIGN KEY (codatributo) REFERENCES atributo (codatributo) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_sku_has_atributo_sku1 FOREIGN KEY (codsku) REFERENCES sku (codsku) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela users_groups
--
ALTER TABLE users_groups
  ADD CONSTRAINT fk_users_groups_groups1 FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_users_groups_users1 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;
