-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27-Set-2017 às 02:52
-- Versão do servidor: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `3dhouse`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atributo`
--

CREATE TABLE `atributo` (
  `codatributo` int(11) NOT NULL,
  `nomeatributo` varchar(45) NOT NULL,
  `codtipoatributo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `atributo`
--

INSERT INTO `atributo` (`codatributo`, `nomeatributo`, `codtipoatributo`) VALUES
(1, 'verde', 1),
(2, 'amarelo', 1),
(3, 'biodegradável', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrinho`
--

CREATE TABLE `carrinho` (
  `codcarrinho` int(11) NOT NULL,
  `datahoracompra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `valorcompra` decimal(10,2) NOT NULL,
  `valorfrete` decimal(10,2) DEFAULT NULL,
  `valorfinalcompra` decimal(10,2) NOT NULL,
  `codcliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `departamento`
--

CREATE TABLE `departamento` (
  `codepartamento` int(11) NOT NULL COMMENT 'Departamento do produto',
  `nomedepartamento` varchar(45) NOT NULL,
  `coddepartamentopai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `departamento`
--

INSERT INTO `departamento` (`codepartamento`, `nomedepartamento`, `coddepartamentopai`) VALUES
(1, 'departamentopai', NULL),
(2, 'departamentofilho', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'cliente', 'grupo de clientes do site');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens`
--

CREATE TABLE `imagens` (
  `id` int(10) NOT NULL,
  `tipo` text NOT NULL,
  `comentario` text,
  `descricao` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `imagens`
--

INSERT INTO `imagens` (`id`, `tipo`, `comentario`, `descricao`) VALUES
(3, 'uploads/avatar3.png', 'rtrt', 'rtrt');

-- --------------------------------------------------------

--
-- Estrutura da tabela `itemcarrinho`
--

CREATE TABLE `itemcarrinho` (
  `coditemcarrinho` int(11) NOT NULL,
  `valoritem` decimal(10,2) NOT NULL,
  `quantidadeitem` int(11) NOT NULL,
  `codcarrinho` int(11) NOT NULL,
  `codproduto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `loja`
--

CREATE TABLE `loja` (
  `codloja` int(11) NOT NULL,
  `nomeloja` varchar(100) DEFAULT NULL,
  `enderecoloja` varchar(100) DEFAULT NULL,
  `cnpjloja` varchar(14) DEFAULT NULL,
  `emailloja` varchar(100) DEFAULT NULL,
  `cidadeloja` varchar(100) DEFAULT NULL,
  `ufloja` varchar(100) DEFAULT NULL,
  `bairroloja` varchar(100) DEFAULT NULL,
  `complementoloja` text,
  `ceploja` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `codproduto` int(11) NOT NULL,
  `nomeproduto` varchar(100) NOT NULL,
  `resumoproduto` text NOT NULL,
  `fichaproduto` text NOT NULL,
  `valorproduto` decimal(10,2) NOT NULL,
  `valorpromocional` decimal(10,2) DEFAULT NULL,
  `codtipoatributo` int(11) DEFAULT NULL COMMENT 'armazena a classificação do produto.',
  `urlseo` varchar(120) NOT NULL COMMENT 'armazena a url para seo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`codproduto`, `nomeproduto`, `resumoproduto`, `fichaproduto`, `valorproduto`, `valorpromocional`, `codtipoatributo`, `urlseo`) VALUES
(4, 'produto de teste', 'efeff', 'fefef', '100.00', '20.00', NULL, 'produto-de-teste'),
(5, 'produto000', 'dwdd', 'wdw', '100.00', '99.00', 1, 'produto000');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtodepartamento`
--

CREATE TABLE `produtodepartamento` (
  `codproduto` int(11) NOT NULL,
  `codprodutodepartamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtofoto`
--

CREATE TABLE `produtofoto` (
  `codprodutofoto` int(11) NOT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `codproduto` int(11) NOT NULL,
  `descricao` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtofoto`
--

INSERT INTO `produtofoto` (`codprodutofoto`, `tipo`, `codproduto`, `descricao`) VALUES
(4, 'uploadprodutofoto/avatar0.png', 4, 'dfdf'),
(5, 'uploadprodutofoto/avatar1.png', 5, 'descrica'),
(6, 'uploadprodutofoto/avatar2.png', 5, 'descricao 2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtofotosku`
--

CREATE TABLE `produtofotosku` (
  `codprodutofotosku` int(11) NOT NULL,
  `codprodutofoto` int(11) NOT NULL,
  `codsku` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtofotosku`
--

INSERT INTO `produtofotosku` (`codprodutofotosku`, `codprodutofoto`, `codsku`) VALUES
(3, 5, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sku`
--

CREATE TABLE `sku` (
  `codsku` int(11) NOT NULL,
  `referencia` varchar(150) DEFAULT NULL COMMENT 'contém a referencia do sku com o produto.',
  `quantidade` int(11) NOT NULL DEFAULT '0' COMMENT 'contém a quantidade do produto em estoque.',
  `codproduto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `sku`
--

INSERT INTO `sku` (`codsku`, `referencia`, `quantidade`, `codproduto`) VALUES
(1, 'Ref-0004', 100, 4),
(2, 'RF-001', 10, 5),
(3, 'RF-002', 25, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `skuatributo`
--

CREATE TABLE `skuatributo` (
  `codsku` int(11) NOT NULL,
  `codatributo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `skuatributo`
--

INSERT INTO `skuatributo` (`codsku`, `codatributo`) VALUES
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoatributo`
--

CREATE TABLE `tipoatributo` (
  `codtipoatributo` int(11) NOT NULL,
  `nometipoatributo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipoatributo`
--

INSERT INTO `tipoatributo` (`codtipoatributo`, `nometipoatributo`) VALUES
(1, 'cor'),
(2, 'material');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `cpf` int(11) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `sexo` text NOT NULL,
  `cep` int(8) NOT NULL,
  `cidade` text NOT NULL,
  `estado` text NOT NULL,
  `rua` text NOT NULL,
  `bairro` text NOT NULL,
  `complemento` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `cpf`, `phone`, `sexo`, `cep`, `cidade`, `estado`, `rua`, `bairro`, `complemento`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$N.QdF4QrGdm2V17A902xK.zaEnsoXiozr4mQQtasYQ0Y9wHc3TYju', '', 'admin@admin.com', '', 'uTXm7pNUVy9irk9eL0qGBu7d2344909a76b6dcac', 1505918365, NULL, 1268889823, 1506462094, 1, 'pedro henrique', 'rovani', 214000, '555', 'masculino', 99955000, 'vila lanagaro', 'rs', '22 de outubro', ' centro', 'nao sei'),
(7, '::1', 'cliente@gmail.com', '$2y$08$lNe3tpPhc66rFd9V6Yi2I.UoFNIjxLY/zpyBbWcrdS5jT7.Pr3g4C', NULL, 'cliente@gmail.com', NULL, 'M-kNCNhVibolh2dmKfsS9Oe458fd03d20dca515c', 1506191301, NULL, 1505938871, 1506013684, 1, 'cliente', 'cliente 2', 456, '147159', 'masculino', 77480000, 'Alvorada', 'TO', 'dwd', 'wdw', 'wdwd'),
(8, '::1', 'teste@gmail.com', '$2y$08$NXsSD5YFJyknAIqn.SD3lOaBbsCo0W.ntrT.7Pbe35Q6xt3YlXKPG', NULL, 'teste@gmail.com', NULL, NULL, NULL, NULL, 1506188854, NULL, 1, 'ef', 'efeii', 47, '47', 'feminino', 99740000, 'Barão de Cotegipe', 'RS', 'rru', 'rur', 'fdf');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(3, 1, 1),
(9, 7, 3),
(12, 8, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `codusuario` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(300) NOT NULL,
  `cpf` int(11) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `rua` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `complemento` text,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `telefone` int(15) NOT NULL,
  `sexo` char(9) NOT NULL,
  `perfil` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`codusuario`, `nome`, `email`, `senha`, `cpf`, `cep`, `rua`, `bairro`, `complemento`, `cidade`, `estado`, `telefone`, `sexo`, `perfil`) VALUES
(1, 'Pedro Henrique Rovani', 'pedroadsrovani@gmail.com', 'Ckvv4dZfYZ7Eg9bCKM21VLAf1LE0ZB4CZdn/U2BiXhmB5F41LAUeO1xrnRxtclx+yBBnyk3qYKMx07BUhPMOUg==', 789, '99955000', '22 de Outubro', 'Centro', 'frfrfrggrgrfrfegeageragrgrg54', 'Vila Lângaro', 'RS', 99155785, '', 'administrador'),
(3, 'Teste 2', 'teste@gmail.com', 'WiN8Q8sVuzitulldaf1SU2aMgHMhKjTth4kksrcg72LBG5eVX0FPK2b9u0SaTXGEVBaBgUWr4JrN6nPbHwaY/g==', 22, '99955000', 'Rya', 'centro', 'efef', 'Vila Lângaro', 'RS', 54, 'masculino', 'administrador');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atributo`
--
ALTER TABLE `atributo`
  ADD PRIMARY KEY (`codatributo`),
  ADD KEY `fk_atributo_tipoatributo1_idx` (`codtipoatributo`);

--
-- Indexes for table `carrinho`
--
ALTER TABLE `carrinho`
  ADD PRIMARY KEY (`codcarrinho`),
  ADD KEY `fk_carrinho_comprador1_idx` (`codcliente`);

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`codepartamento`),
  ADD KEY `fk_departamento_departamento1_idx` (`coddepartamentopai`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagens`
--
ALTER TABLE `imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itemcarrinho`
--
ALTER TABLE `itemcarrinho`
  ADD PRIMARY KEY (`coditemcarrinho`),
  ADD KEY `fk_itemcarrinho_carrinho1_idx` (`codcarrinho`),
  ADD KEY `fk_itemcarrinho_produto1_idx1` (`codproduto`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loja`
--
ALTER TABLE `loja`
  ADD PRIMARY KEY (`codloja`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`codproduto`),
  ADD KEY `fk_produto_tipoatributo1_idx` (`codtipoatributo`);

--
-- Indexes for table `produtodepartamento`
--
ALTER TABLE `produtodepartamento`
  ADD PRIMARY KEY (`codproduto`,`codprodutodepartamento`),
  ADD KEY `fk_produto_has_departamento_departamento1_idx` (`codprodutodepartamento`),
  ADD KEY `fk_produto_has_departamento_produto1_idx` (`codproduto`);

--
-- Indexes for table `produtofoto`
--
ALTER TABLE `produtofoto`
  ADD PRIMARY KEY (`codprodutofoto`),
  ADD UNIQUE KEY `codprodutofoto_UNIQUE` (`codprodutofoto`),
  ADD KEY `fk_produtofoto_produto1_idx` (`codproduto`);

--
-- Indexes for table `produtofotosku`
--
ALTER TABLE `produtofotosku`
  ADD PRIMARY KEY (`codprodutofotosku`),
  ADD KEY `fk_produtofotosku_produtofoto1_idx` (`codprodutofoto`),
  ADD KEY `fk_produtofotosku_sku1_idx` (`codsku`);

--
-- Indexes for table `sku`
--
ALTER TABLE `sku`
  ADD PRIMARY KEY (`codsku`),
  ADD KEY `fk_sku_produto1_idx1` (`codproduto`);

--
-- Indexes for table `skuatributo`
--
ALTER TABLE `skuatributo`
  ADD PRIMARY KEY (`codsku`,`codatributo`),
  ADD KEY `fk_sku_has_atributo_atributo1_idx` (`codatributo`),
  ADD KEY `fk_sku_has_atributo_sku1_idx` (`codsku`);

--
-- Indexes for table `tipoatributo`
--
ALTER TABLE `tipoatributo`
  ADD PRIMARY KEY (`codtipoatributo`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`codusuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atributo`
--
ALTER TABLE `atributo`
  MODIFY `codatributo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `carrinho`
--
ALTER TABLE `carrinho`
  MODIFY `codcarrinho` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departamento`
--
ALTER TABLE `departamento`
  MODIFY `codepartamento` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Departamento do produto', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `imagens`
--
ALTER TABLE `imagens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `itemcarrinho`
--
ALTER TABLE `itemcarrinho`
  MODIFY `coditemcarrinho` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `codproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `produtofoto`
--
ALTER TABLE `produtofoto`
  MODIFY `codprodutofoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `produtofotosku`
--
ALTER TABLE `produtofotosku`
  MODIFY `codprodutofotosku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sku`
--
ALTER TABLE `sku`
  MODIFY `codsku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipoatributo`
--
ALTER TABLE `tipoatributo`
  MODIFY `codtipoatributo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `codusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `atributo`
--
ALTER TABLE `atributo`
  ADD CONSTRAINT `fk_atributo_tipoatributo1` FOREIGN KEY (`codtipoatributo`) REFERENCES `tipoatributo` (`codtipoatributo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `fk_departamento_departamento1` FOREIGN KEY (`coddepartamentopai`) REFERENCES `departamento` (`codepartamento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `itemcarrinho`
--
ALTER TABLE `itemcarrinho`
  ADD CONSTRAINT `fk_itemcarrinho_carrinho1` FOREIGN KEY (`codcarrinho`) REFERENCES `carrinho` (`codcarrinho`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_itemcarrinho_produto1` FOREIGN KEY (`codproduto`) REFERENCES `produto` (`codproduto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `fk_produto_tipoatributo1` FOREIGN KEY (`codtipoatributo`) REFERENCES `tipoatributo` (`codtipoatributo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produtodepartamento`
--
ALTER TABLE `produtodepartamento`
  ADD CONSTRAINT `fk_produto_has_departamento_departamento1` FOREIGN KEY (`codprodutodepartamento`) REFERENCES `departamento` (`codepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produto_has_departamento_produto1` FOREIGN KEY (`codproduto`) REFERENCES `produto` (`codproduto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `produtofoto`
--
ALTER TABLE `produtofoto`
  ADD CONSTRAINT `fk_produtofoto_produto1` FOREIGN KEY (`codproduto`) REFERENCES `produto` (`codproduto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `produtofotosku`
--
ALTER TABLE `produtofotosku`
  ADD CONSTRAINT `fk_produtofotosku_produtofoto1` FOREIGN KEY (`codprodutofoto`) REFERENCES `produtofoto` (`codprodutofoto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produtofotosku_sku1` FOREIGN KEY (`codsku`) REFERENCES `sku` (`codsku`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `sku`
--
ALTER TABLE `sku`
  ADD CONSTRAINT `fk_sku_produto1` FOREIGN KEY (`codproduto`) REFERENCES `produto` (`codproduto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `skuatributo`
--
ALTER TABLE `skuatributo`
  ADD CONSTRAINT `fk_sku_has_atributo_atributo1` FOREIGN KEY (`codatributo`) REFERENCES `atributo` (`codatributo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sku_has_atributo_sku1` FOREIGN KEY (`codsku`) REFERENCES `sku` (`codsku`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
